<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/19
 * Time: 15:59
 */
Route::group(['name'=>'tdmin','middleware'=>['TdminAuth']],function(){
    Route::get('/','tdmin/Index/index');
    Route::get('controlboard','tdmin/Index/controlboard');
    Route::get('goodslist','tdmin/Goods/goodsList');
    Route::get('goodscate','tdmin/Goods/categoryList');
    Route::post('editCategory','tdmin/Goods/editCategory');
    Route::get('goodstype','tdmin/Goods/goodsType');
    Route::get('rules','tdmin/Permission/rules');
    Route::get('groups','tdmin/Permission/groups');
    Route::get('adminlist','tdmin/Authuser/index');
    Route::get('videocate','tdmin/Video/videocate');
    Route::get('videolist','tdmin/Video/index');
    Route::get('orderlist','tdmin/Order/index');
    Route::get('articlelist','tdmin/Article/index');
    Route::get('complainlist','tdmin/Complain/index');
    Route::get('site','tdmin/Sysconfig/index');
    Route::get('backsql','tdmin/Sysconfig/index');
    Route::get('integration','tdmin/Capital/integration');
    Route::get('commission','tdmin/Captial/commission');
    Route::get('withdraw','tdmin/Captital/withdraw');
    Route::get('memberlist','tdmin/Member/index');
    Route::get('viplevel','tdmin/Member/viplevel');
    Route::get('couponlist','tdmin/Coupon/couponList');
    Route::get('buildorder','tdmin/BuildOrder/index');
    Route::get('repairorder','tdmin/RepairOrder/index');
    Route::get('slide','tdmin/Video/slide');
    Route::any('shopapply','tdmin/Apply/shopindex');
    Route::any('builderapply','tdmin/Apply/builderindex');
    Route::any('goodsComment','tdmin/Comment/index');
});
<?php
namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use app\common\controller\Common as Tool;

class Common extends Controller
{
    
    public function initialize()
    {
        parent::initialize();
        // dump(request()->server('REMOTE_ADDR'));die;
        // $ip = ['39.89.118.204','119.166.49.210'];
        // if(!in_array(request()->server('REMOTE_ADDR'), $ip)){
        //     session(null);
        //     echo '<div style="font-size: 80px;text-align: center;">系统维护中……</div>';
        //     die;
        // }
      // session(null);
      // echo '<div style="font-size: 80px;text-align: center;">清理缓存成功</div>';
      // die;
       
        if(session('?uid')){
            $user = db('Member')->where(['id'=>session('uid')])->find();
            if(!$user){
                $this->redirect('Wechat/login');
            }else{
                if(empty($user['wx_openid'])){
                    $this->redirect('Wechat/login');
                }
            }
        }else{
            $this->redirect('Wechat/login');
        }
    }

    public function getProvince(Request $request)
    {
        $param = $request->param();
        $list = model('Region')->where(['region_type'=>1])->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }


    public function getCity(Request $request)
    {
        $param = $request->param();
        $list = model('Region')->where(['parent_id'=>$param['province']])->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }

    public function getCounty(Request $request)
    {
        $param = $request->param();
        $list = model('Region')->where(['parent_id'=>$param['city']])->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }
    /**
     * [imgUpload 图片上传]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function imgUpload()
    {
        $file = request()->file('file');
        if (true !== $this->validate(['file' => $file], ['file' => 'require|image'])) {
            $this->error('请选择图像文件');
        } else {
            $info = $file->validate(['size' => 1204 * 1204 * 10, 'ext' => 'jpg,png,gif'])->move('Uploads');
            if ($info) {
                // 成功上传后 获取上传信息
                $savename = '/Uploads/' . $info->getSaveName();
                return json(['code' => 0, 'msg' => '上传成功', 'data' => $savename]);
            } else {
                // 上传失败获取错误信息
                $error = $file->getError();
                return json(['code' => 40001, 'msg' => $error]);
            }
        }
    }
    /**
     * [base64Upload 图片base64上传]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function base64Upload()
    {
        $re = base64_image_content(input('file'),'Uploads/'.date('Y-m-d'));
        if($re){
            return json(['code'=>0,'msg'=>'保存成功','data'=>config('site.site_url').'/'.$re]);
        }else{
            return json(['code' => 40001, 'msg' =>'失败']);
        }
    }


}

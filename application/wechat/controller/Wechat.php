<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/7/16
 * Time: 16:55
 */
namespace app\wechat\controller;

use think\Request;
use think\Controller;
use EasyWeChat\Factory;
use think\Db;

class Wechat extends Controller
{
    public $config ;

    public function __construct()
    {
        $this->config = [
            'app_id' => config('site.wx_gzh_appid'),
            'secret' => config('site.wx_gzh_screte'),
            'response_type' => 'array',
            'token' => '',
        ];
    }

    public function index()
    {
        $app = Factory::officialAccount($this->config);

        $response = $app->server->serve();

        $response->send();exit;

    }

    public function login(Request $request)
    {
        $param = $request->param();
        $config = [
            'app_id' => config('site.wx_gzh_appid'),
            'secret' => config('site.wx_gzh_screte'),
            'response_type' => 'array',
            'oauth' => [
                'scopes'   => ['snsapi_userinfo'],
                'callback' => config('site.site_url').'/wechat/Wechat/loginback',
            ]
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $oauth->redirect()->send();
    }

    public function loginback(Request $request)
    {
        $app = Factory::officialAccount($this->config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $user = $user->toArray();

        if(isset($user['original']['unionid']) and $user['original']['unionid']){
            $has = Db::name('Member')->where(['union_id'=>$user['original']['unionid']])->find();
            if($has){
                $re = model('Member')->where(['union_id'=>$user['original']['unionid']])->setField('wx_openid',$user['id']);
            }else{
                $re = model('Member')->save(['nickname'=>$user['nickname'],'avatar'=>$user['avatar'],'realname'=>$user['nickname'],'wx_openid'=>$user['id'],'union_id'=>$user['original']['unionid']]);
            }
            if(false !== $re){
                echo '<div style="font-size: 80px;text-align: center;margin-top: 200px;color: #0c570f;">绑定成功</div>';
            }else{
                echo '<div style="font-size: 80px;text-align: center;margin-top: 200px;color: #0c570f;">绑定失败，请重试！</div>';
            }
        }else{
            echo '<div style="font-size: 80px;text-align: center;margin-top: 200px;color: #0c570f;">绑定失败，请重试！</div>';
        }
        exit;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/18
 * Time: 14:31
 */
namespace app\common\model;
use think\Model;
class Member extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['role_text'];

    public function getRoleTextAttr($value,$data)
    {
        $arr = ['0'=>'普通会员','1'=>'商家会员','2'=>'个人施工方','3'=>'企业施工方'];
        return $arr[$data['role']];
    }
}
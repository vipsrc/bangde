<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/21
 * Time: 9:39
 */
namespace app\common\model;

use think\Model;

class VipLevel extends Model
{
    protected $append = ['role_text'];

    public function getRoleTextAttr($value,$data)
    {
        $arr = ['0'=>'普通用户','1'=>'商家用户','2'=>'施工单位'];
        return $arr[$data['role']];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/5/16
 * Time: 18:22
 */
namespace app\common\model;

use think\Model;

class BuildOrder extends Model
{

    protected $autoWriteTimestamp = true;
    protected $append = ['type_text','device_type_text','plan_type_text','status_text'];
    /**
     * [user 找到接受订单所属的施工方]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function user()
    {
        return $this->belongsTo('Member', 'uid', 'id');
    }

    /**
     * [publisherUser 找到订单的发布人]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function publisherUser()
    {
        return $this->belongsTo('Member', 'publisher', 'id')->bind(['publisher_name'=>'nickname']);
    }

    /**
     * [buildComment 查找所属的评论]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function buildComment()
    {
        return $this->hasMany('BuildComment', 'order_no', 'order_no');
    }

    public function getTypeTextAttr($value,$data)
    {
        $arr = config('base.border_type');
        return $arr[$data['type']];
    }

    public function getDeviceTypeTextAttr($value,$data)
    {
        $arr = config('base.device_type');
        return $arr[$data['device_type']];
    }

    public function getPlanTypeTextAttr($value,$data)
    {
        $arr = config('base.plan_type');
        return $arr[$data['plan_type']];
    }

    public function getStatusTextAttr($value,$data)
    {
        $arr = config('base.border_status');
        return $arr[$data['status']];
    }

    public function builder()
    {
        return $this->belongsTo('Member','builder','id')->bind(['builder_name'=>'nickname']);
    }
}

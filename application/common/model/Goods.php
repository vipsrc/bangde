<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2018/12/6
 * Time: 17:31
 */
namespace app\common\model;

use think\Model;

class Goods extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['status_text', 'is_recom_text','delivery_type_text'];

    /**
     * [getStatusTextAttr 获取状态文本]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @param    [type]     $value [description]
     * @param    [type]     $data  [description]
     * @return   [type]            [description]
     */
    public function getStatusTextAttr($value, $data)
    {
        $arr = ['0' => '下架', '1' => '上架'];
        return $arr[$data['status']];
    }

    /**
     * [getIsRecomTextAttr 获取推荐文本]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @param    [type]     $value [description]
     * @param    [type]     $data  [description]
     * @return   [type]            [description]
     */
    public function getIsRecomTextAttr($value, $data)
    {
        $arr = ['0' => '否', '1' => '是'];
        return $arr[$data['is_recom']];
    }

    /**
     * [goodsAttr 商品属性]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function goodsAttr()
    {
        return $this->hasMany('GoodsAttr','goods_id','id');
    }

    /**
     * [cate 关联分类]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function cate()
    {
        return $this->belongsTo('Cate','cid','id');
    }

    /**
     * [gallery 关联相册]
     * @Author   雨夜
     * @DateTime 2019-01-04
     * @return   [type]     [description]
     */
    public function gallery()
    {
        return $this->hasMany('GoodsGallery','gid','id');
    }

    public function getDeliveryTypeTextAttr($value,$data)
    {
        $arr = config('base.delivery_type');
        return $arr[$data['delivery_type']];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/28
 * Time: 8:41
 */
namespace app\common\model;
use think\Model;

class UserCoupon extends Model
{
    protected $append = ['status_text'];

    public function getStatusTextAttr($value,$data)
    {
        $arr = ['-1'=>'已过期','0'=>'未使用','1'=>'已使用'];
        return $arr[$data['status']];
    }

}
<?php
namespace app\common\model;

use think\Model;

class Comment extends Model
{
	protected $autoWriteTimestamp = true;
	protected $append = ['is_show_text'];

	public function user()
	{
		return $this->belongsTo('Member','uid','id')->bind(['nickname']);
	}

	public function goods()
	{
		return $this->belongsTo('Goods','gid','id');
	}

	public function getIsShowTextAttr($value,$data)
    {
        $arr = ['0'=>'显示','1'=>'隐藏'];
        return $arr[$data['is_show']];
    }
}
?>
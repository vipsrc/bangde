<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 25/3/19
 * Time: 下午2:54
 */

namespace app\common\model;


use think\Model;

class Praise extends Model
{
    protected $autoWriteTimestamp = true;
    protected $table = 'td_forum_praise';
}
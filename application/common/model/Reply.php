<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 22/3/19
 * Time: 上午10:59
 */

namespace app\common\model;


use think\Model;

class Reply extends Model
{
    protected $autoWriteTimestamp = true;
    protected $table = 'td_forum_reply';

    public function member()
    {
        return $this->belongsTo('Member','uid','id')->bind(['nickname','avatar']);
    }
}
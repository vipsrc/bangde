<?php
namespace app\common\model;
use think\Model;

class UserAddress extends Model
{
	public function province()
    {
        return $this->belongsTo('Region','province','region_id')->bind(['province_name'=>'region_name']);
    }

    public function city()
    {
        return $this->belongsTo('Region','city','region_id')->bind(['city_name'=>'region_name']);
    }

    public function county()
    {
        return $this->belongsTo('Region','county','region_id')->bind(['county_name'=>'region_name']);
    }
}
?>
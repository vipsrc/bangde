<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/6/26
 * Time: 15:11
 */
namespace app\common\model;
use think\Model;

class Intention extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['house_model_text','house_type_text'];
    
    public function user()
    {
        return $this->belongsTo('Member','uid','id')->bind(['nickname']);
    }
    
    public function province()
    {
        return $this->belongsTo('Region','province','region_id')->bind(['province_name'=>'region_name']);
    }

    public function city()
    {
        return $this->belongsTo('Region','city','region_id')->bind(['city_name'=>'region_name']);
    }

    public function device()
    {
        return $this->belongsTo('DeviceType','device_type','id')->bind(['device_type_name'=>'name']);
    }

    public function getHouseModelTextAttr($value,$data)
    {
        $arr = config('base.index_house_model');
        return $arr[$data['house_model']];
    }

    public function getHouseTypeTextAttr($value,$data)
    {
        $arr = config('base.index_house_type');
        return $arr[$data['house_type']];
    }
}
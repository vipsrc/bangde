<?php
/* @project : tdbase
 * @auther  : 秋水
 * @date    : 2018/09/13
 * @desc    : 模型
 */

namespace app\common\model;
use think\Model;

class Coupon extends Model {

    protected $autoWriteTimestamp = true;

    protected $type = [
        'start_time'  =>  'timestamp',
        'end_time'  =>  'timestamp'
    ];
}
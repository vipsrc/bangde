<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/4/2
 * Time: 11:46
 */
namespace app\common\model;
use think\Model;

class RepairOrder extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['status_text'];

    public function user()
    {
        return $this->belongsTo('Member', 'uid', 'id');
    }

    public function builder()
    {
        return $this->belongsTo('Member','builder','id')->bind(['builder_name'=>'nickname']);
    }


    public function getStatusTextAttr($value,$data)
    {
        $arr = config('base.rorder_status');
        return $arr[$data['status']];
    }
}
<?php
namespace app\common\model;

use think\Model;

class Collection extends Model
{

	protected $autoWriteTimestamp = true;

	public function goods()
	{
		return $this->belongsTo('Goods','gid','id');
	}
}

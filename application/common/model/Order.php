<?php
namespace app\common\model;

use think\Model;

class Order extends Model
{
	protected $autoWriteTimestamp = true;

	protected $append = ['status_text','pay_state_text'];

	public function getStatusTextAttr($value,$data)
	{
		$arr = config('base.order_status');
		return $arr[$data['status']];
	}

	public function getPayStateTextAttr($value,$data)
	{
		$arr = config('base.pay_state');
		return $arr[$data['pay_state']];
	}

	public function orderGoods()
	{
		return $this->hasMany('OrderGoods','order_no','order_no');
	}

}
?>
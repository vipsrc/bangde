<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/22
 * Time: 13:36
 */
namespace app\common\model;
use think\Model;

class Apply extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['type_text'];

    public function getTypeTextAttr($value,$data)
    {
        $arr = ['1'=>'商家认证','2'=>'个人施工单位','3'=>'企业施工单位'];
        return $arr[$data['type']];
    }


    public function member()
    {
        return $this->belongsTo('Member','uid','id')->bind(['nickname']);
    }

    public function province()
    {
        return $this->belongsTo('Region','province','region_id')->bind(['province_name'=>'region_name']);
    }

    public function city()
    {
        return $this->belongsTo('Region','city','region_id')->bind(['city_name'=>'region_name']);
    }

    public function county()
    {
        return $this->belongsTo('Region','county','region_id')->bind(['county_name'=>'region_name']);
    }

    public function reprovince()
    {
        return $this->belongsTo('Region','province','region_id')->bind(['reprovince_name'=>'region_name']);
    }

    public function recity()
    {
        return $this->belongsTo('Region','city','region_id')->bind(['recity_name'=>'region_name']);
    }

    public function recounty()
    {
        return $this->belongsTo('Region','county','region_id')->bind(['recounty_name'=>'region_name']);
    }
}
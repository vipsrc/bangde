<?php
namespace app\common\model;

use think\Model;

class Complain extends Model
{
	protected $autoWriteTimestamp = true;

	public function member()
	{
		return $this->hasOne('Member','id','uid');
	}
}

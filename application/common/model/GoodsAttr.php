<?php
namespace app\common\model;

use think\Model;

class GoodsAttr extends Model
{
	/**
	 * [attribute 关联属性]
	 * @Author   雨夜
	 * @DateTime 2018-12-13
	 * @return   [type]     [description]
	 */
	public function attribute()
	{
		return $this->belongsTo('Attribute','attr_id','attr_id');
	}
}

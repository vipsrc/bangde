<?php
namespace app\common\model;

use think\Model;

class BuildComment extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['type_text','is_show_text'];
    /**
     * [user 获取评论人信息]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function user()
    {
        return $this->belongsTo('Member', 'uid', 'id')->bind(['nickname']);
    }

    /**
     * [buildUser 获取施工方信息]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function buildUser()
    {
        return $this->belongsTo('Member', 'builder', 'id')->bind(['builder_name'=>'nickname']);
    }

    public function getTypeTextAttr($value,$data)
    {
        $arr = ['0'=>'安装订单','1'=>'维修订单'];
        return $arr[$data['type']];
    }

    public function getIsShowTextAttr($value,$data)
    {
        $arr = ['0'=>'显示','1'=>'隐藏'];
        return $arr[$data['is_show']];
    }

}

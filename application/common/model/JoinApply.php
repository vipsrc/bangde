<?php
namespace app\common\model;

use think\Model;

class JoinApply extends Model
{

	protected $autoWriteTimestamp = true;

	public function user()
	{
		return $this->belongsTo('Member','uid','id')->bind('nickname');
	}
}
?>
<?php
namespace app\common\model;

use think\Model;

class CommissionLog extends Model
{

    protected $autoWriteTimestamp = true;
    protected $append             = ['type_text'];

    public function getTypeTextAttr($value, $data)
    {
        $arr = config('base.commission_type');
        return $arr[$data['type']];
    }

    public function user()
    {
        return $this->belongsTo('Member', 'uid', 'id')->bind(['nickname']);
    }
}

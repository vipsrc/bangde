<?php
namespace app\common\model;

use think\Model;

class Withdraw extends Model
{
    protected $autoWriteTimestamp = true;
    protected $append = ['status_text'];

    public function user()
    {
    	return $this->belongsTo('Member','uid','id')->bind(['nickname']);
    }

    public function getStatusTextAttr($value,$data)
    {
    	$arr = config('base.withdraw_status');
    	return $arr[$data['status']];
    }
}

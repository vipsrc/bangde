<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 20/3/19
 * Time: 下午4:32
 */

namespace app\common\model;


use think\Model;

class Forum extends Model
{
    protected $autoWriteTimestamp = true;

    public function member()
    {
        return $this->belongsTo('Member','uid','id')->bind(['nickname','avatar']);
    }

}
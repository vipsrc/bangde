<?php
/**
 * 秋水
 * 2018/11/28
 * 总公共控制类
 */
namespace app\common\controller;

use \think\Controller;
use think\Db;
use EasyWeChat\Factory;

class Common extends Controller
{
	static function editIntegration($uid,$amount,$type,$remark='')
    {
        model('Member')->where(['id'=>$uid])->setInc('integration',$amount);
        model('IntegrationLog')->isUpdate(false)->save(['uid'=>$uid,'type'=>$type,'amount'=>$amount,'remark'=>$remark]);
//        $this->sendMessage('',$template,$url);
    }

    static function editCommission($uid,$amount,$type,$remark='')
    {
        model('Member')->where(['id'=>$uid])->setInc('commision',$amount);
        model('CommissionLog')->isUpdate(false)->save(['uid'=>$uid,'type'=>$type,'amount'=>$amount,'remark'=>$remark]);
    }

    static function getAllChild($cid,$result=[])
    {
        $cate = Db::name('Cate')->where(['id'=>$cid])->find();
        if(!$cate){
            return false;
        }
        $result[] = $cid;
        $child = Db::name('Cate')->where(['parentid'=>$cid])->column('id');
        if($child){
            foreach($child as $k=>$v){
                self::getAllChild($v,$result);
            }
        }else{
            $result = array_merge($child,$result);
        }
        return $result;
    }

    static function sendMessage($touid,$template,$data=[],$miniprogram=[],$url='')
    {
        $config = [
            'app_id' => config('site.wx_gzh_appid'),
            'secret' => config('site.wx_gzh_screte'),
            'response_type' => 'array',
        ];
        $user = model('Member')->where(['id'=>$touid])->find();
        if(!$user or empty($user['wx_openid'])){
            return false;
        }else{
            $openid = $user['wx_openid'];
        }
        $app = Factory::officialAccount($config);
        $app->template_message->send([
            'touser' => $openid,
            'template_id' => $template,
            'data' => $data,
            'miniprogram' => $miniprogram,
            'url' => $url,
        ]);
    }

}

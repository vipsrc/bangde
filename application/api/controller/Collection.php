<?php
namespace app\api\controller;

use think\Db;

class Collection extends Common
{
	public function getCollectionByUid()
	{
		$param = input();
		$uid = $this->uid;
		$list = model('Collection')->with('goods')->where([['uid','eq',$uid]])->select()->each(function($item,$key){
            $item['goods']['img'] = config('site.site_url').$item['goods']['img'];
        });
		return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
	}

	public function collect()
	{
		$param = input();
		$uid = $this->uid;

		$has = model('Collection')->where([['uid','eq',$uid],['gid','eq',$param['gid']]])->find();
		if($has){
			$re = model('Collection')->where('id',$has['id'])->delete();
			if(false !== $re){
				return json(['code'=>0,'msg'=>'取消成功']);
			}else{
				return json(['code'=>40001,'msg'=>'取消失败']);
			}
		}else{
			$re = model('Collection')->save(['uid'=>$uid,'gid'=>$param['gid']]);
			if(false !== $re){
				return json(['code'=>0,'msg'=>'收藏成功']);
			}else{
				return json(['code'=>40001,'msg'=>'取消失败']);
			}
		}
	}
}
?>
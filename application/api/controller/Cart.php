<?php
namespace app\api\controller;

class Cart extends Common
{

    /**
     * [putCart 添加购物车]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function putCart()
    {

        $param = input();
        $rule  = [
            'gid|商品ID'     => 'require|number',
            'goods_sn|商品号' => 'require',
            'num|数量'       => 'require|number',
            'goods_attr|属性名'=>'require'
        ];
        $validate = $this->validate($param, $rule);
        if (true != $validate) {
            return json(['code' => 44001, 'msg' => '参数错误', 'desc' => $validate]);
        }
        $goods = db('Goods')->where(['id' => $param['gid']])->find();
        if (!$goods) {
            return json(['code' => 44001, 'msg' => '参数错误', 'desc' => '未查询到该商品']);
        }
        list($gid, $goods_attr) = explode('#', $param['goods_sn']);
        $data                    = [];
        $map                     = [];
        $map                     = [
            'uid'      => $this->uid,
            'gid'      => $goods['id'],
            'goods_sn' => $param['goods_sn'],
        ];
        $data = array(
            'goods_sn'   => $param['goods_sn'],
            'gid'        => $param['gid'],
            'num'        => $param['num'],
            'goods_attr' => $param['goods_attr'],
            'uid'        => $this->uid,
        );
        $cartgoodsnum = db('cart')->where($map)->value('num'); //获取购物车中商品数量

        if (!$cartgoodsnum) {
            //判断一下，加入购物车的数量是否在库存范围内
            // $this->is_enough($param['goods_sn'], $goods['num'], $param['num']);
            //如果购物车里面没有，新增
            $re1 = db('cart')->insertGetId($data);
            $cart_id = $re1;
        } else {
            //如果购物车里面有，增加
            //判断一下，加入购物车的数量是否在库存范围内
            // $this->is_enough($_POST['goods_sn'], $goods['num'], $_POST['num'] + $cartgoodsnum);
            if ($param['num'] <= 0) {
                $re1 = db('cart')->where($map)->delete();
            } else {
                $re1 = db('cart')->where($map)->setField('num', $param['num']);
            }
            $cart_id = db('cart')->where($map)->value('id');
        }

        if ($re1 !== false) {
            // $cart     = db('cart')->where(['uid' => $this->uid])->select();
            // $cartdata = $this->getCartData($cart);
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '添加成功', 'data' => $cart_id]);
        } else {
            return json(['code' => 44005, 'desc' => '系统错误']);
        }
    }

    /**
     * [emptyCart 清空购物车]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [array]     [description]
     */
    public function emptyCart()
    {
        $param = input();
        $map[] = ['uid','eq',$this->uid];
//        $map[] = ['id','in',$param['cart_id']];
        $re    = db('cart')->where($map)->delete();
        if ($re) {
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '已清空购物车']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败', 'desc' => '系统错误']);
        }
    }

    /**
     * [cart 购物车详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [array]     [data=>商品详情，total=>总计]
     */
    public function cart()
    {
        $param    = input();
        $cart     = [];
        $map[] = ['uid','eq',$this->uid];
        if(isset($param['cart_id'])){
            $map[] = ['id','in',$param['cart_id']];
        }
        $cart     = db('cart')->where($map)->select();
        $cartdata = $this->getCartData($cart);
        return json(['code' => 0, 'desc' => 'success', 'data' => $cartdata]);
    }

    public function getCart($cart_id=[])
    {
        $map[] = ['uid','eq',$this->uid];
        $map[] = ['id','in',$cart_id];
        $mycart = db('cart')->where($map)->select();
        return $this->getcartdata($mycart);
    }

    /***********************************************私有类**********************************************/

    /**
     * [getCartData 重组购物车数据]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @param    [array]     $cart [购物车原始数组]
     * @return   [array]           [重组后的数组]
     */
    private function getCartData($cart)
    {
        if(session('?uid')){
            $member = model('Member')->where('id',session('uid'))->find();
            $memberlevel = getLevel($member['role'],$member['totalConsume']);
            $discount = $memberlevel['discount'];
        }else{
            $discount = 100;
        }
        $cartdata['data']                 = [];
        $cartdata['total']['num']         = 0;
        $cartdata['total']['totalAmount'] = 0;
        $cartdata['total']['delivery_fee'] = 0;
        if (!empty($cart)) {
            foreach ($cart as $k => $v) {
                $goods[$k] = db('Goods')->where(['id' => $v['gid']])->find();
                // 商品状态判断是否可购买
                if (!$goods[$k] || $goods[$k]['status'] !== 1) {
                    db('cart')->where(['id' => $v['id']])->delete();
                    unset($cart['data'][$k]);
                    continue;
                }
                //如果该商品存在限时抢购，切换价格
                if (strtotime($goods[$k]['end_time']) >= time() && strtotime($goods[$k]['start_time']) <= time() && $goods[$k]['is_promote'] == 1) {
                    $goods[$k]['price'] = $goods[$k]['promote_price'];
                }
                $goods[$k]['img'] = config('site.site_url') . $goods[$k]['img'];
                if (strpos($v['goods_sn'], '#') !== false) {
                    $goods_sn_arr = explode('#', $v['goods_sn']);
                    unset($goods_sn_arr[0]);
                    foreach ($goods_sn_arr as $kk => $vv) {
                        $attr[$k][$kk] = db('goods_attr')->where(['goods_attr_id' => $vv])->find();
                        $attr[$k][$kk] && $goods[$k]['price'] += $attr[$k][$kk]['attr_price']*$discount/100;
                    }
                }
                $cartdata['data'][$k]           = $v;
                $cartdata['data'][$k]['goods']  = $goods[$k];
                $cartdata['data'][$k]['amount'] = round($goods[$k]['price'] * $v['num']*$discount/100, 2);
                $cartdata['total']['num'] += $v['num'];
                $cartdata['total']['totalAmount'] += $cartdata['data'][$k]['amount'];
                $cartdata['total']['delivery_fee'] += $goods[$k]['delivery_fee'];
            }
        }
        return $cartdata;
    }
}

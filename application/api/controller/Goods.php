<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2018/12/5
 * Time: 16:15
 */
namespace app\api\controller;

use Db;
use think\Cache;
use think\Controller;
use think\Request;

class Goods extends Common
{

    /***
     * 获取服务分类列表
     */
    public function categoryList()
    {
        $where           = [];
        $categoryList    = db('Cate')->where($where)->order('sort desc')->field('id,name,parentid,icon')->select();
        foreach($categoryList as $k=>$v){
            $categoryList[$k]['icon'] = config('site.site_url').$v['icon'];
        }
        $arr = cate_merge($categoryList,null,0,'id','parentid');
        $result['data']  = $arr;
        $result['count'] = count($categoryList);
        $result['code']  = 0;
        $result['msg']   = 'success';
        return json($result);
    }


    /**
     * 商品列表数据
     * @author 雨夜
     * @date 2018年12月7日09:10:56
     * @return \think\response\Json
     */
    public function goodsList()
    {
        $param = input();
        $discount = 100;
        if(session('?uid')){
            $member = model('Member')->where('id',session('uid'))->find();
            $memberlevel = getLevel($member['role'],$member['totalConsume']);
            $discount = $memberlevel['discount'];
        }
        $map   = [];
        $map[] = ['is_del', 'eq', '0'];
        $map[] = ['status', 'eq', '1'];
        if(isset($param['type']) and $param['type'] == 'recom'){
            $map[] = ['is_recom','eq',1];
        }
        if(isset($param['type']) and $param['type'] == 'cart_recom'){
            $map[] = ['cart_recom','eq',1];
        }
        if(isset($param['cate']) and $param['cate']){
            $cate = [];
            $cate1 = Db::name('Cate')->where(['id'=>$param['cate']])->find();
            if($cate1['parentid'] == 0){
                $cate2 = Db::name('Cate')->where(['parentid'=>$cate1['id']])->column('id');
                foreach($cate2 as $k=>$v){
                    $cate3 = Db::name('Cate')->where(['parentid'=>$v])->column('id');
                    $cate = array_merge($cate,$cate3);
                }
            }else{
                $cate = Db::name('Cate')->where(['parentid'=>$param['cate']])->column('id');
                $cate[] = $param['cate'];
                $map[] = ['cid','in',$cate];
            }
            $map[] = ['cid','in',$cate];
        }
        if(isset($param['keywords']) and $param['keywords']){
            $map[] = ['name','like','%'.$param['keywords'].'%'];
        }
        $list  = model('Goods')->where($map)->page(input('page',1), input('limit',10))->order('sort desc')->select()->toArray();
        foreach($list as $k=>$v)
        {
            $list[$k]['img'] = config('site.site_url').$v['img'];
            $list[$k]['price'] = round($v['price']*$discount/100,2);
        }
        $count = model('Goods')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }



    /**
     * [getGoodsInfo 获取商品详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getGoodsInfo()
    {
        $param = input();
        $info = model('Goods')->with(['gallery','goodsAttr','cate','goodsAttr.attribute'])->where(['id'=>$param['gid']])->find();
        if(!$info){
            return json(['code'=>44001,'msg'=>'参数错误','desc'=>'未找到该商品']);
        }
        if(session('?uid')){
            $member = model('Member')->where('id',session('uid'))->find();
            $memberlevel = getLevel($member['role'],$member['totalConsume']);
            $discount = $memberlevel['discount'];
        }else{
            $discount = 100;
        }
        $info['price'] = round($info['price']*$discount/100,2);
        $info['star'] = Db::name('Comment')->where(['gid'=>$info['id']])->avg('goods_star');

        $info['parameter'] && $info['parameter'] = json_decode($info['parameter'],true);
        $info['img'] = config('site.site_url').$info['img'];
        $info['infor'] = str_replace('src="', 'src="'.config('site.site_url'), $info['infor']);
        foreach($info['gallery'] as $k=>$v){
            $info['gallery'][$k] = config('site.site_url').$v['img_url'];
        }
        $attr = [];
        foreach($info['goods_attr'] as $k=>$v)
        {
            if(isset($attr[$v['attr_id']])){
                unset($v['attribute']);
                $attr[$v['attr_id']]['values'][] = $v;
            }else{
                $attr[$v['attr_id']]['name'] = $v['attribute']['attr_name'];
                unset($v['attribute']);
                $attr[$v['attr_id']]['values'][] = $v;
            }
        }
        unset($info['goods_attr']);
        $info['attribute'] = $attr;
        if($this->uid){
            $collection = Db::name('Collection')->where([['uid','eq',$this->uid],['gid','eq',$info['id']]])->find();
            $info['is_collect'] = $collection ? 1 : 0;
        }else{
            $info['is_collect'] = 0;
        }
        return json(['code'=>0,'msg'=>'请求成功','desc'=>'','data'=>$info]);

    }

    public function getGoodsVideo(Request $request)
    {
        $param = $request->param();
        $list = model('GoodsVideo')->where(['gid'=>$param['gid']])->page($param['page'],$param['limit'])->order('id desc')->select()->each(function($item,$key){
//            $item['video'] = config('site.qiniu_domain').$item['video'];
            return $item;
        });
        $count = model('GoodsVideo')->where(['gid'=>$param['gid']])->count();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: 江远
 * Date: 2018/11/30
 * Time: 18:46
 */
namespace app\api\controller;

use think\Controller;
use EasyWeChat\Factory;
use think\Db;
use think\Request;
use think\facade\Cache;

class Login extends Controller
{
    public function login(Request $request)
    {
        $param = $request->param();
        $pid = input('?pid') ? input('pid') : 0;
        $iv = input('iv');
        $encryptData = input('encryptedData');

        $config = [
            'app_id' => config('site.wx_appid'),
            'secret' => config('site.wx_screte'),
            'response_type' => 'array',
        ];
        $app = Factory::miniProgram($config);
        $result = $app->auth->session($param['code']);
        if(isset($result['openid']) and isset($result['session_key'])){
            $decryptedData = $app->encryptor->decryptData($result['session_key'], $iv, $encryptData);
            if(!$decryptedData || !isset($decryptedData['unionId'])){
                return json(['code'=>40001,'msg'=>'获取用户信息失败']);
            }
            $unionId = $decryptedData['unionId'];
            $member = model('Member')->where(['union_id'=>$unionId])->find();
            if($member){
                $re = db('Member')->where(['union_id' => $unionId])->update(['avatar' => $param['headimgurl'],'nickname'=>$param['nickname'],'openid'=>$result['openid']]);
                if (false !== $re) {
                    $uid = $member['id'];
                }
            }else{
                $re = Db::name('Member')->insertGetId([
                    'openid' => $result['openid'],
                    'avatar' => $param['headimgurl'],
                    'nickname' => $param['nickname'],
                    'realname' => $param['nickname'],
                    'union_id'=>$unionId,
                    'create_time' => time(),
                    'update_time' => time(),
                    'pid' => $pid,
                ]);
                if (false !== $re) {
                    $uid = $re;
                } else {
                    return json(['code' => 50000, 'msg' => '登录失败']);
                }
            }
            if($member['login_token']){
                $token = $member['login_token'];
            }else{
                $token = authcode($uid, 'ENCODE');
            }
            $set = db('Member')->where(['id' => $uid])->setField('login_token', $token);
            if (false !== $set) {
                return json(['code' => 0, 'msg' => '登录成功', 'data' => $token,'uid'=>$uid]);
            } else {
                return json(['code' => 50000, 'msg' => '登录失败']);
            }
        }else{
            return json(['code'=>40001,'msg'=>$result['errmsg']]);
        }
    }

    /**
     * 发送短信
     * @param phone
     * @param type 1 注册 2 其他
     * @return \think\response\Json
     */
    public function sendmsg(Request $request)
    {
        $param = $request->param();
        if (empty($param['mobile'])) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        $phone = $param['mobile'];
        $type = $param['type'] ? $param['type'] : 1;
        if ($type == 1) {
            $isregister = db('member')->where(['mobile' => $phone])->find();
            if ($isregister) {
                return json(['code' => 44004, 'msg' => '手机号已被注册，请重试！']);
            }
        }
        //带频率控制的验证码
        $success = true;
        $msg = '发送成功，请注意查看';
        $cacheCode = Cache::get($phone . '_login_code');
        //控制发短信的频率
        if (!empty($cacheCode)) {
            $info = json_decode($cacheCode, true);
            if (!empty($info) && time() - $info['time'] < 59) {
                $success = false;
                $msg = '60秒内不允许重复发';
                return json(['code' => 50000, 'msg' => $msg]);
            }
        }
        if ($success) {
            //通过校验，则开始发信
            $sms_code = rand(100000, 999999);
            $response = \SMS::sendSms(config('site.sms_sign'), config('site.sms_templet'), $phone, ['code' => $sms_code]);
            if ($response->Code == 'OK') {
                //记录发信时间
                Cache::set($phone . '_login_code', json_encode(['code' => $sms_code, 'time' => time()]), 300);
                return json(['code' => 0, 'msg' => $msg, 'data' => $sms_code]);
            } else {
                Cache::set($phone . '_login_code', json_encode(['code' => $sms_code, 'time' => time()]), 300);
                $success = false;
                $msg = $response->Message;
                return json(['code' => 50000, 'msg' => '本日次数已达上限，无法发送', 'data' => $sms_code]);
            }
        }
    }

}

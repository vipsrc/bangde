<?php
namespace app\api\controller;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use think\Request;
use EasyWeChat\Factory;
use think\Db;

class Index extends Common
{
    /**
     * [base64Upload 图片base64上传]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function base64Upload()
    {
        $re = base64_image_content(input('file'),'Uploads/'.date('Y-m-d'));
        if($re){
            return json(['code'=>0,'msg'=>'保存成功','data'=>$re]);
        }else{
            return json(['code' => 40001, 'msg' =>'失败']);
        }
    }

    /**
     * [imgUpload 图片上传]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function imgUpload()
    {
        $file = request()->file('file');
        if (true !== $this->validate(['file' => $file], ['file' => 'require|image'])) {
            $this->error('请选择图像文件');
        } else {
            $info = $file->validate(['size' => 1204 * 1204 * 10, 'ext' => 'jpg,png,gif'])->move('Uploads');
            if ($info) {
                // 成功上传后 获取上传信息
                $savename = '/Uploads/' . $info->getSaveName();
                return json(['code' => 0, 'msg' => '上传成功', 'data' => $savename]);
            } else {
                // 上传失败获取错误信息
                $error = $file->getError();
                return json(['code' => 40001, 'msg' => $error]);
            }
        }
    }


    /**
     * [textImgUpload 文本图片上传]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function textImgUpload()
    {
        $file = request()->file('file');
        if (true !== $this->validate(['file' => $file], ['file' => 'require|image'])) {
            $this->error('请选择图像文件');
        } else {
            $info = $file->validate(['size' => 1204 * 1204 * 10, 'ext' => 'jpg,png,gif'])->move('Uploads');
            if ($info) {
                // 成功上传后 获取上传信息
                $savename = '/Uploads/' . $info->getSaveName();
                return json(['code' => 0, 'msg' => '上传成功', 'data' => ['src' => $savename]]);
            } else {
                // 上传失败获取错误信息
                $error = $file->getError();
                return json(['code' => 40001, 'msg' => $error]);
            }
        }
    }


    /**
     * [videoUpload 视频上传]弃用
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @return   [type]     [description]
     */
    public function videoUpload()
    {
        die;
        try{
            $validate = $this->validate(['file'=>request()->file('file')],['file'=>'require|file|fileExt:mp4,mov,avi']);
            if(true !== $validate){
                return json(['code'=>44001,'msg'=>$validate]);
            }
            $values = array_values($_FILES);
            $auth   = new Auth(config('site.qiniu_accessKey'), config('site.qiniu_secretKey'));
            $token = $auth->uploadToken(config('site.qiniu_bucket'));
            $uploadMgr = new UploadManager();
            list($ret, $err) = $uploadMgr->putFile($token,'video'.time(),$values[0]['tmp_name']);
            if ($err !== null) {
                return json(['code'=>40001,'msg'=>'上传出错'.serialize($err)]);
            }else{
                return json(['code'=>0,'msg'=>'上传成功','data'=>$ret['key']]);
            }
            
        }catch (Exception $e){
            return json(['code'=>40001,'msg'=>$e->getMessage()]);
        }
    }

    public function qiniuToken()
    {
        $auth   = new Auth(config('site.qiniu_accessKey'), config('site.qiniu_secretKey'));
        $returnBody = '{"key":"$(key)","hash":"$(etag)","mimeType":$(mimeType),"bucket":"$(bucket)","name":"$(x:name)"}';
        $policy = array(
            'returnBody' => $returnBody
        );
        $token = $auth->uploadToken(config('site.qiniu_bucket'),null,3600,$policy,true);
        return json(['code'=>0,'msg'=>'请求成功','uptoken'=>$token,'qiniu_domain'=>config('site.qiniu_domain')]);

    }
    

    /**
     * [locationToAddress 坐标转地址]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
//    public function locationToAddress()
//    {
//        $param = input();
//        if(!isset($param['lon']) || !isset($param['lat'])){
//            return json(['code'=>44001,'msg'=>'参数错误']);
//        }
//        $url = 'https://apis.map.qq.com/ws/geocoder/v1/?location='.$param['lat'].','.$param['lon'].'&key='.config('site.map_key');
//        $result = http_request($url);
//        $result = json_decode($result,true);
//        if($result['status']!==0){
//            return json(['code'=>40001,'msg'=>$result['message']]);
//        }else{
//            return json(['code'=>0,'msg'=>'请求成功','data'=>$result['result']]);
//        }
//    }

    public function getProvince()
    {
        $province = Db::name('Region')->where(['region_type'=>1])->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$province]);
    }

    public function getCity(Request $request)
    {
        $map = [];
        $map[] = ['parent_id','=',$request->param('province')];
        $map[] = ['region_type','=',2];
        $city = Db::name('Region')->where($map)->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$city]);
    }

    public function getCounty(Request $request)
    {
        $map = [];
        $map[] = ['parent_id','=',$request->param('city')];
        $map[] = ['region_type','=',3];
        $county = Db::name('Region')->where($map)->select();
        if(!$county){
            $county = Db::name('Region')->where(['region_id'=>$request->param('city')])->select();
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>$county]);
    }
    
    
    public function locationToAddress(Request $request)
    {

        $param = $request->param();
        if(!isset($param['location'])){
            return json(['code'=>44004,'msg'=>'定位坐标不能为空']);
        }
        $url = 'https://apis.map.qq.com/ws/geocoder/v1/?location='.$param['location'].'&key='.config('site.map_key');

        $re = http_request($url);

        if(false == $re){
            return json(['code'=>40001,'msg'=>'参数错误']);
        }
        $re = json_decode($re,true);
        if($re['status']==0){
            $adcode = $re['result']['ad_info']['adcode'];
            $address = $re['result']['address'];
            $region = model('Region')->where(['areaCode'=>$adcode])->find();
            if($region){
                if($region['region_type']==3){
                    $region['city'] = model('Region')->where(['region_id'=>$region['parent_id']])->find();
                    $region['province'] = model('Region')->where(['region_id'=>$region['city']['parent_id']])->find();
                }else if($region['region_type']==2){
                    $region['province'] = model('Region')->where(['region_id'=>$region['parent_id']])->find();
                }
                return json(['code'=>0,'msg'=>'请求成功','data'=>$region,'address'=>$address]);
            }else{
                return json(['code'=>44001,'msg'=>'所在地区暂未开通业务，请重新选择！']);
            }
        }else{
            return json(['code'=>40001,'msg'=>$re['message']]);
        }
    }


}

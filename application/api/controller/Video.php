<?php
namespace app\api\controller;

use think\Request;

class Video extends Common
{

	/**
	 * [videoCate 视频分类]
	 * @Author   雨夜
	 * @DateTime 2018-12-18
	 * @return   [type]     [description]
	 */
	public function videoCate()
	{
		$param = input();
		$list = model('VideoCate')->page($param['page'],$param['limit'])->order('sort desc')->select();
		$count = model('Video')->count();
		return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
	}

	/**
	 * [getVideoList 视频列表数据]
	 * @Author   雨夜
	 * @DateTime 2018-12-18
	 * @return   [type]     [description]
	 */
	public function getVideoList()
	{
		$param = input();
		$map = [];
		if(isset($param['cate']) and $param['cate']){
			$map[] = ['cate','eq',$param['cate']];
		}
		if(isset($param['keywords']) and $param['keywords']){
			$map[] = ['title|summary','like','%'.$param['keywords'].'%'];
		}
		$list = model('Video')->where($map)->page($param['page'],$param['limit'])->order('sort desc')->select()->each(function($item,$key){
			$item['video_url'] = config('site.qiniu_domain').$item['video_url'];
			return $item;
		});
		$count = model('Video')->count();
		return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
	}

    /**
     * @author 雨夜
     * @date 2019年3月19日10:31:25
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
	public function getVideoInfo()
	{
		$param = input();
		$map[] = ['id','eq',$param['id']];
		$info = model('video')->where($map)->find();
		$info['video_url'] = config('site.qiniu_domain').$info['video_url'];
		if($info){
			return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
		}else{
			return json(['code'=>40001,'msg'=>'系统错误']);
		}
	}


	public function getSlide()
    {
        $post = input();
        $list = model('slide')->page($post['page'], $post['limit'])->order('sort desc')->select()->toArray();
        foreach($list as $k=>$v){
            $list[$k]['img_url'] = config('site.site_url').$v['img'];
        }
        if ($list) {
            $result['data'] = $list;
            $result['count'] = db('slide')->count();
            $result['code'] = 0;
            return json($result);
        } else {
            return json(['code' => 205, 'msg' => '暂无数据']);
        }
    }

}
?>
<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 22/3/19
 * Time: 上午10:20
 */

namespace app\api\controller;

use think\facade\Request;
use think\Db;
use app\common\controller\Common as Tool;


class Forum extends Common
{
    /**
     * 返回分类数据
     */
    public function getForumCate(){
        $forumCate = Db::name('forum_cate')->select();
        $count = count($forumCate);
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$forumCate]);
    }
    /**
     * 帖子前台列表
     */
    public function getForumList()
    {
        $param = input();
        $page = input('page', 1);
        $limit = input('limit', 15);
        $map = [];
        if(input('?cate')){
            $map[] = ['cid','eq',$param['cate']];
        }

        $list = model('Forum')->with('member')->where($map)->page($page,$limit)->order('create_time desc')->select();
        foreach($list as $k=>$v){
            $list[$k]['reply_num'] = model('Reply')->where(['forum_id'=>$v['id'],'pid'=>0])->count();
            $list[$k]['is_praise'] = model('Praise')->where(['uid'=>session('uid'),'forum_id'=>$v['id'],'type'=>0])->find() ? 1 : 0;
            $v['imgs'] && $list[$k]['imgs'] = explode(',',$v['imgs']);
            $v['video'] && $list[$k]['video'] = explode(',',$v['video']);
        }
        $count = model('Forum')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
    }


    public function getForum()
    {
        $info = model('Forum')->with('member')->where(['id'=>input('forum_id')])->find();
        if($info){
            $info['is_praise'] = model('Praise')->where(['uid'=>session('uid'),'forum_id'=>$info['id'],'type'=>0])->find() ? 1 : 0;
            $info['imgs'] && $info['imgs'] = explode(',',$info['imgs']);
            $info['video'] && $info['video'] = explode(',',$info['video']);
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * 发帖
     */
    public function saveForum()
    {

        $param = input();
        $param['uid'] = session('uid');
        $rule = [
            'title|标题'=>'require',
            'content|内容'=>'require',
            'cid'=>'require'

        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
        if(isset($param['id'])){

            $re = model('Forum')->isUpdate(true)->allowField(true)->save($param);
        }else{

            $re = model('Forum')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){

            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>44005,'msg'=>'保存失败']);
        }
    }

    /**
     * 分享获得积分
     */
    public function shareForum(){

        //用户id 帖子id
        $param = input();

        $map[] = ['forum_id','=',$param['forum_id']];
        $sh = model('Share')->where($map)->find();
        if (!$sh){
            model('Member')->where('id',session('uid'))->setInc('integration',config('base.share_int'));
            model('Share')->save(['uid'=>session('uid'),'forum_id'=>$param['forum_id']]);
            $amount = config('site.share_forum_integration');
            $remark = '分享论坛获取'.$amount.'积分';
            Tool::editIntegration(session('uid'),$amount,5,$remark);
            return json(['code'=>0,'msg'=>'分享成功']);
        }else{
            return json(['code'=>44005,'msg'=>'您已经分享过']);
        }

    }

    /**
     * 用户点赞
     */
    public function praise(\app\common\model\Praise $praise){
        $data = Request::param();
        $map[] = ['uid','=',session('uid')];
        $map[] = ['forum_id','=',$data['id']];
        $map[] = ['type','=',$data['type']];
        $p = $praise->where($map)->find();
        if (!$p){
            $praise->save([
                'uid'=>session('uid'),
                'forum_id'=>$data['id'],
                'type'=>$data['type']
            ]);
            if($data['type']==0){
                Db::name('forum')->where('id',$data['id'])->setInc('praise');
            }else{
                Db::name('forum_reply')->where('id',$data['id'])->setInc('praise');
            }
            return json(['code'=>0,'msg'=>'点赞成功']);
        }else{
            $praise->where($map)->delete();
            if($data['type']==0){
                Db::name('forum')->where('id',$data['id'])->setDec('praise');
            }else{
                Db::name('forum_reply')->where('id',$data['id'])->setDec('praise');
            }
            return json(['code'=>0,'msg'=>'取消点赞']);
        }
    }

    /**
     * 用户发表评论
     */
    public function reply(){

        $param = input();
        $where = [];
        $where[] = ['id','=',$param['forum_id']];
        $forumDate = model('Forum')->where($where)->find();

        $data['uid'] = session('uid');
        $data['forum_id'] = $param['forum_id'];
        $data['touid'] = $forumDate['uid'];
        $data['content'] = $param['content'];
        if ($param['pid']){
            $data['pid'] = $param['pid'];
        }else{
            $data['pid'] = 0;
        }

        $re = model('Reply')->save($data);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'评论成功']);
        }else{
            return json(['code'=>44005,'msg'=>'评论失败']);
        }


    }

    /**
     * 前端页面数据显示
     */
    public function getReplyList(){

        $data = $this->replyTree();
        $map = [];
        $map = ['forum_id'=>input('forum_id'),'pid'=>0];
        $count = model('Reply')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$data]);
    }

    public function getReplyChild()
    {
        $map = [];
        $map[] = ['forum_id','eq',input('forum_id')];
        $map[] = ['pid','eq',input('pid')];
        $reply = model('Reply')->with('member')->where(['id'=>input('pid')])->find();
        $list = model('Reply')->with('member')->where($map)->page(input('page',1),input('limit',10))->select();
        $count = model('Reply')->where($map)->count();
        if($list){
            return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'info'=>$reply,'count'=>$count]);
        }else{
            return json(['code'=>0,'msg'=>'请求成功','data'=>[],'info'=>$reply,'count'=>$count]);
        }

    }

    public function replyTree(){
        $page = input('page', 1);
        $limit = input('limit', 15);
        $map[] = ['forum_id','=',input('forum_id')];
        $map[] = ['pid','eq',0];
        $data = model('Reply')->with('member')->where($map)->page($page,$limit)->select();
        foreach($data as $k=>$v){
            $data[$k]['reply'] = model('Reply')->with('member')->where(['pid'=>$v['id']])->select();
            $data[$k]['is_praise'] = model('Praise')->where(['uid'=>session('uid'),'forum_id'=>$v['id'],'type'=>1])->find() ? 1 : 0;
        }
        return $data;
    }


}
<?php
namespace app\api\controller;

use app\common\model\Coupon as CouponModel;
use think\Db;
use think\facade\Request as Request;

class Coupon extends Common
{

    /**
     * [receiveCoupon 用户领取优惠券]
     * @Author   青阳
     * @DateTime 2019-05-23
     * 优惠券表数据 存 用户领取优惠券表
     * /api/coupon/receivecoupon
     * @return   [type]     [description]
     */
    public function receiveCoupon()
    {
        $param              = Request::param();
        $param['member_id'] = session('uid') ?: -1;
        $rule               = [
            'member_id|领券人不能为空' => 'require', // 领取人
            'id|查不到优惠券'         => 'require', // 优惠券id
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44001, 'msg' => $validate]);
        }
        $coupon = Db::name('Coupon')->where('id', 'eq', $param['id'])->find();
        $has = Db::name('UserCoupon')->where(['coupon_id'=>$coupon['id'],'member_id'=>session('uid')])->find();
        if($has){
            return json(['code'=>44001,'msg'=>'已领取过该优惠券']);
        }
        $return = ['code' => 44001, 'msg' => '该优惠券已经失效'];
        if ($coupon && (time() <= $coupon['end_time'])) {
            $u_param['status']       = 0; // -1过期；0未使用；1已使用
            $u_param['coupon_value'] = $coupon['amount']; // 优惠券金额
            $u_param['coupon_id'] = $coupon['id']; // 优惠券金额
            $u_param['name']         = $coupon['name']; // 优惠券名称
            $u_param['limit_amount'] = $coupon['limit_amount']; // 最低消费
            $u_param['end_time']     = ($coupon['use_time'] * 86400) + time(); // 用户领到的过期时间以用户领取时间+优惠券有效期为准
            $u_param['member_id']    = $param['member_id'];
            $param                   = '';
            $return                  = ['code' => 0, 'msg' => '优惠券领取成功'];
            if (!Db::name('UserCoupon')->insertGetId($u_param)) {
                $return = ['code' => 44001, 'msg' => '优惠券领取失败'];
            }
        }

        return json($return);
    }

    /**
     * [couponList] 可领取优惠券
     * @author 雨夜
     * @date 2019/5/24
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function couponList()
    {
        $param = Request::param();
        $list = Db::name('coupon')->where([['start_time','<=',time()],['end_time','>=',time()]])->select();
        foreach($list as $k=>$v){
            $list[$k]['end_time'] = Date('Y-m-d',$v['end_time']);
            $ishas[$k] = Db::name('UserCoupon')->where(['member_id'=>session('uid'),'coupon_id'=>$v['id']])->find();
            $list[$k]['is_has'] = $ishas[$k] ? '1':'0';
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }
}

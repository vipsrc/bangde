<?php
namespace app\api\controller;

class Sysconfig extends Common
{

	/**
	 * [getSysConfig 获取系统配置]
	 * @Author   雨夜
	 * @DateTime 2019-01-05
	 * @return   [type]     [description]
	 */
	public function getSysConfig()
	{
		$config = config('site.');
		//$config['about_join'] = str_replace('src="', 'src="'.$config['site_url'], $config['about_join']);
		return json(['code'=>0,'msg'=>'请求成功','data'=>$config]);
	}


	/**
	 * [getContacts 获取客服数据]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [type]     [description]
	 */
	public function getContacts()
	{
		$param = input();
		$list = model('Contacts')->page($param['page'],$param['limit'])->select();
		$count = model('Contacts')->count();
		return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
	}

	/**
	 * [addContacts 添加客服]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 */
	public function saveContacts()
	{
		$param = input();
		$rule = [
			'nickname|昵称'=>'require',
			'wx_account|微信号'=>'require',
			'mobile|手机号'=>'require|mobile'
		];
		$validate = $this->validate($param,$rule);
		if(true !== $validate){
			return json(['code'=>44001,'msg'=>'参数错误','desc'=>$validate]);
		}
		if(isset($param['id'])){
			$re = model('Contacts')->isUpdate(true)->save($param);
		}else{
			$re = model('Contacts')->save($param);
		}
		
		if(false != $re){
			return json(['code'=>0,'msg'=>'请求成功','desc'=>'添加成功']);
		}else{
			return json(['code'=>44005,'msg'=>'创建失败','desc'=>'系统错误']);
		}
	}

	/**
	 * [getContactsById 根据id获取客服详情]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [type]     [description]
	 */
	public function getContactsById()
	{
		$param = input();
		$info = model('Contacts')->get($param['id']);
		return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
	}


	/**
	 * [delContacts 删除客服]
	 * @Author   雨夜
	 * @DateTime 2018-12-18
	 * @return   [type]     [description]
	 */
	public function delContacts()
	{
		$param = input();
		$re = db('contacts')->where(['id'=>$param['id']])->delete();
		if(false !== $re){
			return json(['code'=>0,'msg'=>'删除成功']);
		}else{
			return json(['code'=>44004,'msg'=>'删除失败']);
		}
	}

}
?>
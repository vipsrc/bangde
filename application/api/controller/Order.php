<?php
namespace app\api\controller;

use app\api\controller\Cart;
use think\Db;
use think\Request;

class Order extends Common
{
    /**
     * [orderList 订单列表]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function orderList()
    {
        $param = input();
        $map   = [];
        $map[] = ['uid', 'eq', session('uid')];
        $map[] = ['is_delete','eq','0'];
        $map[] = ['status','neq','-1'];
        if (isset($param['order_no'])) {
            $map[] = ['order_no', 'like', '%' . $param['order_no'] . '%'];
        }
        if (isset($param['status']) and $param['status']!=='') {
            $map[] = ['status', 'eq', $param['status']];
        }
        if (isset($param['pay_state']) and $param['pay_state']!=='') {
            $map[] = ['pay_state', 'eq', $param['pay_state']];
        }
        if (isset($param['refund']) and $param['refund']!=='0') {
            $map[] = ['refund', 'neq', '0'];
        }

        $list  = model('Order')->with(['OrderGoods','OrderGoods.Goods'])->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->toArray();
        foreach($list as $k=>$v){
            foreach($v['order_goods'] as $kk=>$vv){
                $list[$k]['order_goods'][$kk]['goods']['img'] = config('site.site_url').$vv['goods']['img'];
            }
        }
        $count = model('Order')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $list, 'count' => $count]);
    }

    /**
     * [getOrderInfo 订单详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getOrderInfo()
    {
        $param = input();
        $map   = [];
        $map[] = ['id', 'eq', $param['id']];
        $info  = model('Order')->with('OrderGoods,OrderGoods.Goods')->where($map)->find()->toArray();
        if($info['status']=='0'){
            $info['pay_limit'] = strtotime($info['create_time'])+(config('site.pay_limit')*3600);
        }
        if($info['status']=='2'){
            $info['recieve_limit'] = $info['pay_time']+(config('site.recieve_limit')*86400);
        }
        $num = 0;
        foreach($info['order_goods'] as $k=>$v){
            $info['order_goods'][$k]['goods']['img'] = config('site.site_url').$v['goods']['img'];
            $comment = Db::name('comment')->where(['order_no'=>$info['order_no'],'gid'=>$v['gid']])->find();
            if($comment){
                $info['ordergodos'][$k]['comment'] = $comment;
            }
            $num+=$v['num'];
        }
        $info['total']['goods_num'] = $num;
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $info]);
    }


    public function getOrderGoods(Request $request)
    {
        $param = $request->param();
        $ordergoods = model('OrderGoods')->with('goods')->where(['id'=>$param['order_goods_id']])->find()->toArray();
        if(!$ordergoods){
            return json(['code'=>44001,'msg'=>'参数错误,未找到订单信息']);
        }
        $ordergoods['order_info'] = model('Order')->where(['order_no'=>$ordergoods['order_no']])->find();
        return json(['code'=>0,'msg'=>'获取成功','data'=>$ordergoods]);
    }

    /**
     * [order 下单]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function order()
    {
        $param = input('post.');
        // 实例化模型
        $Order       = model('Order');
        $Ordergoods  = model('OrderGoods');
        $Goods       = model('Goods');
        $UserAddress = model('UserAddress');
        $User        = model('Member');
        $userinfo    = $User->find($this->uid);
        if (!$param['address_id']) {
            return json(['code' => 44001, 'msg' => '请填写收货信息', 'desc' => '请填写收货信息']);
        }
        $default_address = $UserAddress->with('province,city,county')->where(array('uid' => $this->uid, 'id' => $param['address_id']))->find();
        if (!$default_address) {
            return json(['code' => 44001, 'msg' => '收货地址未找到', 'desc' => '收货地址未找到']);
        }
        $cart_id = isset($param['cart_id']) ? $param['cart_id'] : [];
        //生成订单号
        $order_no = date('YmdHis') . $this->uid;
        
        //开始处理提交过来的订单商品
        $cart        = new Cart();
        $mycartgoods = $cart->getCart($cart_id);
        foreach ($mycartgoods['data'] as $k => $v) {
            $ordergoodsdata[$k] = [
                'order_no'   => $order_no,
                'goods_sn'   => $v['goods_sn'],
                'gid'        => $v['gid'],
                'name'       => $v['goods']['name'],
                'price'      => $v['goods']['price'],
                'num'        => $v['num'],
                'goods_attr' => $v['goods_attr'],
            ];
        }
        Db::startTrans();
        $re1 = $Ordergoods->saveAll($ordergoodsdata);
        if (!$re1) {
            Db::rollback();
            return json(['code' => 44005, 'msg' => '创建失败', 'desc' => '订单商品写入失败']);
        }
        $left = $mycartgoods['total']['totalAmount'];
        if(isset($param['coupon']) and $param['coupon']){
            $coupon = Db::name('UserCoupon')->where(['id'=>$param['coupon']])->find();
            if(!$coupon){
                return json(['code'=>44004,'msg'=>'未找到优惠券信息']);
            }
            if($coupon['limit_amount']>$left){
                return json(['code'=>44004,'msg'=>'优惠券不适用！']);
            }
            if($coupon['end_time']<time()){
                return json(['code'=>44004,'msg'=>'优惠券已过期！']);
            }
            if($coupon['status']!==0){
                return json(['code'=>44004,'msg'=>'优惠券已使用！']);
            }
            $couponvalue = $coupon['coupon_value'] > $left ? $left : $coupon['coupon_value'];
            $left-=$couponvalue;
            $coupon_id = $coupon['id'];
        }else{
            $coupon_id = 0;
            $couponvalue = 0;
        }

        if($param['use_commision']){
            if($left>0){
                $mycommission = $userinfo['commision'];
                if($mycommission>=$left){
                    $use_commsion_num = $left;
                }else{
                    $use_commsion_num = $mycommission;
                }
                $left-=$use_commsion_num;
            }

        }else {
            $use_commsion_num = 0;
        }

        //判断一下是否使用了积分
        if ($param['use_integration'] == 1) {
            if($left>0){
                //计算应付总额 = 订单总额 - 可用积分*兑换比例 - 佣金抵扣
                $myIntegralNum = $userinfo['integration'];
                //可用积分换算到现金的金额
                $integratemoney = $myIntegralNum*config('site.integration_percent');
                if ($integratemoney > $left-$use_commsion_num) {
                    $integralCash = round($left-$use_commsion_num,2);
                    $use_inter    = ($left-$use_commsion_num)/config('site.integration_percent');
                } else {
                    $integralCash = $integratemoney;
                    $use_inter    = $myIntegralNum;
                }
            }
        } else {
            $integralCash = 0;
            $use_inter    = 0;
        }

        $pay_cash = $mycartgoods['total']['totalAmount']+$mycartgoods['total']['delivery_fee'] - $integralCash - $use_commsion_num-$couponvalue;
        $data     = array(
            'order_no'      => $order_no,
            'name'          => $default_address['consignee'],
            'tel'           => $default_address['tel'],
            'totalAmount'   => $mycartgoods['total']['totalAmount'],
            'uid'           => $this->uid,
            'status'         => 0,
            'pay_state'     => 0,
            'pay_method'    => 0,
            'pay_name'    => '未支付',
            'give_integral' => config('site.order_integration'),
            'give_commision' => round($mycartgoods['total']['totalAmount']*config('site.fanli_percent'),2),
            'use_inter'     => $use_inter,
            'discount'      => $integralCash,
            'use_commision' => $use_commsion_num,
            'pay_cash'      => $pay_cash,
            'coupon_id'     =>$coupon_id,
            'coupon_value'  =>$couponvalue,
            'delivery_fee'  =>$mycartgoods['total']['delivery_fee']
        );
        $data['address'] = $default_address['province_name'] . $default_address['city_name'] . $default_address['county_name'] . $default_address['address'];
        if($pay_cash==0){
            $data['pay_name'] ='余额支付';
            $data['pay_method'] =2;
            $data['pay_state'] =1;
            $data['pay_time'] =time();
            $data['status'] =1;
        }
        $re2 = $Order->save($data);
        if (false !== $re2) {
            $oid = $Order->id;
            $User->where(['id'=>$this->uid])->setDec('integration',$use_inter);
            $User->where(['id'=>$this->uid])->setDec('commision',$use_commsion_num);
            //扣除消费积分
            if ($use_inter != 0) {
                Db::name('IntegrationLog')->insert(['uid' => $this->uid, 'type' => 0, 'amount' => '-' . $use_inter, 'remark' => '订单：' . $order_no . '使用' . $use_inter . '积分抵扣' . $integralCash . '元','create_time'=>time()]);
            }
            //扣除消费佣金
            if ($use_commsion_num != 0) {
                Db::name('CommissionLog')->insert(['uid' => $this->uid, 'type' => 0, 'amount' => '-' . $use_commsion_num, 'remark' => '订单：' . $order_no . '使用' . $use_commsion_num . '佣金抵扣' . $use_commsion_num . '元','create_time'=>time()]);
            }
            if($coupon_id!==0){
                Db::name('UserCoupon')->where(['id'=>$coupon_id])->setField('status',1);
            }
            //清空购物车
            Db::name('cart')->where([['uid','eq',$this->uid],['id','in',$cart_id]])->delete();
            Db::commit();
            if($pay_cash=='0'){
                return json(['code' => 1, 'msg' => '请求成功', 'desc' => '下单并支付成功', 'data' => $order_no]);
            }
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '下单成功', 'data' => $order_no]);
        } else {
            Db::rollback();
            return json(['code' => 44005, 'msg' => '创建失败', 'desc' => '下单失败']);
        }

    }

    /**
     * [changeOrderStatus 修改订单状态]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function changeOrderStatus()
    {

        $param = input();
        $Order = Model('Order');
        if (input('?id')) {
            $order = $Order->where(array('id' => $param['id']))->find();
            if(!$order){
                return json(['code'=>44001,'msg'=>'参数错误']);
            }
            if(isset($param['status']) and $param['status']==2 and empty($param['delivery_num'])){
                return json(['code'=>44001,'msg'=>'缺少快递号']);
            }
            if(isset($param['status']) and $param['status']=='0'){
                $param['pay_state'] = 0;
            }
            if ($Order->isUpdate(true)->allowField(true)->save($param)) {
                if(isset($param['status']) and $param['status']=='3'){
                    $user = model('Member')->where(['id'=>$order['uid']])->find();
                    if($user['pid'] and $order['give_commision']>0){
                        Db::name('Member')->where(['id'=>$user['pid']])->setInc('commision',$order['give_commision']);
                        Db::name('CommissionLog')->insert(['uid'=>$user['pid'],'type'=>1,'amount'=>$order['give_commision'],'remark'=>'订单'.$order['order_no'].'返利'.$order['give_commision'].'元','create_time'=>time()]);
                    }
                    if($user['pid'] and $order['give_integral']>0){
                        Db::name('Member')->where(['id'=>$user['id']])->setInc('integration',$order['give_integral']);
                        Db::name('IntegrationLog')->insert(['uid'=>$order['uid'],'type'=>1,'amount'=>$order['give_integral'],'remark'=>'订单'.$order['order_no'].'返利'.$order['give_integral'].'积分','create_time'=>time()]);
                    }
                    $ordergodos = db('OrderGoods')->where(['order_no'=>$order['order_no']])->select();
                    foreach($ordergodos as $k=>$v){
                        Db::name('Goods')->where(['id'=>$v['gid']])->setInc('sales',$v['num']);
                    }
                    model('Member')->where(['id'=>$order['uid']])->setInc('totalConsume',$order['totalAmount']);
                }
                return json(['code' => 0, 'msg' => '处理成功']);
            } else {
                return json(['code' => 40001, 'msg' => '处理失败']);
            }

        }
    }

    /**
     * [delOrder 删除订单]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function delOrder()
    {
        $param  = input();
        $re = db('Order')->where(['id'=>$param['id']])->setField('is_delete',1);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>44004,'msg'=>'删除失败']);
        }
    }


    public function orderAgain(Request $request)
    {
        $param = $request->param();
        $order = model('Order')->where(['id'=>$param['order_id']])->find();
        if(!$order){
            return json(['code'=>44004,'msg'=>'未找到订单']);
        }
        $ordergoods = model('OrderGoods')->where(['order_no'=>$order['order_no']])->select();
        foreach($ordergoods as $k=>$v){
            $map                     = [
                'uid'      => session('uid'),
                'gid'      => $v['gid'],
                'goods_sn' => $v['goods_sn'],
            ];
            $data = array(
                'goods_sn'   => $v['goods_sn'],
                'gid'        => $v['gid'],
                'num'        => $v['num'],
                'goods_attr' => $v['goods_attr'],
                'uid'        => session('uid'),
            );
            $cartgoodsnum = Db::name('cart')->where($map)->value('num'); //获取购物车中商品数量

            if (!$cartgoodsnum) {
                //判断一下，加入购物车的数量是否在库存范围内
                // $this->is_enough($param['goods_sn'], $goods['num'], $param['num']);
                //如果购物车里面没有，新增
                $re1 = Db::name('cart')->insertGetId($data);
            } else {
                //如果购物车里面有，增加
                //判断一下，加入购物车的数量是否在库存范围内
                // $this->is_enough($_POST['goods_sn'], $goods['num'], $_POST['num'] + $cartgoodsnum);
                $re1 = Db::name('cart')->where($map)->setField('num', $param['num']);
            }
        }
        return json(['code'=>0,'msg'=>'添加购物车成功']);
    }


    public function getCanPublishOrder()
    {
        $param = input();
        $map = [];
        $map[] = ['status','in',[3,4]];
        $map[] = ['uid','eq',session('uid')];
        $list  = model('Order')->with(['OrderGoods','OrderGoods.Goods'])->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->toArray();
        foreach($list as $k=>$v){
            foreach($v['order_goods'] as $kk=>$vv){
                $list[$k]['order_goods'][$kk]['goods']['img'] = config('site.site_url').$vv['goods']['img'];
            }
        }
        $count = model('Order')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $list, 'count' => $count]);
    }

    public function delivery(Request $request)
    {
        $param = $request->param();
        $order = model('Order')->where(['id'=>$param['order_id']])->find();
        if(!$order || empty($order['delivery_num'])){
            return json(['code'=>44004,'msg'=>'参数错误或订单状态有误!']);
        }
        $host = "https://wuliu.market.alicloudapi.com";
        $path = "/kdi";//API访问后缀
        $method = "GET";
        $appcode = config('site.delivery_code');
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "no=".$order['delivery_num'];  //参数写在这里
        $bodys = "";
        $url = $host . $path . "?" . $querys;//url拼接

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        //curl_setopt($curl, CURLOPT_HEADER, true); 如不输出json, 请打开这行代码，打印调试头部状态码。
        //状态码: 200 正常；400 URL无效；401 appCode错误； 403 次数用完； 500 API网管错误
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result,true);
        if($result and $result['status']=='0'){
            return json(['code'=>0,'msg'=>'请求成功','data'=>$result['result']]);
        }else{
            return json(['code'=>40001,'msg'=>'物流信息获取失败']);
        }
    }

}

<?php
namespace app\api\controller;

class Track extends Common
{
	public function getTrackByUid()
	{
		$param = input();
		$uid = $this->uid;
		$list = model('Track')->with('goods')->where([['uid','eq',$uid]])->select()->each(function($item,$key){
            $item['goods']['img'] = config('site.site_url').$item['goods']['img'];
        });
		return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
	}

	public function track()
    {
    	$param = input();
    	$uid = $this->uid;
        $has = model('Track')->where([['uid','eq',$uid],['gid','eq',$param['gid']]])->find();
        if($has){
            $re = model('Track')->where('id',$has['id'])->setInc('num');
        }else{
            $re = model('Track')->save(['uid'=>$uid,'gid'=>$param['gid'],'num'=>1]);
        }
        if(false !== $re){
        	return json(['code'=>0,'msg'=>'请求成功']);
        }else{
        	return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }
}
?>
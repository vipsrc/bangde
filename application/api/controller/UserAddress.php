<?php
namespace app\api\controller;

use think\Request;
use think\Db;

class UserAddress extends Common
{
	/**
	 * [getAddressList 地址列表]
	 * @Author   雨夜
	 * @DateTime 2019-01-07
	 * @return   [type]     [description]
	 */
	public function getAddressList(Request $request)
	{
		$param = $request->param();
		$map = [];
		$map[] = ['a.uid','eq',$this->uid];
		$list = Db::name('user_address a')
            ->join('region b','a.province = b.region_id')
            ->join('region c','a.city = c.region_id')
            ->join('region d','a.county = d.region_id')
            ->field('a.*,b.region_name as province_name,c.region_name as city_name,d.region_name as county_name')
            ->where($map)->order('a.is_default desc')->select();
		return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
	}

	/**
	 * [getAddressInfo 地址详情]
	 * @Author   雨夜
	 * @DateTime 2019-01-07
	 * @return   [type]     [description]
	 */
	public function getAddressInfo()
	{
		$param = input();
		$map = [];
		$map[] = ['id','eq',$param['address_id']];
		$info = model('UserAddress')->where($map)->find()->toArray();
		if($info){
            $info['province_name'] = Db::name('Region')->where(['region_id'=>$info['province']])->value('region_name');
            $info['city_name'] = Db::name('Region')->where(['region_id'=>$info['city']])->value('region_name');
            $info['county_name'] = Db::name('Region')->where(['region_id'=>$info['county']])->value('region_name');
			return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
		}else{
			return json(['code'=>44001,'msg'=>'参数错误']);
		}
	}

	/**
	 * [getDefaultAddress 获取默认地址]
	 * @Author   雨夜
	 * @DateTime 2019-01-08
	 * @return   [type]     [description]
	 */
	public function getDefaultAddress()
	{
		$param = input();
		$map = [];
		$map[] = ['is_default','eq',1];
		$map[] = ['uid','eq',$this->uid];
		$info = model('UserAddress')->where($map)->find();
		if($info){
		    $info['province_name'] = Db::name('Region')->where(['region_id'=>$info['province']])->value('region_name');
            $info['city_name'] = Db::name('Region')->where(['region_id'=>$info['city']])->value('region_name');
            $info['county_name'] = Db::name('Region')->where(['region_id'=>$info['county']])->value('region_name');
			return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
		}else{
			return json(['code'=>44001,'msg'=>'暂无数据']);
		}
	}

	/**
	 * [saveAddress 保存地址]
	 * @Author   雨夜
	 * @DateTime 2019-01-07
	 * @return   [type]     [description]
	 */
	public function saveAddress()
	{
		$param = input();
		$rule = [
			'consignee|收货人'=>'require',
			'province|省份'=>'require',
			'city|市区'=>'require',
			'county|县区'=>'require',
			'address|详细地址'=>'require',
			'tel|手机号'=>'require|mobile'
		];
		$validate = $this->validate($param,$rule);
		if(true !== $validate){
			return json(['code'=>44001,'msg'=>$validate]);
		}
		if($param['is_default']==1){
			db('UserAddress')->where(['uid'=>$this->uid])->setField('is_default',0);
		}
		$param['uid'] = $this->uid;
		if(isset($param['address_id'])){
			$param['id'] = $param['address_id'];
			$re = model('UserAddress')->isUpdate(true)->allowField(true)->save($param);
		}else{
//            $has = model('UserAddress')->where(['uid'=>$this->uid])->find();
//            $param['is_default'] = empty($has) ? 1 : 0;
            $re = model('UserAddress')->isUpdate(false)->allowField(true)->save($param);
		}
		if(false !== $re){
			return json(['code'=>0,'msg'=>'保存成功']);
		}else{
			return json(['code'=>40001,'msg'=>'系统错误']);
		}
	}

	/**
	 * [setDefault 设置默认]
	 * @Author   雨夜
	 * @DateTime 2019-01-07
	 */
	public function setDefault()
	{
		$param = input();
		db('UserAddress')->where(['uid'=>$this->uid])->setField('is_default',0);
		$re = db('UserAddress')->where(['id'=>$param['address_id'],'uid'=>$this->uid])->setField('is_default',1);
		if(false!== $re){
			return json(['code'=>0,'msg'=>'保存成功']);
		}else{
			return json(['code'=>40001,'msg'=>'系统错误']);
		}
	}

	/**
	 * [delAddress 删除地址]
	 * @Author   雨夜
	 * @DateTime 2019-01-07
	 * @return   [type]     [description]
	 */
	public function delAddress()
	{
		$param = input();
		if(db('UserAddress')->where(['id'=>$param['address_id'],'is_default'=>1])->find()){
			return json(['code'=>40001,'msg'=>'默认地址不可删除！']);
		}
		$re = db('UserAddress')->where(['id'=>$param['address_id']])->delete();
		if(false!== $re){
			return json(['code'=>0,'msg'=>'删除成功']);
		}else{
			return json(['code'=>40001,'msg'=>'系统错误']);
		}
	}

}
?>
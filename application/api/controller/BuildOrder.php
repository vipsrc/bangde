<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/5/16
 * Time: 16:02
 */
namespace app\api\controller;

use app\common\model\BuildOrder as Order;
use think\Db;
use think\Request;
use app\common\controller\Common as Tool;

class BuildOrder extends Common
{

    /**
     * [saveOrder 安装订单]
     * @DateTime 2019-05-22
     */
    public function saveOrder(Request $request)
    {
        $param        = $request->param();
        $param['uid'] = $param['publisher'] = session('uid') ?: -1;
        $rule         = [
            'type'        => 'require|number', // 发布类型：0平台下单；1其他平台订单；2代他人发布
            'device_type' => 'require|number', // 设备类型：1新风系统；2净水；3空调；4地暖；5智能家居；
            'plan_type'   => 'require', // 施工方案类型 PVC枝干式安装 标准PE章鱼式安装 高端PE章鱼式安装 地送方案安装
            'contacts'    => 'require', // 联系人
            'phone'       => 'require|mobile', // 联系方式
            'device'      => 'require', // 设备信息
            'floor_num'   => 'require|number', // 楼层数
            'open_num'    => 'require|number', // 风口数
            'area'        => 'require|number', // 面积
            'home'        => 'require|number', // 室
            'hall'        => 'require|number', // 厅数
            'imgs'        => 'require', //
            'province'    => 'require|number', // 省
            'city'        => 'require|number', // 市
            'county'      => 'require|number', // 区
            'address'     => 'require', // 详细地址
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44004, 'msg' => $validate]);
        }
        if(is_array($param['imgs'])){
            $param['imgs'] = json_encode($param['imgs']);
        }
        $param['imgs'] = trim($param['imgs'],',');
        $param['publisher'] = session('uid');
        $param['order_no'] = date('Ymd') . substr(implode(null, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);

        if ($param['type'] == 2) {
            $member = Db::name('Member')->where('mobile=:mobile', ['mobile' => [$param['phone'], \PDO::PARAM_STR]])->value('id');
            if($member){
                $param['uid'] = $member;
            }else{
                return json(['code'=>44004,'msg'=>'未找到对应用户']);
            }
        }else{
            $param['uid'] = session('uid');
        }

        if ($param['device_type'] == 1) {
            $plan_price = Db::name('PlanPrice')->where([['province', '=', $param['province']], ['plan_type', '=', $param['plan_type']], ['bottom', '<=', $param['area']], ['top', '>=', $param['area']]])->find();

            if ($plan_price) {
                $price           = $plan_price['base1'] + $plan_price['base2'] * $plan_price['area'] + $plan_price['base3'] * ($param['floor_num'] - 1) + $plan_price['base4'] * $param['open_num'];
                $param['amount'] = round($price, 2);
            }
        }
        $order = model('BuildOrder');
        $re = $order->allowField(true)->save($param);
        if (false !== $re) {
            $id = $order->id;
            model('Progress')->save(['oid'=>$id,'order_no'=>$param['order_no'],'content'=>date('Y-m-d H:i:s').'下单成功','status'=>1]);

            return json(['code' => 0, 'msg' => '发布成功','data'=>$id]);
        } else {
            return json(['code' => 40001, 'msg' => '系统错误']);
        }
    }

    public function prePrice(Request $request)
    {
        $param        = $request->param();
        $param['uid'] = $param['publisher'] = session('uid') ?: -1;
        $rule         = [
            'type'        => 'require|number', // 发布类型：0平台下单；1其他平台订单；2代他人发布
            'device_type' => 'require|number', // 设备类型：1新风系统；2净水；3空调；4地暖；5智能家居；
            'plan_type'   => 'require', // 施工方案类型 PVC枝干式安装 标准PE章鱼式安装 高端PE章鱼式安装 地送方案安装
            'contacts'    => 'require', // 联系人
            'phone'       => 'require|mobile', // 联系方式
            'device'      => 'require', // 设备信息
            'floor_num'   => 'require|number', // 楼层数
            'open_num'    => 'require|number', // 风口数
            'area'        => 'require|number', // 面积
            'home'        => 'require|number', // 室
            'hall'        => 'require|number', // 厅数
            'imgs'        => 'require', //
            'province'    => 'require|number', // 省
            'city'        => 'require|number', // 市
            'county'      => 'require|number', // 区
            'address'     => 'require', // 详细地址
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44004, 'msg' => $validate]);
        }

        if ($param['device_type'] == 1) {
            $plan_price = Db::name('PlanPrice')->where([['province', '=', $param['province']], ['plan_type', '=', $param['plan_type']], ['bottom', '<=', $param['area']], ['top', '>=', $param['area']]])->find();

            if ($plan_price) {
                $price           = $plan_price['base1'] + $plan_price['base2'] * $plan_price['area'] + $plan_price['base3'] * ($param['floor_num'] - 1) + $plan_price['base4'] * $param['open_num'];
                $amount = round($price, 2);
            }else{
                return json(['code' => 40001, 'msg' => '无法预估']);
            }
            return json(['code' => 0, 'msg' => '预估成功','data'=>$amount]);
        }else{
            return json(['code' => 40001, 'msg' => '无法预估']);
        }
    }

    /**
     * [getOrderList 获得订单列表]
     * @Author   青阳
     * @DateTime 2019-05-14
     * @param    Request    $request [description]
     * builder publisher uid 都是 member 表的 id，member 表 有三种角色 普通用户，商家，施工方
     * @return   [type]              [description]
     */
    public function getOrderList(Request $request)
    {
        $param = $request->param();
        $map   = [];
        $map[] = ['publisher|uid', 'eq', session('uid')];
        $map[] = ['is_del','eq',0];


        if (isset($param['status'])) {
            $map[] = ['status', 'eq', $param['status']];
        }
        if (isset($param['type'])) {
            $map[] = ['type', 'eq', $param['type']];
        }
        if (isset($param['phone'])) {
            $map[] = ['phone', 'like', '%' . $param['publisher'] . '%'];
        }
        if(isset($param['role']) and $param['role'] == 2){
            $map[] = ['builder','eq',session('uid')];
        }
        $order = $param['order'] ?? 'update_time desc';
        $list  = Order::where($map)->page(($param['page'] ?? 1), ($param['limit'] ?? 10))->order($order)->select()->toArray();
        foreach($list as $k=>$v){
            $list[$k]['imgs'] = explode(',',$v['imgs']);
            $list[$k]['comment'] = Db::name('BuildComment')->where(['order_no'=>$v['order_no']])->find();
            $list[$k]['is_mine'] = session('uid')==$v['uid'] ? '1':'0';

        }
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $list]);
    }

    /**
     * [getOrderInfo 订单详情]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function getOrderInfo()
    {
        $param = input();
        $map   = [];
        if (isset($param['id'])) {
            $map[] = ['id', 'eq', $param['id']];
        }
        if (isset($param['uid'])) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        if (isset($param['publisher'])) {
            $map[] = ['publisher', 'eq', $param['publisher']];
        }
        if (isset($param['order_no'])) {
            $map[] = ['order_no', 'eq', $param['order_no']];
        }
        $info = Order::with('user,publisherUser')->where($map)->find();
        if ($info) {
            // 默认0待指派；1已指派；2接单；3维修中；4完成
            $info['imgs'] && $info['imgs'] = explode(',',$info['imgs']);
            $info['builder_name'] = Db::name('Member')->where(['id'=>$info['builder']])->field('nickname,avatar')->find();
            $plan = Db::name('Plan')->where(['oid'=>$info['id']])->order('id desc')->find();
            if($plan){
                $info['plan'] = $plan ;
                $info['plan_status'] = $plan['status']==1 ? 1 : 0;
            }else{
                $info['plan'] = [];
                $info['plan_status'] = 0;
            }
            $comment = model('BuildComment')->where(['type'=>0,'order_no'=>$info['order_no']])->find();
            $comment && $comment['img'] = explode(',',$comment['img']);
            $info['is_comment'] = $comment ? 1 : 0;
            $info['comment'] = $comment;

            $progress = model('Progress')->where(['oid'=>$info['id'],'status'=>$info['status']])->find();
            $info['is_progress'] = $progress ? 1 : 0;
            if($progress){
                $progress['imgarr'] = explode(',',$progress['img']);
            }
            $info['progress'] = $progress;

            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $info]);
        }

        return json(['code' => 40001, 'msg' => '查不到对应数据', 'desc' => '查不到对应数据']);
    }

    /**
     * [changeOrderStatus 更改订单状态]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function changeOrderStatus()
    {
        $param      = input();
        $buildOrder = model('BuildOrder');
        if (input('?id')) {
            $order = $buildOrder->where(array('id' => $param['id']))->find();
            if ($order && $buildOrder->isUpdate(true)->allowField(true)->save($param)) {
                if($param['status']=='5'){
                    $fee = round($order['amount']*config('site.second_percent')/100,2);
                    Tool::editCommission($order['builder'],$fee,4,'订单：'.$order['order_no'].'第二笔安装费'.$fee.'元');
                    Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户已确认完工','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
                }else if($param['status']=='6'){
                    $fee = round($order['amount']*config('site.last_percent')/100,2);
                    Tool::editCommission($order['builder'],$fee,4,'订单：'.$order['order_no'].'第三笔安装费'.$fee.'元');
                    Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方已收尾，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
                }
                return json(['code' => 0, 'msg' => '处理成功']);
            }
            return json(['code' => 44001, 'msg' => '处理失败，查不到数据']);
        }
        return json(['code' => 40001, 'msg' => '处理失败，缺少参数']);
    }

    /**
     * [delOrder 删除订单]
     * @Author   青阳
     * @DateTime 2019-05-15
     * @return   [type]     [description]
     */
    public function delOrder()
    {
        $param = input();
        // order表 表示删除is_delete => 1  build_order表status默认0待指派；1已指派；2接单；3维修中；4完成
        $result = model('BuildOrder')->where(['id' => $param['order_id']])->setField('is_del', 1);
        $return = ['code' => 0, 'msg' => '删除成功'];
        if (false == $result) {
            $return = ['code' => 44004, 'msg' => '删除失败'];
        }
        return json($return);
    }

    /**
     * [uploadPlan 上传保存方案]
     * @Author   青阳
     * @DateTime 2019-05-21
     * @return   [type]     [description]
     */
    public function uploadPlan(Request $request)
    {
        $param = $request->param();
        $rule  = [
            'order_no' => 'require', // 订单号码
            'link'     => 'require', // 七牛云的地址，由前段传参
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44004, 'msg' => $validate]);
        }
        $order = model('BuildOrder')->where(['order_no'=>$param['order_no']])->find();
        if(!$order){
            $return = ['code' => 44004, 'msg' => '订单错误'];
        }else{
            $param['oid'] = $order['id'];
            $param['status'] = 0; // 默认0 (0待确认 1已确认 2驳回)
            $result          = model('Plan')->allowField(true)->save($param);
            $return          = ['code' => 40001, 'msg' => '系统错误'];
            if (false !== $result) {
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方已上传方案，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
                $return = ['code' => 0, 'msg' => '发布成功'];
            }
        }

        return json($return);
    }

    /**
     * [ChangePlanStatus 更改方案状态 确认或者驳回 0待确认 1已确认 2驳回]
     * @Author   青阳
     * @DateTime 2019-05-21
     */
    public function CPStatus()
    {
        $id     = input('plan_id/d');
        $status = input('status');
        if ($status == null) {
            return json(['code' => 40001, 'msg' => '参数不全']);
        }
        $plan = model('Plan')->where(['id'=>$id])->find();
        $order = model('BuildOrder')->where(['id'=>$plan['oid']])->find();
        $status_arr = [
            0 => '待确认',
            1 => '确认方案',
            2 => '驳回方案',
        ];
        // 是确认还是驳回
        $return = ['code' => 40001, 'msg' => '系统错误，' . $status_arr[$status] . '失败'];

        $re = model('Plan')->where('id', 'eq', $id)->update(['status'=>$status,'reply'=>input('reply','')]);
        if(false !== $re){
            if($status==1){
                model('Progress')->save(['oid'=>$plan['oid'],'order_no'=>$plan['order_no'],'content'=>date('Y-m-d H:i:s').'确认施工方案','status'=>3,'img'=>$plan['link']]);
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户已确认方案，请尽快预约开工时间','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }else{
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'方案驳回，请及时整改确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            $return = ['code' => 0, 'msg' => $status_arr[$status] . '成功'];
        }
        return json($return);

    }
    
    public  function saveprogress(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['id'=>$param['order_id'],'builder'=>session('uid')])->find();
        if(!$order){
            return json(['code'=>40001,'msg'=>'未找到订单']);
        }
        if(empty($param['img']) and empty($param['video'])){
            return json(['code'=>44004,'msg'=>'视频或图片必传一项！']);
        }

        $img = $param['img'];
        $video = $param['video'];

        $data = [
            'order_no'=>$order['order_no'],
            'oid'=>$order['id'],
            'gid'=>$order['gid']??0,
            'img'=>$img,
            'video'=>$video,
            'content'=>$param['content'],
            'status'=>$order['status']
        ];
        if($order['type']==0 and !empty($order['gid']) and $param['video']){
            $video = explode(',',$param['video']);
            if(false !== $video){
                foreach($video as $k=>$v){
                    model('GoodsVideo')->save(['gid'=>$order['gid'],'video'=>$v,'title'=>$order['device'].'安装']);
                }
            }else{
                model('GoodsVideo')->save(['gid'=>$order['gid'],'video'=>$param['video'],'title'=>$order['device'].'安装']);
            }
        }
        $re = model('Progress')->save($data);
        if(false !== $re){
            if($order['status']==4){
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方已上传现场进展，请查阅','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [getProgress] 获取施工进度
     * @author 雨夜
     * @date 2019/6/17
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getProgress(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order){
            return json(['code'=>40001,'msg'=>'未找到订单']);
        }
        $data = [];
        $list = model('Progress')->where(['oid'=>$order['id']])->select()->toArray();
        foreach($list as $k=>$v){
            $v['img'] = explode(',',$v['img']);
            if(isset($data[$v['status']])){
                $data[$v['status']]['content'][] = $v;
            }else{
                $data[$v['status']]['title'] = config('base.progress_status')[$v['status']];
                $data[$v['status']]['content'][] = $v;
            }
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>array_values($data)]);
    }


    public function getProgressList(Request $request)
    {
        $param = $request->param();
        $map = [];
        $progress = [];
        $map[] = ['gid','eq',$param['gid']];
        if(isset($param['city']) and $param['city']){
            $orderlist = model('BuildOrder')->where(['city'=>$param['city']])->column('order_no');
            $map[] = ['order_no','in',$orderlist];
        }
        $orderno = model('Progress')->where($map)->column('order_no');
        $orderno = array_unique($orderno);
        foreach($orderno as $key=>$value){
            $data = [];
            $list = model('Progress')->where(['order_no'=>$value])->select()->toArray();
            foreach($list as $k=>$v){
                $v['img'] = explode(',',$v['img']);
                $order = Db::name('BuildOrder')->where(['order_no'=>$value])->find();
                if(isset($data[$v['status']])){
                    $data[$v['status']]['content'][] = $v;
                }else{
                    $data[$v['status']]['title'] = config('base.progress_status')[$v['status']];
                    $data[$v['status']]['order_info'] = $order['address'].'-'.$order['device'];
                    $data[$v['status']]['content'][] = $v;
                }
            }
            $progress[$key] = array_values($data);
        }

        return json(['code'=>0,'msg'=>'请求成功','data'=>$progress]);
    }

    public function getDefaultProgress(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['progress_show'=>1])->find();
        if(!$order){
            return json(['code'=>40001,'msg'=>'未找到订单']);
        }
        $data = [];
        $list = model('Progress')->where(['oid'=>$order['id']])->select()->toArray();
        foreach($list as $k=>$v){
            $v['img'] = explode(',',$v['img']);
            if(isset($data[$v['status']])){
                $data[$v['status']]['content'][] = $v;
            }else{
                $data[$v['status']]['title'] = config('base.progress_status')[$v['status']];
                $data[$v['status']]['content'][] = $v;
            }
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>array_values($data)]);
    }

    public function grabOrderList(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        if($member['role']<2){
            return json(['code'=>44001,'msg'=>'非施工单位，暂无权限！']);
        }
        $recity = model('Apply')->where([['uid','eq',$member['id']],['type','in',[2,3]],['status','eq',1]])->order('id desc')->value('re_city');
        if(!$recity){
            return json(['code'=>44001,'msg'=>'非施工单位，暂无权限！']);
        }
        $map   = [];
        $map[] = ['status', 'eq', 1];
        $map[] = ['is_del', 'eq', 0];
        $map[] = ['create_time', '<=', time()-3*60];
        if(isset($param['province'])){
            $map[]= ['province','eq',$param['province']];
        }
        if(isset($param['city'])){
            $map[]= ['city','in',[$param['city'],$recity]];
        }else{
            $map[] = ['city','eq',$recity];
        }
        if (isset($param['keywords'])) {
            $map[] = ['device', 'like', '%' . $param['keywords'] . '%'];
        }

        $order = 'update_time desc';
        if ($param['type']==1) {
            $list  = model('RepairOrder')->where($map)->page(($param['page'] ?? 1), ($param['limit'] ?? 10))->order($order)->select()->toArray();
            foreach($list as $k=>$v){
                $list[$k]['repair_type_text'] = Db::name('RepairType')->where(['id'=>$v['repair_type']])->value('name');
            }
            $count = model('RepairOrder')->where($map)->count();
        }else{
            $list  = model('BuildOrder')->where($map)->page(($param['page'] ?? 1), ($param['limit'] ?? 10))->order($order)->select()->toArray();
            $count  = model('BuildOrder')->where($map)->count();
        }
        foreach($list as $k=>$v){
            $v['imgs'] && $list[$k]['imgsArr'] = explode(',',$v['imgs']);
            $list[$k]['province_name'] = Db::name('Region')->where(['region_id'=>$v['province']])->value('region_name');
            $list[$k]['city_name'] = Db::name('Region')->where(['region_id'=>$v['city']])->value('region_name');
            $list[$k]['county_name'] = Db::name('Region')->where(['region_id'=>$v['county']])->value('region_name');
            $list[$k]['phone_sub'] = substr($v['phone'],0,3).'****'.substr($v['phone'],-4);
        }
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $list,'count'=>$count]);
    }

    public function grabOrder(Request $request)
    {
        $param = $request->param();

        if($param['type']==1){
            $order = model('RepairOrder')->where(['id'=>$param['order_id']])->find();
            $model = 'RepairOrder';
        }else{
            $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
            $model = 'BuildOrder';
        }
        if(!$order){
            return json(['code'=>44004,'msg'=>'订单参数错误']);
        }
        if($order['status']!==1 || !empty($order['builder'])){
            return json(['code'=>44004,'msg'=>'订单状态错误']);
        }
        $re = model($model)->where(['id'=>$param['order_id']])->update(['builder'=>session('uid'),'status'=>2]);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'抢单成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function getOrderType()
    {
        $member = model('Member')->where(['id'=>session('uid')])->find();
        if($member['role']==0){
            $arr = ['0'=>'平台下单','1'=>'其他平台下单'];
        }else{
            if(input('?type') and input('type')==1){
                $arr = ['0'=>'平台下单','1'=>'其他平台下单'];
            }else{
                $arr = ['0'=>'平台下单','1'=>'其他平台下单','2'=>'代他人发布'];
            }
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>$arr]);
    }

    public function setStartTime(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order || ($order['time_status_u']==1 and $order['time_status_b']==1)){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if(!isset($param['build_time']) || strtotime($param['build_time'].' '.$param['time_rang'])<time()){
            return json(['code'=>44004,'msg'=>'请选择晚于当前的起始日期！']);
        }
        if($member['role']>=2){
            $data['time_status_u'] = 0;
            $data['time_status_b'] = 1;
            $data['start_time'] = $param['build_time'];
            $data['time_rang'] = $param['time_rang'];
        }else{
            $data['time_status_u'] = 1;
            $data['time_status_b'] = 0;
            $data['start_time'] = $param['build_time'];
            $data['time_rang'] = $param['time_rang'];
        }
        $re = Db::name('BuildOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){
            if($member['role']>=2){
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方正在向您发起勘察时间预约，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }else{
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户正在向您发起勘察时间预约，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            return json(['code'=>0,'msg'=>'修改成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function sureTimeStatus(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if($member['role']>=2){
            $data['time_status_b'] = 1;
        }else{
            $data['time_status_u'] = 1;
        }
        $re = Db::name('BuildOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){
            model('BuildOrder')->where(['id'=>$order['id']])->setField('status',3);
            model('Progress')->save(['oid'=>$order['id'],'order_no'=>$order['order_no'],'content'=>date('Y-m-d H:i:s').'与师傅约定好勘察时间','status'=>2]);
            if($member['role']>=2){
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方已确认您的勘察预约时间，等待上门','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }else{
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户已确认您的勘察预约时间，等待上门','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            return json(['code'=>0,'msg'=>'确认成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function setBuildTime(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order || ($order['build_time_status_u']==1 and $order['build_time_status_b']==1)){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if(!isset($param['build_time']) || strtotime($param['build_time'].' '.$param['time_rang'])<time()){
            return json(['code'=>44004,'msg'=>'请选择晚于当前的起始日期！']);
        }
        if($member['role']>=2){
            $data['build_time_status_u'] = 0;
            $data['build_time_status_b'] = 1;
            $data['build_time'] = $param['build_time'];
            $data['build_time_rang'] = $param['time_rang'];
        }else{
            $data['build_time_status_u'] = 1;
            $data['build_time_status_b'] = 0;
            $data['build_time'] = $param['build_time'];
            $data['build_time_rang'] = $param['time_rang'];
        }
        $re = Db::name('BuildOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){
            if($member['role']>=2){
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方正在向您发起开工时间预约，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }else{
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户正在向您发起开工时间预约，请确认','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            return json(['code'=>0,'msg'=>'修改成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function sureBtimeStatus(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('BuildOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if($member['role']>=2){
            $data['build_time_status_b'] = 1;
        }else{
            $data['build_time_status_u'] = 1;
        }
        $re = Db::name('BuildOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){
            model('BuildOrder')->where(['id'=>$order['id']])->setField('status',4);
            $fee = round($order['amount']*config('site.first_percent')/100,2);
            Tool::editCommission($order['builder'],$fee,4,'订单：'.$order['order_no'].'第一笔安装费'.$fee.'元');
            if($member['role']>=2){
                Tool::sendMessage($order['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'施工方已确认您的开工时间预约，等待上门','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }else{
                Tool::sendMessage($order['builder'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'用户已确认您的开工时间预约，等待上门','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            }
            return json(['code'=>0,'msg'=>'确认成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [myGrabOrderList] 已抢订单
     * @author 雨夜
     * @date 2019/6/15
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function myGrabOrderList(Request $request)
    {
        $param = $request->param();
        $map   = [];
        if(isset($param['status']) and $param['status']!==''){
            $map[] = ['status','eq',$param['status']];
        }
        if(isset($param['type']) and $param['type']==1){
            $model = 'RepairOrder';
        }else{
            $model = 'BuildOrder';
        }
        $member = model('Member')->where(['id'=>session('uid')])->find();
        if($member['role']<2){
            return json(['code' => 40004, 'msg' => '非施工单位，暂无权限']);
        }
        $map[] = ['builder', 'eq', session('uid')];
        $order = 'update_time desc';
        $list  = model($model)->where($map)->page(($param['page'] ?? 1), ($param['limit'] ?? 10))->order($order)->select()->toArray();
        if($model == 'RepairOrder'){
            foreach($list as $k=>$v){
                $list[$k]['repair_type_text'] = Db::name('RepairType')->where(['id'=>$v['repair_type']])->value('name');
            }
        }
        $count = model($model)->where($map)->count();
        foreach($list as $k=>$v){
            $v['imgs'] && $list[$k]['imgs'] = explode(',',$v['imgs']);
        }
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $list,'count'=>$count]);
    }
}

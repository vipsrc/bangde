<?php
namespace app\api\controller;

class Complain extends Common
{
    /**
     * [getComplain 投诉数据]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [type]     [description]
     */
    public function getComplain()
    {
        $param = $this->param;
        $list  = Model('Complain')->page($param['page'], $param['limit'])->select()->each(function ($item, $key) {
            $item['nickname'] = db('auth_user')->where(['id' => $item['uid']])->value('nickname');
            return $item;
        });
        $count = Model('Complain')->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [delComplain 删除投诉记录]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [type]     [description]
     */
    public function delComplain()
    {
        $param = input();
        $re    = db('Complain')->where(['id' => $param['id']])->delete();
        if (false != $re) {
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败', 'desc' => '删除失败']);
        }
    }

    /**
     * [saveComplain 设置默认]
     * @Author   雨夜
     * @DateTime 2019-01-07
     * @return   [type]     [description]
     */
    public function saveComplain()
    {
        $param = input();
        $rule = [
            // 'title|标题'=>'require',
            'content|内容'=>'require'
        ];
        $validate = $this->validate($param,$rule);
        if(true!== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
        $re = model('Complain')->save($param);
        if($re !== false){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }


}

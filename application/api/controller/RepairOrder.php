<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/4/2
 * Time: 11:49
 */
namespace app\api\controller;

use think\Request;
use app\common\model\RepairOrder as Order;
use think\Db;

class RepairOrder extends Common
{
    /**
     * @author 雨夜
     * @date 2019年4月11日10:42:08
     * @param Request $request
     * @return \think\response\Json
     */
    public function saveOrder(Request $request)
    {
        $param = $request->param();
        $rule = [
            'type' => 'require|number|in:0,1',
            'repair_type'=>'require|number',
            'contacts' => 'require',
            'phone' => 'require|mobile',
            'fault' => 'require',
            'imgs' => 'require',
            'province' => 'require|number',
            'city' => 'require|number',
            'county' => 'require|number',
            'address' => 'require'
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44004, 'msg' => $validate]);
        }
        $price = Db::name('RepairType')->where(['id'=>$param['repair_type']])->value('price');
        $param['amount'] = $price;
        $param['uid'] = session('uid');
        $param['imgs'] = trim($param['imgs'],',');
        $param['order_no'] = date('Ymd') . substr(implode(null, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        $order = model('RepairOrder');
        $re = $order->allowField(true)->save($param);
        if (false !== $re) {
            $id = $order->id;
            return json(['code' => 0, 'msg' => '发布成功','data'=>$id]);
        } else {
            return json(['code' => 40001, 'msg' => '系统错误']);
        }
    }

    /**
     * @author 雨夜
     * @date 2019年4月11日09:26:58
     * @param Request $request
     * @return \think\response\Json
     */
    public function getOrderList(Request $request)
    {
        $param = $request->param();
        $map = [];
        $map[] = ['uid','eq',session('uid')];
        $map[] = ['is_del','eq',0];
        if(isset($param['status'])){
            $map[] = ['status','eq',$param['status']];
        }
        $list = Order::where($map)->page($param['page'],$param['limit'])->select()->toArray();
        foreach($list as $k=>$v){
            $list[$k]['repair_type_text'] = Db::name('RepairType')->where(['id'=>$v['repair_type']])->value('name');
            $list[$k]['is_comment'] = model('BuildComment')->where(['type'=>1,'order_no'=>$v['order_no']])->find() ? 1 : 0;
            $v['imgs'] && $list[$k]['imgs'] = explode(',',$v['imgs']);
        }
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $list]);
    }

    /**
     * [getRepairType] 获取维修类型
     * @author 雨夜
     * @date 2019/6/17
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getRepairType()
    {
        $list = Db::name('RepairType')->select();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }

    /**
     * [getOrderInfo] 获取订单详情
     * @author 雨夜
     * @date 2019/6/17
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getOrderInfo()
    {
        $param = input();
        $map   = [];
        if (isset($param['id'])) {
            $map[] = ['id', 'eq', $param['id']];
        }

        if (isset($param['order_no'])) {
            $map[] = ['order_no', 'eq', $param['order_no']];
        }

        $info = model('RepairOrder')->with('user')->where($map)->find();
        if ($info) {
            // 默认0待指派；1已指派；2接单；3维修中；4完成
            $info['imgs'] && $info['imgs'] = explode(',',$info['imgs']);
            $info['repair_type_text'] = Db::name('RepairType')->where(['id'=>$info['repair_type']])->value('name');
            $comment = model('BuildComment')->where(['type'=>1,'order_no'=>$info['order_no']])->find();
            $comment && $comment['img'] = explode(',',$comment['img']);
            $comment && $comment['video'] = explode(',',$comment['video']);
            $info['is_comment'] = $comment ? 1 : 0;
            $info['comment'] = $comment;
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $info]);
        }
        return json(['code' => 40001, 'msg' => '查不到对应数据', 'desc' => '查不到对应数据']);
    }

    /**
     * [changeOrderStatus] 修改订单状态
     * @author 雨夜
     * @date 2019/6/17
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function changeOrderStatus()
    {
        $param      = input();
        $buildOrder = model('RepairOrder');
        if (input('?id')) {
            $order = $buildOrder->where(array('id' => $param['id']))->find();
            if ($order && $buildOrder->isUpdate(true)->allowField(true)->save($param)) {

                return json(['code' => 0, 'msg' => '处理成功']);
            }
            return json(['code' => 44001, 'msg' => '处理失败，查不到数据']);
        }
        return json(['code' => 40001, 'msg' => '处理失败，缺少参数']);
    }


    /**
     * [delOrder] 删除订单
     * @author 雨夜
     * @date 2019/6/17
     * @return \think\response\Json
     */
    public function delOrder()
    {
        $param = input();
        $result = model('RepairOrder')->where(['id' => $param['order_id']])->setField('is_del', 1);
        $return = ['code' => 0, 'msg' => '删除成功'];
        if (false == $result) {
            $return = ['code' => 44004, 'msg' => '删除失败'];
        }
        return json($return);
    }

    /**
     * [setRepairTime] 设置上门时间
     * @author 雨夜
     * @date 2019/6/17
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function setRepairTime(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('RepairOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order || ($order['time_status_u']==1 and $order['time_status_b']==1)){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if(!isset($param['repair_time']) || strtotime($param['repair_time'])<time()){
            return json(['code'=>44004,'msg'=>'请选择晚于当前的起始日期！']);
        }
        if($member['role']>=2){
            $data['time_status_u'] = 0;
            $data['time_status_b'] = 1;
            $data['repair_time'] = $param['repair_time'];
            $data['time_rang'] = $param['time_rang'];
        }else{
            $data['time_status_u'] = 1;
            $data['time_status_b'] = 0;
            $data['repair_time'] = $param['repair_time'];
            $data['time_rang'] = $param['time_rang'];
        }
        $re = Db::name('RepairOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){

            return json(['code'=>0,'msg'=>'修改成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [sureRepairTime] 确认维修时间
     * @author 雨夜
     * @date 2019/6/17
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function sureRepairTime(Request $request)
    {
        $param = $request->param();
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $order = model('RepairOrder')->where(['id'=>$param['order_id']])->find();
        if(!$order){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if($member['role']>=2){
            $data['time_status_b'] = 1;
        }else{
            $data['time_status_u'] = 1;
        }
        $re = Db::name('RepairOrder')->where(['id'=>$order['id']])->update($data);
        if(false !== $re){
            model('RepairOrder')->where(['id'=>$order['id']])->setField('status',3);
            return json(['code'=>0,'msg'=>'确认成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }
}
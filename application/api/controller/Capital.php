<?php
namespace app\api\controller;

use think\Db;
use think\Request;

class Capital extends Common
{

    /**
     * [getIntegration 获取积分记录]
     * @Author   雨夜
     * @DateTime 2018-12-19
     * @return   [type]     [description]
     */
    public function getIntegration()
    {
        $param = input();
        $map   = [];
        $map[] = ['uid', 'eq', session('uid')];

        $list = model('IntegrationLog')->with('user')->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->each(function ($item, $key) {
            $item['nickname'] = $item['user']['nickname'];
            return $item;
        });
        $count = model('IntegrationLog')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [getCommission 获取佣金记录]
     * @Author   雨夜
     * @DateTime 2018-12-19
     * @return   [type]     [description]
     */
    public function getCommission()
    {
        $param = input();
        $map   = [];

        $map[] = ['uid', 'eq', session('uid')];
        $list  = model('CommissionLog')->with('user')->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select();
        $count = model('CommissionLog')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    public function exchange(Request $request)
    {
        $param = $request->param();
        if(!$request->has('integration') || $request->param('integration')<=0){
            return json(['code'=>44004,'msg'=>'积分参数错误']);
        }
        $member = model('Member')->where(['id'=>session('uid')])->find();
        if($param['integration']>$member['integration']){
            return json(['code'=>44004,'msg'=>'积分余额不足！']);
        }
        $tomoney = round($param['integration']/config('site.integration_percent'),2);
        Db::startTrans();
        $re1 = Db::name('Member')->where('id',session('uid'))->setField('commision',$tomoney);
        $re2 = Db::name('Member')->where('id',session('uid'))->setDec('integration',$param['integration']);
        if(false !== $re1 and false !== $re2){
            Db::name('IntegrationLog')->insert(['uid'=>session('uid'),'type'=>2,'amount'=>'-'.$param['integration'],'remark'=>$param['integration'].'积分兑换余额'.$tomoney.'元','create_time'=>time()]);
            Db::name('CommissionLog')->insert(['uid'=>session('uid'),'type'=>5,'amount'=>$tomoney,'remark'=>$param['integration'].'积分兑换余额'.$tomoney.'元','create_time'=>time()]);
            Db::commit();
            return json(['code'=>0,'msg'=>'兑换成功']);
        }else{
            Db::rollback();
            return json(['code'=>4001,'msg'=>'系统错误']);
        }
    }


    /**
     * [getWithdraw 提现记录]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function getWithdraw()
    {
        $param = input();
        $map   = [];
        $map[] = ['uid', 'eq', session('uid')];
        $list  = model('Withdraw')->with('user')->where($map)->page($param['page'], $param['limit'])->order('status asc , create_time desc')->select()->toArray();
        foreach($list as $k=>$v){
            $way = Db::name('WithdrawWay')->where(['uid'=>$v['uid']])->find();
            if($v['way']==1){
                $list[$k]['way_text'] = '支付宝：'.$way['ali_name'].'账号：'.$way['ali_account'];
            }else{
                $list[$k]['way_text'] = '微信：<img src="'.$way['wx_qrcode'].'" style="height:100%;">';
            }
        }
        $count = model('Withdraw')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [checkWithdraw 审核提现记录]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function checkWithdraw()
    {
        $param    = input();
        $withdraw = model('Withdraw')->get($param['id']);
        if (!$withdraw) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        if ($withdraw['status'] !== 0) {
            return json(['code' => 44001, 'msg' => '申请状态有误']);
        }
        Db::startTrans();
        $re1 = Db::name('Withdraw')->where(['id' => $withdraw['id']])->setField($param['type']);
        if ($param['type'] == '2') {
            $re2 = Db::name('AuthUser')->where(['id' => $withdraw['uid']])->setInc('balance', $withdraw['money']);
            $re3 = Db::name('CommissionLog')->insert(['uid' => $withdraw['uid'], 'type' => 3, 'remark' => '提现失败返还' . $withdraw['amount'] . 'yuan ', 'create_time' => time()]);
        }else{
        	$re2= true;
        	$re3 = true;
        }
        if(false!==$re1 and false!==$re2 and false!==$re3){
        	Db::commit();
        	return json(['code'=>0,'msg'=>'审核成功']);
        }else{
        	Db::rollback();
        	return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [saveWithdraw 提交提现申请]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function saveWithdraw()
    {
    	$param = input();
    	if(!isset($param['amount']) || empty($param['amount']) || $param['amount']<='0' || !is_numeric($param['amount'])){
    		return json(['code'=>44001,'msg'=>'参数错误']);
    	}
    	$user = model('Member')->get(session('uid'));
    	if($param['amount']>$user['commision']){
    		return json(['code'=>44001,'msg'=>'余额不足']);
    	}
    	Db::startTrans();
    	$re1 = Db::name('Member')->where(['id'=>$user['id']])->setDec('commision',round($param['amount'],2));
    	$re2 = Db::name('CommissionLog')->insert(['uid'=>$user['id'],'type'=>2,'amount'=>'-'.round($param['amount'],2),'remark'=>'申请提现'.round($param['amount'],2).'元','create_time'=>time()]);
        $re3 = Db::name('Withdraw')->insert(['uid'=>$user['id'],'way'=>$param['way'],'amount'=>$param['amount'],'create_time'=>time(),'update_time'=>time()]);
    	if(false != $re1 and false !=$re2 and false!=$re3){
            Db::commit();
    		return json(['code'=>0,'msg'=>'申请成功，请等待审核！']);
    	}else{
             Db::rollback();
    		return json(['code'=>40001,'msg'=>'系统错误']);
    	}
    }

    /**
     * [saveWithdrawWay 保存提现方式]
     * @Author   雨夜
     * @DateTime 2019-01-10
     * @return   [type]     [description]
     */
    public function saveWithdrawWay()
    {
        $param = input();
        $param['uid'] = session('uid');

        $has = db('WithdrawWay')->where(['uid'=>$this->uid])->find();
        if($has){
            $param['id'] = $has['id'];
            $re = model('WithdrawWay')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('WithdrawWay')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [getWithDrawWay 提现方式]
     * @Author   雨夜
     * @DateTime 2019-01-10
     * @return   [type]     [description]
     */
    public function getWithDrawWay()
    {
        $param = input();
        $info = model('WithdrawWay')->where(['uid'=>$this->uid])->find();
        if($info){
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }else{
            return json(['code'=>40001,'msg'=>'请求成功']);
        }
    }
}

<?php
namespace app\api\controller;

use think\Db;
use think\Request;

class Comment extends Common
{
    public function getCommentList()
    {
        $param = input();
        $uid   = $this->uid;
        $map   = [];
        $map[] = ['is_show','eq','0'];
        if (isset($param['gid']) and !empty($param['gid'])) {
            $map[] = ['gid', 'eq', $param['gid']];
        }
        $list  = model('Comment')->with('goods,user')->where($map)->page(input('page', 1), input('limit', '10'))->select()->each(function($item,$key){
            $item['imgs'] && $item['imgs'] = explode(',',$item['imgs']);
            $item['video'] && $item['video'] = explode(',',$item['video']);
            return $item;
        });

        $count = model('Comment')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    public function getCommentById()
    {
        $param = input();
        $uid   = $this->uid;
        $map   = [];
        $map[] = ['id', 'eq', $param['comment_id']];
        $info  = model('Comment')->with('goods,user')->where($map)->find();
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $info]);
    }

//    public function comment()
//    {
//        $param = input();
//        $uid   = $this->uid;
//        $rule  = [
//            'gid|商品ID'     => 'require|number',
//            'order_no|订单号' => 'require',
//            'star|星级'      => 'number|between:1,5',
//            'content|内容'   => 'require',
//        ];
//        $validate = $this->validate($param, $rule);
//        if (true !== $validate) {
//            return json(['code' => 44001, 'msg' => $validate]);
//        }
//        $order = Db::name('Order')->where(['order_no' => $param['order_no']])->find();
//        if ($order and $order['status'] == 3) {
//            $ordergodos = Db::name('OrderGoods')->where(['order_no' => $order['order_no'], 'gid' => $param['gid']])->find();
//            if (!$ordergodos) {
//                return json(['code' => 44001, 'msg' => '未找到该订单商品！']);
//            }
//            $data = [
//                'gid'        => $param['gid'],
//                'uid'        => $uid,
//                'goods_star' => $param['star'],
//                'content'    => $param['content'],
//            ];
//            $re = model('Comment')->save($data);
//            if (false !== $re) {
//                return json(['code' => 0, 'msg' => '评价成功']);
//            } else {
//                return json(['code' => 40001, 'msg' => '系统错误']);
//            }
//        } else {
//            return json(['code' => 44001, 'msg' => '订单状态有误！']);
//        }
//    }

    public function comment()
    {
        $param = input();
        $uid   = $this->uid;
        $rule  = [
            'order_no|订单号' => 'require',
            'star|星级'      => 'number|between:1,5',
            'content|内容'   => 'require',
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44001, 'msg' => $validate]);
        }
        $order = Db::name('Order')->where(['order_no' => $param['order_no']])->find();
        if ($order and $order['status'] == 3) {
            $ordergodos = Db::name('OrderGoods')->where(['order_no' => $order['order_no']])->select();
            if (!$ordergodos) {
                return json(['code' => 44001, 'msg' => '未找到该订单商品！']);
            }
            foreach($ordergodos as $k=>$v){
                $data[$k]= [
                    'gid'        => $v['gid'],
                    'uid'        => $uid,
                    'goods_star' => $param['star'],
                    'content'    => $param['content'],
                    'imgs'      =>$param['imgs'],
                    'video'     =>$param['video'],
                    'order_no'  =>$param['order_no']
                ];
            }
            if(isset($param['video'])){
                foreach($ordergodos as $k=>$v){
                    $video[$k]= [
                        'gid'        => $v['gid'],
                        'title'        => '来自客户的商品评价小视频',
                        'video'     =>$param['video']
                    ];
                }
                model('GoodsVideo')->saveAll($video);
            }
            $re = model('Comment')->saveAll($data);
            if (false !== $re) {
                Db::name('Order')->where(['id'=>$order['id']])->setField('status',4);
                return json(['code' => 0, 'msg' => '评价成功']);
            } else {
                return json(['code' => 40001, 'msg' => '系统错误']);
            }
        } else {
            return json(['code' => 44001, 'msg' => '订单状态有误！']);
        }
    }
    /**
     * [getBuildCommentList 查询安装订单评论]
     * @Author   青阳
     * @DateTime 2019-05-14
     * @return   [type]     [description]
     */
    public function getBuildCommentList()
    {
        $param = input();
        $map   = [];
        $map[] = ['is_show','eq',0];
        if (isset($param['builder']) and !empty($param['builder'])) {
            $map[] = ['builder', 'eq', $param['builder']];
        }
        if (!empty($uid)) {
            $map[] = ['uid', 'eq', $uid];
        }
        // 根据 当前用户 uid , 施工方 id （这两个id均对应member表） 查出对应的评论列表，belongsTo每条评论对应的 member 表
        $list  = model('BuildComment')->with('user,buildUser,buildOrder')->where($map)->page(input('page', 1), input('limit', '10'))->select();
        $count = model('BuildComment')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [getBuildCommentById 根据评论id查询评论详情]
     * @Author   青阳
     * @DateTime 2019-05-14
     * @return   [type]     [description]
     */
    public function getBuildCommentById()
    {
        $param = input();
        $map   = [];
        $map[] = ['id', 'eq', $param['build_comment_id']];
        // 关联 评论人user 施工人buildUser 订单详情buildOrder
        $info = model('BuildComment')->with('user,buildUser,buildOrder')->where($map)->find();
        return json(['code' => 0, 'msg' => '请求成功', 'data' => $info]);
    }

    /**
     * [buildComment 评论保存]
     * @Author   青阳
     * @DateTime 2019-05-14
     * @return   [type]     [description]
     */
    public function buildComment()
    {
        $param = input();
        $uid   = session('uid') ?: -1;
        $rule  = [
            'type'=>'require|in:0,1',
            'order_no|订单号' => 'require', // 订单号
            'speed_star|安装速度'     => 'require|number|between:1,5', // 评星
            'service_star|服务态度'     => 'require|number|between:1,5', // 评星
            'quality_star|安装质量'     => 'require|number|between:1,5', // 评星
            'content|内容'  => 'require', // 评论内容
        ];
        $validate = $this->validate($param, $rule);
        if (true !== $validate) {
            return json(['code' => 44001, 'msg' => $validate]);
        }
        if($param['type']=='1'){
            $order = Db::name('RepairOrder')->where(['order_no' => $param['order_no']])->find();
            $build_status = in_array($order['status'], [4]);
        }else{
            $order = Db::name('BuildOrder')->where(['order_no' => $param['order_no']])->find();
            $build_status = in_array($order['status'], [5, 6]);
        }


        if ($order and $build_status !== false) {
            $builder = Db::name('Member')
                ->where(['id' => $order['builder']])
                ->find();
            if (!$builder) {
                return json(['code' => 44001, 'msg' => '未找到施工方信息！']);
            }
            $data = [
                'builder' => $order['builder'],
                'uid'     => session('uid'),
                'speed_star'    => $param['speed_star'],
                'service_star'    => $param['service_star'],
                'quality_star'    => $param['quality_star'],
                'content' => $param['content'],
                'img'     => $param['img'],
                'video'   => $param['video'],
                'type'    => $param['type'],
                'order_no'=> $order['order_no']
            ];
            $re = model('BuildComment')->save($data);
            if (false !== $re) {

                model('Progress')->save(['oid'=>$order['id'],'order_no'=>$order['order_no'],'content'=>date('Y-m-d H:i:s').'施工评价','status'=>7,'img'=>$data['img'],'video'=>$data['video']]);

                return json(['code' => 0, 'msg' => '评价成功']);
            } else {
                return json(['code' => 40001, 'msg' => '系统错误']);
            }
        } else {
            return json(['code' => 44001, 'msg' => '订单状态有误！']);
        }
    }

    public function bcommentReply(Request $request)
    {
        $param = $request->param();
        if(!$param['reply']){
            return json(['code'=>44004,'msg'=>'内容不能为空']);
        }
        $comment = model('BuildComment')->where(['id'=>$param['com_id']])->find();
        if(!$comment){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        $re = model('BuildComment')->where(['id'=>$comment['id']])->setField('reply',$param['reply']);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'回复成功']);
        }else{
            return json(['code'=>40001,'msg'=>'回复失败']);
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2018/12/20
 * Time: 9:05
 */
namespace app\api\controller;

use EasyWeChat\Factory;
use app\common\controller\Common as Tool;
use think\Controller;
use think\Db;

class Wxpay extends Controller
{
    protected $config;
    protected $uid;
    public function __construct()
    {
        parent::__construct();
        $this->config = [
            'app_id' => config('site.wx_appid'),
            'secret' => config('site.wx_screte'),
            'mch_id' => config('site.wx_mch_id'),
            'key'    => config('site.wx_mch_key'),
        ];
        $this->uid = authcode(input('token'));
    }

    /**
     * 微信支付
     * @return \think\response\Json
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function wxPay()
    {
        $param = input();

        $member = model('Member')->get($this->uid);
        if (!isset($param['order_no'])) {
            return json(['code' => 44001, 'msg' => '订单信息错误']);
        }
        $order = db('order')->where(['order_no' => $param['order_no'], 'uid' => $member['id']])->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '订单信息错误1']);
        }
        if ($order['pay_state'] != 0) {
            return json(['code' => 44001, 'msg' => '订单状态错误']);
        }

        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '订单充值',
            'out_trade_no' => $order['order_no'],
            'total_fee'    => 1,//$order['pay_cash'] * 100,
            'trade_type'   => 'JSAPI', // 请对应换成你的支付方式对应的值类型
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/api/Wxpay/orderPayNotify',
        ]);
        if ($result['return_code'] == 'FAIL') {
            return json(['code' => 40001, 'msg' => $result['return_msg']]);
        }
        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $prepay_id = $result['prepay_id'];
            $jssdk     = $app->jssdk;
//            $payconfig = $jssdk->bridgeConfig($prepay_id,false);
            $payconfig = $jssdk->sdkConfig($prepay_id);
            return json(['code' => 0, 'msg' => 'sucsess', 'data' => $payconfig]);
        } else {
            return json(['code' => 40001, 'msg' => '微信支付失败']);
        }
    }

    /**
     * [orderPayNotify 订单微信支付回调]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function orderPayNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('order')->where(['order_no' => $message['out_trade_no']])->find();
            if (!$order || $order['pay_state'] == 1) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if ($message['result_code'] === 'SUCCESS') {
                    $data              = [];
                    $data['pay_state'] = 1;
                    $data['pay_time']  = time();
                    $data['pay_name']  = '微信支付';
                    $data['pay_method'] = 1;
                    $data['status']    = 1;
                    $re                = Db::name('order')->where(['order_no' => $message['out_trade_no']])->update($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }


    public function recharge()
    {
        $param = input();
        $member = db('Member')->where(['id'=>$this->uid])->find();

        $order_no = 'RE'.$member['id'].'T'.date('YmdHis');
        $money = $param['money'];
        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '订单充值',
            'out_trade_no' => $order_no,
            'total_fee'    => 1,//$money * 100,
            'trade_type'   => 'JSAPI',
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/api/Wxpay/rechargeNotify',
            'attach'=>json_encode(['uid'=>$this->uid])
        ]);
        if ($result['return_code'] == 'FAIL') {
            return json(['code' => 40001, 'msg' => $result['return_msg']]);
        }

        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $prepay_id = $result['prepay_id'];
            $jssdk     = $app->jssdk;
//            $payconfig = $jssdk->bridgeConfig($prepay_id,false);
            $payconfig = $jssdk->sdkConfig($prepay_id);
            return json(['code' => 0, 'msg' => 'sucsess', 'data' => $payconfig]);
        } else {
            return json(['code' => 40001, 'msg' => '微信支付失败']);
        }
    }

    public function rechargeNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('order')->where(['order_no' => $message['out_trade_no']])->find();
            if ($order) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态

                if ($message['result_code'] === 'SUCCESS') {

                    $data              = [];
                    $data['order_no'] = $message['out_trade_no'];
                    $data['amount'] = $message['total_fee']/100;
                    $data['create_time']  = time();
                    $data['update_time']  = time();
                    $data['uid']    = json_decode($message['attach'],true)['uid'];
                    $re                = db('RechargeOrder')->insert($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }
                    Db::name('Member')->where(['id'=>$data['uid']])->setInc('commision',$data['amount']);
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }

    public function borderPay()
    {
        $param = input();

        $member = model('Member')->get($this->uid);
        if (!isset($param['order_no'])) {
            return json(['code' => 44001, 'msg' => '订单信息错误']);
        }
        $order = db('BuildOrder')->where(['order_no' => $param['order_no']])->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '未找到订单']);
        }

        $money = $order['amount'];
        $use_balance = 0;
        if(isset($param['use_balance']) and $param['use_balance']==1){

            if($member['commision']>=$order['amount']){
                Db::startTrans();
                $re = model('BuildOrder')->where(['id'=>$order['id']])->update(['use_balance'=>$order['amount'],'pay_cash'=>0,'status'=>1]);
                $re1 = model('Member')->where(['id'=>$order['uid']])->setDec('commision',$order['amount']);
                $re2 = model('CommissionLog')->save(['uid'=>$order['uid'],'type'=>'0','amount'=>'-'.$order['amount'],'remark'=>'安装订单：'.$order['order_no'].'消费抵扣'.$order['amount'].'元']);
                if(false !== $re and false !==$re1 and false !==$re2){
                    Db::commit();
                    Tool::editIntegration($order['publisher'],config('site.publish_integration'),2,'发布安装订单'.$param['order_no'].'获的'.config('site.publish_integration').'积分');
                    return json(['code'=>9,'msg'=>'支付成功']);
                }else{
                    Db::rollback();
                    return json(['code'=>40001,'msg'=>'系统错误']);
                }
            }else{
                $money = round($money-$member['commision'],2);
                $use_balance = $member['commision'];
            }
        }
        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '安装订单支付',
            'out_trade_no' => $order['order_no'],
            'total_fee'    => 1,//$money * 100,
            'trade_type'   => 'JSAPI', // 请对应换成你的支付方式对应的值类型
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/api/Wxpay/borderPayNotify',
            'attach'=>json_encode(['use_balance'=>$use_balance])
        ]);
        if ($result['return_code'] == 'FAIL') {
            return json(['code' => 40001, 'msg' => $result['return_msg']]);
        }
        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $prepay_id = $result['prepay_id'];
            $jssdk     = $app->jssdk;
//            $payconfig = $jssdk->bridgeConfig($prepay_id,false);
            $payconfig = $jssdk->sdkConfig($prepay_id);
            return json(['code' => 0, 'msg' => 'sucsess', 'data' => $payconfig]);
        } else {
            return json(['code' => 40001, 'msg' => '微信支付失败']);
        }
    }

    public function borderPayNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('BuildOrder')->where(['order_no' => $message['out_trade_no']])->find();
            if (!$order || $order['status'] == 1) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if ($message['result_code'] === 'SUCCESS') {
                    $data              = [];
                    $data['status'] = 1;
                    $data['pay_cash'] = round($message['total_fee']/100,2);
                    $data['use_balance'] = json_decode($message['attach'],true)['use_balance'];
                    $re                = db('BuildOrder')->where(['order_no' => $message['out_trade_no']])->update($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }
                    Tool::editIntegration($order['uid'],config('site.publish_integration'),2,'发布安装订单'.$order['order_no'].'获的'.config('site.publish_integration').'积分');
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }

    public function rorderPay()
    {
        $param = input();

        $member = model('Member')->get($this->uid);
        if (!isset($param['order_no'])) {
            return json(['code' => 44001, 'msg' => '订单信息错误']);
        }
        $order = db('RepairOrder')->where(['order_no' => $param['order_no'], 'uid' => $member['id']])->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '订单信息错误1']);
        }

        $money = $order['amount'];
        $use_balance = 0;
        if(isset($param['use_balance']) and $param['use_balance']){

            if($member['commision']>=$order['amount']){
                Db::startTrans();
                $re = model('RepairOrder')->where(['id'=>$order['id']])->update(['use_balance'=>$order['amount'],'pay_cash'=>0,'status'=>1]);
                $re1 = model('Member')->where(['id'=>$order['uid']])->setDec('commision',$order['amount']);
                $re2 = model('CommissionLog')->save(['uid'=>$order['uid'],'type'=>'0','amount'=>'-'.$order['amount'],'remark'=>'安装订单：'.$order['order_no'].'消费抵扣'.$order['amount'].'元']);
                if(false !== $re and false !==$re1 and false !==$re2){
                    Db::commit();
                    Tool::editIntegration($this->uid,config('site.publish_integration'),2,'发布维修订单'.$param['order_no'].'获的'.config('site.publish_integration').'积分');
                    return json(['code'=>9,'msg'=>'支付成功']);
                }else{
                    Db::rollback();
                    return json(['code'=>40001,'msg'=>'系统错误']);
                }
            }else{
                $money = round($money-$member['commision'],2);
                $use_balance = $member['commision'];
            }
        }
        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '维修订单支付',
            'out_trade_no' => $order['order_no'],
            'total_fee'    => 1,//$money * 100,
            'trade_type'   => 'JSAPI', // 请对应换成你的支付方式对应的值类型
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/api/Wxpay/rorderPayNotify',
            'attach'=>json_encode(['use_balance'=>$use_balance])
        ]);
        if ($result['return_code'] == 'FAIL') {
            return json(['code' => 40001, 'msg' => $result['return_msg']]);
        }
        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $prepay_id = $result['prepay_id'];
            $jssdk     = $app->jssdk;
//            $payconfig = $jssdk->bridgeConfig($prepay_id,false);
            $payconfig = $jssdk->sdkConfig($prepay_id);
            return json(['code' => 0, 'msg' => 'sucsess', 'data' => $payconfig]);
        } else {
            return json(['code' => 40001, 'msg' => '微信支付失败']);
        }
    }

    public function rorderPayNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('RepairOrder')->where(['order_no' => $message['out_trade_no']])->find();
            if (!$order || $order['status'] == 1) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if ($message['result_code'] === 'SUCCESS') {
                    $data              = [];
                    $data['status'] = 1;
                    $data['pay_cash'] = round($message['total_fee']/100,2);
                    $data['use_balance'] = json_decode($message['attach'],true)['use_balance'];
                    $re                = db('RepairOrder')->where(['order_no' => $message['out_trade_no']])->update($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }
                    Tool::editIntegration($order['uid'],config('site.publish_integration'),2,'发布安装订单'.$order['order_no'].'获的'.config('site.publish_integration').'积分');
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }
}

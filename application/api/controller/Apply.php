<?php
namespace app\api\controller;

class Apply extends Common
{

    /**
     * 保存认证信息
     * @author 雨夜
     * @date 2019年5月22日10:31:43
     * @return \think\response\Json
     */
	public function saveApply()
	{
		$param = input();
		$rule = [
		    'type|认证类型'=>'require|in:1,2,3',
			'name|姓名'=>'require',
            'contacts|联系人'=>'require',
			'mobile|手机号'=>'require|mobile',
            'province|省份'=>'require',
            'city|城市'=>'require',
            'county|县区'=>'require',
			'id_card_f|身份证正面图'=>'require',
			'id_card_b|身份证背面图'=>'require',
			'id_card_h|身份证手持照'=>'require'
		];
		$validate = $this->validate($param,$rule);
		if(true !== $validate){
			return json(['code'=>44001,'msg'=>$validate]);
		}

		$param['uid'] = session('uid');
		$re = model('Apply')->isUpdate(false)->allowField(true)->save($param);
		if(false !== $re){
			return json(['code'=>0,'msg'=>'申请成功']);
		}else{
			return json(['code'=>44005,'msg'=>'申请失败']);
		}
	}

	/**
	 * [delApply 删除申请]
	 * @Author   雨夜
	 * @DateTime 2018-12-20
	 * @return   [type]     [description]
	 */
	public function delApply()
	{
		$param = input();
		$re = db('JoinApply')->where(['id'=>$param['id']])->delete();
		if(false !== $re){
			return json(['code'=>0,'msg'=>'删除成功']);
		}else{
			return json(['code'=>44004,'msg'=>'删除失败']);
		}
	}


	/**
	 * [getLastApply 获取最新申请]
	 * @Author   雨夜
	 * @DateTime 2019-01-05
	 * @return   [type]     [description]
	 */
	public function getLastApply()
	{
		$param =input();
		$map[] = ['uid','eq',session('uid')];
		$info = model('Apply')->where($map)->order('create_time desc')->find();
		return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
	}
}
?>
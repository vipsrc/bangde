<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/5/30
 * Time: 16:15
 */
namespace app\api\controller;

use think\Db;
use think\Request;
use EasyWeChat\Factory;
use app\common\controller\Common as Tool;

class Member extends Common
{

    public function getMember(Request $request)
    {
        $info = Db::name('Member')->where(['id'=>$this->uid])->find();
        $info['order_num'] = Db::name('Order')->where([['uid','=',$this->uid],['is_delete','eq','0']])->count();
        $info['build_num'] = Db::name('BuildOrder')->where([['uid','=',$this->uid],['is_del','eq','0']])->count();
        $info['repair_num'] = Db::name('RepairOrder')->where([['uid','=',$this->uid],['is_del','eq','0']])->count();
        $info['level'] = getLevel($info['role'],$info['totalConsume']);
        $apply = model('Apply')->with('province,city,county,reprovince,recity,recounty')->where(['uid'=>$info['id']])->order('id desc')->find()->toArray();
        if($apply){
            $apply['certificate'] && $apply['certificate'] = explode(',',$apply['certificate']);
            $apply['receive'] && $apply['receive'] = json_decode($apply['receive'],true);
            if(is_array($apply['receive'])){
                foreach($apply['receive'] as $k=>$v){
                    $v['re_province_name'] = Db::name('Region')->where(['region_id'=>$v['re_province']])->value('region_name');
                    $v['re_city_name'] = Db::name('Region')->where(['region_id'=>$v['re_city']])->value('region_name');
                    $apply['receive_text'][$k] =[
                        ['value'=>$v['re_province'],'name'=>$v['re_province_name']],
                        ['value'=>$v['re_city'],'name'=>$v['re_city_name']]
                    ];
                }
            }

            $info['apply'] = $apply;
        }else{
            $info['apply'] = [];
        }
        return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
    }

    public function bindMobile(Request $request)
    {
        $member = Db::name('Member')->where(['id'=>session('uid')])->find();
        if(!$member){
            return json(['code'=>44001,'msg'=>'参数错误']);
        }
        if($request->param('type')=='change'){
            if($member['mobile'] !== $request->param('old_mobile')){
                return json(['code'=>44001,'msg'=>'原手机号错误!']);
            }
        }else{
            if($member['mobile']){
                return json(['code'=>44001,'msg'=>'手机号已绑定']);
            }
        }

        $has = Db::name('Member')->where([['id','neq',session('uid')],['mobile','=',$request->param('mobile')]])->find();
        if($has){
            return json(['code'=>44001,'msg'=>'手机号已被占用！']);
        }
        $re = Db::name('Member')->where(['id'=>session('uid')])->setField('mobile',$request->param('mobile'));
        if(false !== $re){
            return json(['code'=>0,'msg'=>'绑定成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [myCoupon] 我的优惠券
     * @author 雨夜
     * @date 2019/6/5
     * @param Request $request
     * @return \think\response\Json
     */
    public function myCoupon(Request $request)
    {
        $param = $request->param();

        $list = model('UserCoupon')->where(['member_id'=>session('uid')])->page($param['page'],$param['limit'])->select()->each(function($item){
            $item['end_time'] = date('Y-m-d',$item['end_time']);
            $item['coupon_value'] = floor($item['coupon_value']);
            return $item;
        });
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }

    public function wxQrcode()
    {
        $config = [
            'app_id' => config('site.wx_appid'),
            'secret' => config('site.wx_screte'),
            'response_type' => 'array',
        ];
        $user = model('Member')->get(session('uid'));
        if(file_exists('qrcode/'.$user['openid'].'.png')){
            $filename = config('site.site_url')."/qrcode/".$user['openid'].".png";
            return json(['code'=>0,'msg'=>'请求成功','data'=>$filename]);
        }
        $path = input('url');
        $app = Factory::miniProgram($config);
        $response = $app->app_code->getQrCode($path);
        $filename = $response->saveAs('qrcode/', $user['openid'].".png");
        if ($filename) {
            $filename = config('site.site_url')."/qrcode/".$user['openid']."png";

            return json(['code'=>0,'msg'=>'请求成功','data'=>$filename]);
        } else {
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function getBuilder(Request $request)
    {
        $param = $request->param();
        $builder = model('Member')->where(['id'=>$param['builder']])->find();
        $speed_star= model('BuildComment')->where(['builder'=>$builder['id']])->avg('speed_star');
        $service_star= model('BuildComment')->where(['builder'=>$builder['id']])->avg('service_star');
        $quality_star= model('BuildComment')->where(['builder'=>$builder['id']])->avg('quality_star');
        $builder['star'] = round(($speed_star+$service_star+$quality_star)/3);
        $builder['info'] = model('Apply')->where([['uid','=',$builder['id']],['status','=',1],['type','in',['2','3']]])->order('id desc')->find();
        $builder['comment'] = model('BuildComment')->with('user')->where(['builder'=>$builder['id'],'is_show'=>0])->page(input('page',1),input('limit',10))->select()->each(function($item,$key){
            $item['user'] = $item['user'];
            $item['img'] && $item['img'] = explode(',',$item['img']);
            return $item;
        });
        return json(['code'=>0,'msg'=>'请求成功','data'=>$builder]);
    }

    public function changeIntegration(Request $request)
    {
        $param = $request->param();
        if($param['type']==3){
            $amount = config('site.share_progress_integration');
            $remark = '分享直击现场获取'.$amount.'积分';
            $type = 1;

        }elseif($param['type']==4){
            $amount = config('site.share_goods_integration');
            $remark = '分享商品获取'.$amount.'积分';
            $type = 2;
        }elseif($param['type']==5){
            $amount = config('site.share_forum_integration');
            $remark = '分享论坛获取'.$amount.'积分';
            $type = 3;
        }else{
            return json(['code'=>40001,'msg'=>'参数错误']);
        }
        $count = model('ShareLog')->where(['uid'=>session('uid'),'type'=>$type])->whereTime('create_time','today')->count();
        if($count>=config('site.share_limit')){
            return json(['code'=>44004,'msg'=>'次数已达上限，不获得积分']);
        }
        Tool::editIntegration(session('uid'),$amount,$param['type'],$remark);
        model('ShareLog')->save(['uid'=>session('uid'),'type'=>$type,'integration'=>$amount]);
    }
}
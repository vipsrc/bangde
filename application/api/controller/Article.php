<?php
namespace app\api\controller;

class Article extends Common
{
    /**
     * [getArticleList 文章列表集]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
	public function getArticleList()
	{
		$keywords = input('param.keywords', '');
        $page     = input('param.page', 1);
        $limit    = input('param.limit', 10);
        $where    = [];
        $model    = model('Article');
        if ($keywords != '') {
            $where[] = ['title', 'like', "%".$keywords."%"];
        }
        $list = $model->where($where)->page($page, $limit)->order('sort desc,create_time asc')->select()->toArray();
        foreach($list as $k=>$v){
            if($v['imgs']){
                $imgs = explode(',',$v['imgs']);
                foreach($imgs as $kk=>$vv){
                    $list[$k]['imgarr'][$kk] = config('site.site_url').$vv;
                }
            }
        }
        $count = $model->where($where)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
	}

    /**
     * [getArticleInfo 获取文章详情]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function getArticleInfo()
    {
        $param = input();
        $info = model('Article')->get($param['id']);
        if(!$info){
            return json(['code'=>44001,'msg'=>'参数错误']);
        }else{
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }
    }
}
?>
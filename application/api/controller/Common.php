<?php
/**
 * 秋水
 * 2018/11/28
 * 公共控制类
 */
namespace app\api\controller;

use think\Controller;
use \think\auth\Auth;
use think\Db;

class Common extends Controller
{
    public $uid;

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        parent::__construct();
        if (input('?token') and input('token')) {
            $authUser = input('token');
            $token     = authcode($authUser);
            $uid = $token;
            $user = db('Member')->where(['id'=>$token])->find();
            if($user['login_token']!=$authUser){
                echo json_encode(['code' => 40004, 'msg' => '登录超时，请重试！']);
                exit;
            }
            if($user['frozen']==1){
                echo json_encode(['code' => 40004, 'msg' => '账号已被冻结，请联系管理员！']);
                exit;
            }
            session('uid',$uid);
            $this->uid = $uid;
        }

        // $this->auto_receipt();
        // $this->is_exceed_pay_time();
    }

    /**
     * [getData] 获取数据
     * @author 雨夜
     * @date 2019/5/22
     * @return mixed
     */
    private function getData()
    {
        if (request()->isPost()) {
            $str = file_get_contents('php://input');
            return json_decode($str, true);
        }
        if (request()->isGet()) {
            return request()->param();
        }
    }

    //自动收货
    protected function auto_receipt()
    {
        $receipt_day = config('site.recieve_limit');
        $Order = model('Order');
        //获取所有待收货订单id
        $orderList = $Order->where('DATEDIFF(CURDATE(),FROM_UNIXTIME(`pay_time`, "%Y-%m-%d"))>=' . $receipt_day . ' and status=2 and pay_state=1')->select();
        foreach ($orderList as $k => $v) {
            if ($Order->where(array('uid' => $v['uid'], 'oid' => $v['oid']))->setField('status', 3)) {
                //增加商品销量
                $this->addsalenum($v['order_no']);
                $this->fanli($order['id']);
                return 1;
            } else {
                return 0;
            }
        }
    }

    /**
     * [fanli 收货返利]
     * @Author   雨夜
     * @DateTime 2019-01-11
     * @param    [type]     $oid [description]
     * @return   [type]          [description]
     */
    protected function fanli($oid)
    {
        $order = model('Order')->where(['id' => $oid])->find();
        $user = model('AuthUser')->where(['id' => $order['uid']])->find();
        if ($user['pid'] and $order['give_commision'] > 0) {
            Db::name('AuthUser')->where(['id' => $user['pid']])->setInc('commision', $order['give_commision']);
            Db::name('CommissionLog')->insert(['uid' => $order['uid'], 'type' => 1, 'amount' => $order['give_commision'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_commision'] . '元', 'create_time' => time()]);
        }
        if ($order['give_integral'] > 0) {
            Db::name('AuthUser')->where(['id' => $user['id']])->setInc('integration', $order['give_integral']);
            Db::name('IntegrationLog')->insert(['uid' => $order['uid'], 'type' => 1, 'amount' => $order['give_integral'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_integral'] . '积分', 'create_time' => time()]);
        }
    }

    //没有付款的订单取消订单
    protected function is_exceed_pay_time()
    {
        //获取是否设置了付款时限
        if (!config('?site.pay_limit')) return false;
        $where[] = ['create_time', 'lt', time() - (config('site.pay_limit') * 60)];
        $where[] = ['pay_state', 'eq', 0];
        $where[] = ['status', 'neq', -1];
        $Orders = model('Order');
        $list = $Orders->where($where)->select();
        foreach ($list as $k => $v) {
            $re1 = Db::name('Order')->where(['id' => $v['id']])->setField('status', -1);
//            if($v['use_inter']>0){
//                Db::name('AuthUser')->where(['id'=>$v['uid']])->setInc('integration',$v['use_inter']);
//                Db::name('IntegrationLog')->insert(['uid'=>$v['uid'],'type'=>2,'amount'=>$v['use_inter'],'remark'=>'订单'.$v['order_no'].'被取消退还积分','create_time'=>time()]);
//            }
//            if($v['use_commision']>0){
//                Db::name('AuthUser')->where(['id'=>$v['uid']])->setInc('commision',$v['use_commision']);
//                Db::name('CommissionLog')->insert(['uid'=>$v['uid'],'type'=>3,'amount'=>$v['use_commision'],'remark'=>'订单'.$v['order_no'].'被取消退还佣金','create_time'=>time()]);
//            }
        }
    }

    //增加商品销量
    protected function addsalenum($oid)
    {
        $Order = model('OrderGoods');
        $Goods = model('Goods');
        $Ordergoods = $Order->field('gid,num')->where(array('order_no' => $oid))->select();
        foreach ($Ordergoods as $key => $value) {
            $Goods->where(array('id' => $value['gid']))->setInc('sales', $value['num']);
        }
    }
}

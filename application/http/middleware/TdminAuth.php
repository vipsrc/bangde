<?php

namespace app\http\middleware;

use \think\auth\Auth;

class TdminAuth
{
    public function handle($request, \Closure $next)
    {
        //判断管理员是否登录
        if (!session('uid')) {
            if ($request->controller() == 'Index' && $request->action() == 'index') {
                return redirect('tdmin/login/index');
            } else {
                echo '<script type="text/javascript"> window.parent.location.reload(); </script>';
                exit;
            }
        }
        $module     = $request->module();
        $controller = $request->controller();
        $action     = $request->action();
        $this->auth = Auth::instance(); //实例化auth
        $url        = $module . '/' . $controller . '/' . $action;

        $url        = strtolower($url);
        if (session('uid') !== config('app.auth.super_admin')) {
            if (!in_array($url, ['tdmin/index/index', 'tdmin/index/controlboard','tdmin/login/logout'])) {
                $auth = Auth::instance(); //实例化auth
                if (!$auth->check($url, session('uid'),'0')) {
                    //$this->error('暂无权限', url('tdmin/Index/index'));
                }
            }
        }
        return $next($request);
    }
}

<?php

namespace app\http\middleware;

class ApiAuth
{
    public function handle($request, \Closure $next)
    {
        //判断是否登录
        if (input('?token') and input('token')) {
            $authUser = input('token');
            $token     = authcode($authUser);
            $uid = $token;
            $user = db('Member')->where(['id'=>$token])->find();
            if($user['login_token']!=$authUser){
                echo json_encode(['code' => 40004, 'msg' => '登录超时，请重试！']);
                exit;
            }
            if($user['frozen']==1){
                echo json_encode(['code' => 40004, 'msg' => '账号已被冻结，请联系管理员！']);
                exit;
            }
            session('uid',$uid);
        }else{
            echo json_encode(['code' => 40004, 'msg' => '请登录后重试']);
            exit;
        }

        return $next($request);
    }
}

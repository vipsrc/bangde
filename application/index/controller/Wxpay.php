<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2018/12/20
 * Time: 9:05
 */
namespace app\index\controller;

use EasyWeChat\Factory;
use think\Controller;
use think\Db;
use think\Request;
use app\common\controller\Common as Tool;

class Wxpay extends Controller
{
    protected $config;
    protected $uid;
    public function __construct()
    {
        parent::__construct();
        $this->config = [
            'app_id' => config('site.wx_appid'),
            'secret' => config('site.wx_screte'),
            'mch_id' => config('site.wx_mch_id'),
            'key'    => config('site.wx_mch_key'),
        ];
        $this->uid = session('uid');
    }

    /**
     * 微信支付
     * @return \think\response\Json
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function wxPay()
    {
        $param = input();
        $member = model('Member')->get($this->uid);
        $this->assign('member',$member);
        $this->assign('page_title','微信支付');
        if (!isset($param['order_no'])) {
            $this->error('订单信息错误');
        }
        $order = db('order')->where(['order_no' => $param['order_no'], 'uid' => $member['id']])->find();
        if (!$order) {
            $this->error('订单信息错误');
        }

        if ($order['pay_state'] != 0) {
            $this->error('订单状态错误');
        }

        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '订单充值',
            'out_trade_no' => $order['order_no'],
            'total_fee'    => 1,//$order['pay_cash'] * 100,
            'trade_type'   => 'NATIVE', // 请对应换成你的支付方式对应的值类型
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/index/Wxpay/orderPayNotify',
        ]);

        if ($result['return_code'] == 'FAIL') {
            $this->error($result['return_msg']);
        }
        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $code_url = $result['code_url'];
            $this->assign('code_url',$code_url);
            $this->assign('order_no',$order['order_no']);
            return view();
        } else {
            $this->error('获取支付信息失败',url('User/index'));
        }
    }

    /**
     * [orderPayNotify 订单微信支付回调]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function orderPayNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('order')->where(['order_no' => $message['out_trade_no']])->find();
            if (!$order || $order['pay_state'] == 1) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if ($message['result_code'] === 'SUCCESS') {
                    $data              = [];
                    $data['pay_state'] = 1;
                    $data['pay_time']  = time();
                    $data['status']    = 1;
                    $re                = db('order')->where(['order_no' => $message['out_trade_no']])->update($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }

    public function orderQuery(Request $request)
    {
        $param = $request->param();
//        $order = model('Order')->where(['order_no'=>$param['order_no']])->find();
//        if(!$order){
//            return json(['code'=>40001,'data'=>'REVOKED']);
//        }
        $app    = Factory::payment($this->config);
        $result = $app->order->queryByOutTradeNumber($param['order_no']);
        if ($result['return_code'] == 'FAIL') {
            return json(['code' => 40001]);
        }
        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            return json(['code' => 0,'data'=>$result['trade_state']]);
        } else {
            return json(['code' => 40001,'data'=>$result['trade_state']]);
        }
    }

    public function recharge()
    {
        $param = input();
        $validate = $this->validate($param,['money|金额'=>'number']);
        if(true!=$validate){
            $this->error($validate,url('user/mywallet'));
        }
        $member = db('Member')->where(['id'=>$this->uid])->find();
        $this->assign('member',$member);
        $order_no = 'RE'.$member['id'].'T'.date('YmdHis');
        $money = $param['money'];
        $app    = Factory::payment($this->config);
        $result = $app->order->unify([
            'body'         => '订单充值',
            'out_trade_no' => $order_no,
            'total_fee'    => $money * 100,
            'trade_type'   => 'NATIVE',
            'openid'       => $member['openid'],
            'notify_url'   => config('site.site_url') . '/index/Wxpay/rechargeNotify',
            'attach'=>json_encode(['uid'=>$this->uid])
        ]);
        if ($result['return_code'] == 'FAIL') {
            $this->error($result['return_msg']);
        }

        if ($result['return_code'] == 'SUCCESS' and $result['result_code'] == 'SUCCESS') {
            $code_url = $result['code_url'];
            $this->assign('code_url',$code_url);
            $this->assign('order_no',$order_no);
            $this->assign('page_title','充值支付');
            return view();
        } else {
            $this->error('微信支付失败');
        }
    }

    public function rechargeNotify()
    {
        $app      = Factory::payment($this->config);
        $response = $app->handlePaidNotify(function ($message, $fail) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order = db('RechargeOrder')->where(['order_no' => $message['out_trade_no']])->find();
            if ($order) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            if ($message['return_code'] === 'SUCCESS') {
                // return_code 表示通信状态，不代表支付状态

                if ($message['result_code'] === 'SUCCESS') {
                    $data              = [];
                    $data['order_no'] = $message['out_trade_no'];
                    $data['amount'] = $message['total_fee']/100;
                    $data['create_time']  = time();
                    $data['update_time']  = time();
                    $data['uid']    = json_decode($message['attach'],true)['uid'];
                    $re                = db('RechargeOrder')->insert($data);
                    if (!$re) {
                        return $fail('订单处理失败，稍后重试');
                    }

                    Tool::editCommission($data['uid'],$data['amount'],'6',date('Y-m-d H:i:s').'成功充值'.$data['amount'].'元');
                } elseif ($message['result_code'] === 'FAIL') {
                    // 用户支付失败
                    return true;
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        $response->send(); // return $response;
    }


}

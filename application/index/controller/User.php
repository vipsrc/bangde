<?php
/**
 * User.php
 * desc:
 * created on  2019/6/18 2:41 PM
 * Created by caogu
 */

namespace app\index\controller;


use app\api\controller\UserAddress;
use app\common\model\Member;
use app\common\model\Order;
use think\Db;
use think\Request;

class User extends Common
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->assign('page_title','个人中心');
        if(!session('uid')){
            $this->redirect('Login/index');
        }
    }

    public function index(){

        $uid=$this->uid;
        $member_info=(new Member())->find($uid);
        $this->assign('member_info',$member_info);
//        dump($member_info);
        $orderModel=new Order();
        //最新三条订单-所有订单
        $where=[
            'uid'=>$uid,
            'is_delete'=>0
        ];
        $order_list_all=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->limit(0,3)->select();
        $this->assign('order_all',$order_list_all);

        //取3条未付款订单
        $where['status']=0;
        $no_pay_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->limit(0,3)->select();
        $this->assign('no_pay_order',$no_pay_order);
        //未付款的数量
        $no_pay_count=$orderModel->where($where)->count();
        $this->assign('no_pay_count',$no_pay_count);

        //取3条未发货订单
        $where['status']=1;
        $wait_deliver_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->limit(0,3)->select();
        $this->assign('wait_deliver_order',$wait_deliver_order);
        //未发货的数量
        $wait_deliver_count=$orderModel->where($where)->count();
        $this->assign('wait_deliver_count',$wait_deliver_count);

        //取3条待收货订单
        $where['status']=2;
        $wait_received_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->limit(0,3)->select();
        $this->assign('wait_received_order',$wait_received_order);
        //待收货的数量
        $wait_received_count=$orderModel->where($where)->count();
        $this->assign('wait_received_count',$wait_received_count);

        //取3条未评价订单
        $where['status']=3;
        $no_comment_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->limit(0,3)->select();
        $this->assign('no_comment_order',$no_comment_order);
        //未评价的数量
        $no_comment_count=$orderModel->where($where)->count();
        $this->assign('no_comment_count',$no_comment_count);
        $mycoupon = model('UserCoupon')->where(['member_id'=>session('uid'),'status'=>0])->count();
        $this->assign('mycoupon',$mycoupon);

        //爆款推荐
        $where=[
            'status'=>1,
            'is_del'=>0,
            'is_recom'=>1
        ];
        $recom_goos_list=model('Goods')->where($where)->limit(0,5)->select();
        $this->assign('recom_goos_list',$recom_goos_list);

        return $this->fetch();
    }

    public function edit_info(){
        $uid=$this->uid;

        $member_info=(new Member())->find($uid);
        $this->assign('member_info',$member_info);
        dump($member_info);
        return $this->fetch();
    }

    public function change_avatar(){
        dump(input('param.'));
    }

    /**
     * 增加收获地址页面
     * @return mixed|\think\response\Json
     */
    public function add_address(){
        if(request()->isAjax()){
            $param = input();
            if($param['province']=='请选择'){
                return json(['code'=>44001,'msg'=>'请选择省市区']);
            }

            $rule = [
                'consignee|收货人'=>'require',
                'province|省份'=>'require',
                'city|市区'=>'require',
                'county|县区'=>'require',
                'address|详细地址'=>'require',
                'tel|手机号'=>'require|mobile'
            ];
            $validate = (new UserAddress())->validate($param,$rule);
            if(true !== $validate){
                return json(['code'=>44001,'msg'=>$validate]);
            }

            if($param['is_default']){
                db('UserAddress')->where(['uid'=>$this->uid])->setField('is_default',0);
            }
            $param['uid'] = $this->uid;
            if(isset($param['address_id'])){
                $param['id'] = $param['address_id'];
                $re = model('UserAddress')->isUpdate(true)->allowField(true)->save($param);
            }else{
//            $has = model('UserAddress')->where(['uid'=>$this->uid])->find();
//            $param['is_default'] = empty($has) ? 1 : 0;
                $re = model('UserAddress')->isUpdate(false)->allowField(true)->save($param);
            }
            if(false !== $re){
                return json(['code'=>0,'msg'=>'保存成功']);
            }else{
                return json(['code'=>40001,'msg'=>'系统错误']);
            }
        }
        //获取用户地址列表
        //收货地址
        $map[] = ['a.uid','eq',$this->uid];
        $list = Db::name('user_address a')
            ->join('region b','a.province = b.region_id')
            ->join('region c','a.city = c.region_id')
            ->join('region d','a.county = d.region_id')
            ->field('a.*,b.region_name as province_name,c.region_name as city_name,d.region_name as county_name')
            ->where($map)->order('a.is_default desc')->select();
        $this->assign('address',$list);
        //省份数据
        $province = model('Region')->where(['region_type'=>1])->select();
        $this->assign('province',$province);
        return $this->fetch();
    }

    /**
     * 修改收获地址页面
     * @return mixed|\think\response\Json
     */
    public function edit_address(){
        if(request()->isAjax()){
            $param = input();
            if($param['province']=='请选择'){
                return json(['code'=>44001,'msg'=>'请选择省市区']);
            }

            $rule = [
                'consignee|收货人'=>'require',
                'province|省份'=>'require',
                'city|市区'=>'require',
                'county|县区'=>'require',
                'address|详细地址'=>'require',
                'tel|手机号'=>'require|mobile'
            ];

            $validate = (new UserAddress())->validate($param,$rule);
            if(true !== $validate){
                return json(['code'=>44001,'msg'=>$validate]);
            }

            if($param['is_default']){
                db('UserAddress')->where(['uid'=>$this->uid])->setField('is_default',0);
            }
            $param['uid'] = $this->uid;
            if(isset($param['address_id'])){
                $param['id'] = $param['address_id'];
                $re = model('UserAddress')->isUpdate(true)->allowField(true)->save($param);
            }else{
//            $has = model('UserAddress')->where(['uid'=>$this->uid])->find();
//            $param['is_default'] = empty($has) ? 1 : 0;
                $re = model('UserAddress')->isUpdate(false)->allowField(true)->save($param);
            }
            if(false !== $re){
                return json(['code'=>0,'msg'=>'保存成功']);
            }else{
                return json(['code'=>40001,'msg'=>'系统错误']);
            }
        }
        $address_id=input('param.addr_id');
        if(!$address_id){
            $this->redirect(url('add_address'));
        }
        $this->assign('addr_id',$address_id);
        //详情

        $map[] = ['id','eq',$address_id];
        $info = model('UserAddress')->where($map)->find()->toArray();
        if($info){
            $info['province_name'] = Db::name('Region')->where(['region_id'=>$info['province']])->value('region_name');
            $info['city_name'] = Db::name('Region')->where(['region_id'=>$info['city']])->value('region_name');
            $info['county_name'] = Db::name('Region')->where(['region_id'=>$info['county']])->value('region_name');
        }
        $this->assign('addr_info',$info);

        //获取用户地址列表

        //收货地址
        $map=[];
        $map[] = ['a.uid','eq',$this->uid];
        $list = Db::name('user_address a')
            ->join('region b','a.province = b.region_id')
            ->join('region c','a.city = c.region_id')
            ->join('region d','a.county = d.region_id')
            ->field('a.*,b.region_name as province_name,c.region_name as city_name,d.region_name as county_name')
            ->where($map)->order('a.is_default desc')->select();
        $this->assign('address',$list);
        //省份数据
        $province = model('Region')->where(['region_type'=>1])->select();
        $this->assign('province',$province);
        return $this->fetch();
    }

    /**
     * 设为默认地址
     * @return \think\response\Json
     */
    public function set_default_addr(){
        $param = input();
        db('UserAddress')->where(['uid'=>$this->uid])->setField('is_default',0);
        $re = db('UserAddress')->where(['id'=>$param['address_id'],'uid'=>$this->uid])->setField('is_default',1);
        if(false!== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function del_addr(){
        $param = input();
        if(db('UserAddress')->where(['id'=>$param['address_id'],'is_default'=>1])->find()){
            return json(['code'=>40001,'msg'=>'默认地址不可删除！']);
        }
        $re = db('UserAddress')->where(['id'=>$param['address_id']])->delete();
        if(false!== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function order_list(){
        $uid=$this->uid;
        $member_info=(new Member())->find($uid);
        $this->assign('member_info',$member_info);
//        dump($member_info);
        $orderModel=new Order();
        //最新三条订单-所有订单
        $where=[
            'uid'=>$uid,
            'is_delete'=>0
        ];
        $order_list_all=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->select();
        $this->assign('order_all',$order_list_all);

        //取3条未付款订单
        $where['status']=0;
        $no_pay_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->select();
        $this->assign('no_pay_order',$no_pay_order);

        //未付款的数量
        $no_pay_count=$orderModel->where($where)->count();
        $this->assign('no_pay_count',$no_pay_count);

        //取3条未发货订单
        $where['status']=1;
        $wait_deliver_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->select();
        $this->assign('wait_deliver_order',$wait_deliver_order);
        //未发货的数量
        $wait_deliver_count=$orderModel->where($where)->count();
        $this->assign('wait_deliver_count',$wait_deliver_count);

        //取3条待收货订单
        $where['status']=2;
        $wait_received_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->select();
        $this->assign('wait_received_order',$wait_received_order);
        //待收货的数量
        $wait_received_count=$orderModel->where($where)->count();
        $this->assign('wait_received_count',$wait_received_count);

        //取3条未评价订单
        $where['status']=3;
        $no_comment_order=$orderModel->with('orderGoods,order_goods.goods')->where($where)->order('id','desc')->select();
        $this->assign('no_comment_order',$no_comment_order);
        //未评价的数量
        $no_comment_count=$orderModel->where($where)->count();
        $this->assign('no_comment_count',$no_comment_count);
        return $this->fetch();
    }

    public function order_detaile(){
        $param = input();
        $map   = [];
        $map[] = ['id', 'eq', $param['id']];
        $info  = model('Order')->with('OrderGoods,OrderGoods.Goods')->where($map)->find()->toArray();
        if($info['status']=='0'){
            $info['pay_limit'] = strtotime($info['create_time'])+(config('site.pay_limit')*3600);
        }
        if($info['status']=='2'){
            $info['recieve_limit'] = $info['pay_time']+(config('site.recieve_limit')*86400);
        }
        $num = 0;
        foreach($info['order_goods'] as $k=>$v){
            $info['order_goods'][$k]['goods']['img'] = config('site.site_url').$v['goods']['img'];
            $comment = Db::name('comment')->where(['order_no'=>$info['order_no'],'gid'=>$v['gid']])->find();
            if($comment){
                $info['ordergodos'][$k]['comment'] = $comment;
            }
            $num+=$v['num'];
        }
        $info['total']['goods_num'] = $num;
        $this->assign('info',$info);
        if($info['coupon_id']){
            $info['coupon'] = model('UserCoupon')->where(['id'=>$info['coupon_id']])->find();
        }

        return $this->fetch();
    }

    public function cancelOrder()
    {
        $param = input();
        $re = model('Order')->where(['id'=>$param['oid']])->setField('status','-1');
        if(false !==$re){
            $this->success('取消成功',url('User/order_list'));
            exit;
        }else{
            $this->error('失败请重试！');
        }
    }

    public function orderComment(Request $request)
    {
        $order = model('Order')->where(['id'=>$request->param('oid')])->find();
        if(!$order){
            $this->error('未找到订单');
        }
        $this->assign('info',$order);
        return view();
    }

    public function mycoupon(Request $request)
    {
        $param = $request->param();
        $map = [];
        if(isset($param['status'])){
            $map[] = ['status','eq',$param['status']];
        }else{
            $map[] = ['status','eq',0];
        }
        $map[] = ['member_id','eq',session('uid')];
        $list = model('UserCoupon')->where($map)->paginate(4,'false',['query'=>$request->param()])->each(function($item,$key){
            $item['end_time'] = date('Y-m-d',$item['end_time']);
            return $item;
        });
        $page = $list->render();
        $this->assign('page',$page);
        $this->assign('list',$list);
        return view();
    }

    public function mywallet()
    {
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $this->assign('member',$member);
        $log = model('CommissionLog')->where(['uid'=>session('uid')])->order('create_time desc')->select();
        $this->assign('log',$log);
        return view();
    }

    public  function myintegration()
    {
        $member = model('Member')->where(['id'=>session('uid')])->find();
        $this->assign('member',$member);
        $log = model('IntegrationLog')->where(['uid'=>session('uid')])->order('create_time desc')->paginate(10);
        $this->assign('log',$log);
        return view();
    }

    public function setting(Request $request)
    {
        $param = $request->param();
        if($request->isAjax()){
            if(!$param['realname'] || !$param['mobile']){
                return json(['code'=>44004,'msg'=>'参数错误']);
            }
            $re = model('Member')->where(['id'=>session('uid')])->update(['realname'=>$param['realname'],'mobile'=>$param['mobile']]);
            if(false !== $re){
                return json(['code'=>0,'msg'=>'修改成功']);
            }else{
                return json(['code'=>4001,'msg'=>'修改失败']);
            }
        }
        return view();
    }

    public function saveWithdraw()
    {
        $param = input();
        if(!isset($param['amount']) || empty($param['amount']) || $param['amount']<='0' || !is_numeric($param['amount'])){
            return json(['code'=>44001,'msg'=>'参数错误']);
        }
        $user = model('Member')->get(session('uid'));
        if($param['amount']>$user['commision']){
            return json(['code'=>44001,'msg'=>'余额不足']);
        }
        $param['uid'] = session('uid');
        $has = db('WithdrawWay')->where(['uid'=>$this->uid])->find();
        if($has){
            $param['id'] = $has['id'];
            $re = model('WithdrawWay')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('WithdrawWay')->isUpdate(false)->allowField(true)->save($param);
        }
        $param['way'] = 2;
        Db::startTrans();
        $re1 = Db::name('Member')->where(['id'=>$user['id']])->setDec('commision',round($param['amount'],2));
        $re2 = Db::name('CommissionLog')->insert(['uid'=>$user['id'],'type'=>2,'amount'=>'-'.round($param['amount'],2),'remark'=>'申请提现'.round($param['amount'],2).'元','create_time'=>time()]);
        $re3 = Db::name('Withdraw')->insert(['uid'=>$user['id'],'way'=>$param['way'],'amount'=>$param['amount'],'create_time'=>time(),'update_time'=>time()]);
        if(false != $re1 and false !=$re2 and false!=$re3){
            Db::commit();
            return json(['code'=>0,'msg'=>'申请成功，请等待审核！']);
        }else{
            Db::rollback();
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }


    public function saveWithdrawWay()
    {
        $param = input();
        $param['uid'] = session('uid');

        $has = db('WithdrawWay')->where(['uid'=>$this->uid])->find();
        if($has){
            $param['id'] = $has['id'];
            $re = model('WithdrawWay')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('WithdrawWay')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }
}
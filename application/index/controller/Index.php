<?php
/**
 * 秋水
 * 2018/11/28
 * 默认控制类
 */
namespace app\index\controller;

use app\api\controller\Cart;
use app\common\model\Cate;
use app\common\model\Comment;
use app\common\model\Progress;
use app\common\model\Region;
use think\Db;
use think\Request;

class Index extends Common
{
    //首页
    public function index()
    {
        //商品列表
        $param = input();
        $discount = 100;
        if(session('?uid')){
            $member = model('Member')->where('id',session('uid'))->find();
            $memberlevel = getLevel($member['role'],$member['totalConsume']);
            $discount = $memberlevel['discount'];
        }
        $map = [];
        $map[] = ['is_del', 'eq', '0'];
        $map[] = ['status', 'eq', '1'];
        if (isset($param['type']) and $param['type'] = 'recom') {
            $map[] = ['is_recom', 'eq', 1];
        }
        if (isset($param['type']) and $param['type'] = 'cart_recom') {
            $map[] = ['cart_recom', 'eq', 1];
        }
        if (isset($param['cate']) and $param['cate']) {
            $cate = [];
            $cate1 = Db::name('Cate')->where(['id' => $param['cate']])->find();
            if ($cate1['parentid'] == 0) {
                $cate2 = Db::name('Cate')->where(['parentid' => $cate1['id']])->column('id');
                foreach ($cate2 as $k => $v) {
                    $cate3 = Db::name('Cate')->where(['parentid' => $v['id']])->column('id');
                    $cate = array_merge($cate, $cate3);
                }
            } else {
                $cate = Db::name('Cate')->where(['parentid' => $param['cate']])->column('id');
                $cate[] = $param['cate'];
                $map[] = ['cid', 'in', $cate];
            }
            $map[] = ['cid', 'in', $cate];
        }
        if (isset($param['keywords']) and $param['keywords']) {
            $map[] = ['name', 'like', '%' . $param['keywords'] . '%'];
        }
        $list = model('Goods')->where($map)->page(input('page', 1), input('limit', 10))->order('sort desc')->select()->toArray();
        foreach ($list as $k => $v) {
            //使用相对路径可以显示图片，使用绝对路径，本地的没有显示出来，所以先注释掉
//            $list[$k]['img'] = config('site.site_url').$v['img'];
            $list[$k]['price'] = round($v['price'] * $discount / 100, 2);
        }

        $this->assign('goods_list', $list);
        //轮播图左侧的商品分类
        $categoryList = db('Cate')->order('sort desc')->field('id,name,parentid,icon')->select();
        $arr = cate_merge($categoryList, null, 0, 'id', 'parentid');

        $this->assign('category_list', $arr);
        //轮播图
        $slide_list = model('slide')->page(1, 5)->order('sort desc')->select()->toArray();
        foreach ($slide_list as $k => $v) {
//            $slide_list[$k]['img_url'] = config('site.site_url').$v['img'];
            $slide_list[$k]['img_url'] = $v['img'];
        }
        $this->assign('slide_list', $slide_list);
        $this->assign('key', '');
        //省份数据
        $province = Region::where(['region_type' => 1])->order('areaCode asc')->select();
        $this->assign('province', $province);
        $device = Db::name('DeviceType')->select();
        $this->assign('device', $device);
        //询价列表
        $intention = model('Intention')->with('user,province,city,device')->limit(10)->order('create_time desc')->select();
        $intentioncount = model('Intention')->count();
        $this->assign('intention',$intention);
        $this->assign('intenttioncount',$intentioncount);
        return $this->fetch();
    }

    //商品列表页
    public function goods_list()
    {
        //轮播图左侧的商品分类
        $categoryList = db('Cate')->order('sort desc')->field('id,name,parentid,icon')->select();
        $arr = cate_merge($categoryList, null, 0, 'id', 'parentid');
        $this->assign('category_list', $arr);
        $param = input();
        $map = [];
        //类别ID
        $cid = input('param.cate');
        $this->assign('cate', $cid);
        //类别名称
        $cate_name = (new Cate())->where('id', '=', $cid)->value('name');
        //搜索关键词
        $key = input('param.key');
        $this->assign('key', $key);
        if ($key) {
            $map[] = ['name', 'like', "%$key%"];
            $cate_name = $key;
        }
        $this->assign('cate_name', $cate_name);
        $discount = 100;
//        if(session('?uid')){
//            $member = model('Member')->where('id',session('uid'))->find();
//            $memberlevel = getLevel($member['role'],$member['totalConsume']);
//            $discount = $memberlevel['discount'];
//        }
        $map[] = ['is_del', 'eq', '0'];
        $map[] = ['status', 'eq', '1'];
        if (isset($param['type']) and $param['type'] = 'recom') {
            $map[] = ['is_recom', 'eq', 1];
        }
        if (isset($param['type']) and $param['type'] = 'cart_recom') {
            $map[] = ['cart_recom', 'eq', 1];
        }
        if (isset($param['cate']) and $param['cate']) {
            $cate = $this->get_all_child_cate($param['cate'], $param['cate']);
            $map[] = ['cid', 'in', $cate];
        }
//        dump($map);
        if (isset($param['keywords']) and $param['keywords']) {
            $map[] = ['name', 'like', '%' . $param['keywords'] . '%'];
        }
        $list = model('Goods')->where($map)
            ->page(input('page', 1), input('limit', 20))
            ->order('sort desc')
            ->select()
            ->toArray();
        foreach ($list as $k => $v) {
            //使用相对路径可以显示图片，使用绝对路径，本地的没有显示出来，所以先注释掉
//            $list[$k]['img'] = config('site.site_url').$v['img'];
            $list[$k]['price'] = round($v['price'] * $discount / 100, 2);
        }
        $count = model('Goods')->where($map)->count();
        $this->assign('count', $count);
        $this->assign('goods_list', $list);
        //推荐商品列表
        $where = [
            'status' => 1,
            'is_del' => 0,
            'is_recom' => 1
        ];
        $recom_goos_list = model('Goods')->where($where)->limit(0, 6)->select();
        $this->assign('recom_goos_list', $recom_goos_list);
        //分页
        $page = input('param.page', 1);
        $allpage = ceil($count / input('limit', 20));
        $fenye = $this->fenye($page, $allpage, $cid);
        $this->assign('fenye', $fenye);
        $this->assign('page', $page);
        $this->assign('allpage', $allpage);
        return $this->fetch();
    }

    /**商品详情页
     * @return mixed|\think\response\Json
     */
    public function goods_detaile()
    {
        $id = input('param.id');
        if (!$id) {
            $this->redirect(url('index/index'));
        }
        $this->assign('id', $id);
        //轮播图左侧的商品分类
        $categoryList = db('Cate')->order('sort desc')->field('id,name,parentid,icon')->select();
        $arr = cate_merge($categoryList, null, 0, 'id', 'parentid');
        $this->assign('category_list', $arr);
        //获取商品详情-来自api/controller/goods/getGoodsInfo，雨夜的 方法
        $info = model('Goods')->with(['gallery', 'goodsAttr', 'cate', 'goodsAttr.attribute'])->where(['id' => $id])->find();
        if (!$info) {
            return json(['code' => 44001, 'msg' => '参数错误', 'desc' => '未找到该商品']);
        }
        $info['parameter'] && $info['parameter'] = json_decode($info['parameter'], true);
        //使用相对路径可以显示图片，使用绝对路径，本地的没有显示出来，所以先注释掉
//        $info['img'] = config('site.site_url').$info['img'];
        $info['infor'] = str_replace('src="', 'src="' . config('site.site_url'), $info['infor']);
        foreach ($info['gallery'] as $k => $v) {
//            $info['gallery'][$k] = config('site.site_url').$v['img_url'];
            $info['gallery'][$k] = $v['img_url'];
        }
        $attr = [];
        foreach ($info['goods_attr'] as $k => $v) {
            if (isset($attr[$v['attr_id']])) {
                unset($v['attribute']);
                $attr[$v['attr_id']]['values'][] = $v;
            } else {
                $attr[$v['attr_id']]['name'] = $v['attribute']['attr_name'];
                unset($v['attribute']);
                $attr[$v['attr_id']]['values'][] = $v;
            }
        }
        unset($info['goods_attr']);
        $info['attribute'] = $attr;
        if (session('uid')) {
            $collection = Db::name('Collection')->where([['uid', 'eq', session('uid')], ['gid', 'eq', $info['id']]])->find();
            $info['is_collect'] = $collection ? 1 : 0;
        } else {
            $info['is_collect'] = 0;
        }
        $this->assign('goods_info', $info);

        //商品评价列表和数量
        $where = [
            'gid' => $id,
            'is_show' => 0
        ];
        $commentModel = new Comment();
        $page = input('param.page', 1);
        $limit = input('param.limit', 8);
        $comment_count = $commentModel->where($where)->count();
        $comment_list = $commentModel->with('goods,user')->where($where)->page($page, $limit)->order('id', 'desc')->select();
        $this->assign('comment_count', $comment_count);
        $this->assign('comment_list', $comment_list);
        $fenye = $this->comment_fenye($page, $comment_count, $id);
        $this->assign('fenye', $fenye);
        //爆款推荐
        $where = [
            'status' => 1,
            'is_del' => 0,
            'is_recom' => 1
        ];
        $recom_goos_list = model('Goods')->where($where)->limit(0, 6)->select();
        $this->assign('recom_goods_list', $recom_goos_list);
        $this->assign('key', '');
        //相关推荐
        $cid = Db::name('goods')->where('id', '=', $id)->value('cid');
        $where = [];
        $where[] = ['status', 'eq', 1];
        $where[] = ['is_del', 'eq', 0];
        $where[] = ['cid', 'eq', $cid];
        $where[] = ['id', 'notin', $id];
        $cate_goos_list = model('Goods')->where($where)->limit(0, 6)->select();
        $this->assign('cate_goods_list', $cate_goos_list);
        //三个专区块
        //临时任意选取三个分类，作为展示
        $zhuanqu_cid = '1,2,3';
        return $this->fetch();
    }

    /**
     * 递归获取一个商品类别下的所有商品类别id，并返回逗号拼接的ID字符串
     * @param $cid
     * @param string $str
     * @return string
     */
    public function get_all_child_cate($cid, $str = '')
    {
        $child = (new Cate())->where('parentid', '=', $cid)->column('id');
        if ($child) {
            $str .= ',' . implode(',', $child);
            foreach ($child as $key => $val) {
                $str = $this->get_all_child_cate($val, $str);
            }
        }
        return trim($str, ',');
    }

    /**
     * 商品分页
     * @param $page
     * @param $allpage
     * @param $cate
     * @return string
     */
    public function fenye($page, $allpage, $cate)
    {
        $url = url('index/index/goods_list', ['cate' => $cate]);
        if ($allpage <= 1) {
            return '';
        }
        //分页
        $fenye = '<div class="page text-right clearfix">';
        if ($page > 1) {
            $prve = $page - 1;
            $fenye .= '<a href="' . $url . '?page=1"><b>首页</b></a>&nbsp;<a href="' . $url . '?page=' . $prve . '">上一页</a>&nbsp;';
        }
        $start = $page - 2 > 0 ? $page - 2 : 1;
        $end = $page + 2 < $allpage ? $page + 2 : $allpage;
        for ($i = $start; $i <= $end; $i++) {
            if ($i == $page) {
                $fenye .= '&nbsp;&nbsp;<a href="javascript:;" class="select">' . $page . '</a>&nbsp;';
            } else {
                $fenye .= '<a href="' . $url . '?page=' . $i . '">' . $i . '</a>&nbsp;';
            }
        }
        if ($page < $allpage) {
            $next = $page + 1;
            $fenye .= '<a href="' . $url . '?page=' . $next . '">下一页</a>&nbsp;<a href="' . $url . '?page=' . $allpage . '">尾页</a>';
        }
        $fenye .= '<a class="disabled">' . $page . '/' . $allpage . '页</a>
				<form action="' . $url . '" method="get" class="page-order">
					到第
					<input type="text" value="' . $page . '" name="page">
					页
					<input class="sub" type="submit" value="确定">
				</form>
				</div>';
        return $fenye;
    }

    /**
     * 评论分页
     * @param $page
     * @param $allpage
     * @param $goods_id
     * @return string
     */
    public function comment_fenye($page, $allpage, $goods_id)
    {
        $url = url('index/index/goods_detaile', ['id' => $goods_id]);
        if ($allpage <= 1) {
            return '';
        }
        //分页
        $fenye = '<div class="page text-right clearfix">';
        if ($page > 1) {
            $prve = $page - 1;
            $fenye .= '<a href="' . $url . '?page=1"><b>首页</b></a>&nbsp;<a href="' . $url . '?page=' . $prve . '">上一页</a>&nbsp;';
        }
        $start = $page - 2 > 0 ? $page - 2 : 1;
        $end = $page + 2 < $allpage ? $page + 2 : $allpage;
        for ($i = $start; $i <= $end; $i++) {
            if ($i == $page) {
                $fenye .= '&nbsp;&nbsp;<a href="javascript:;" class="select">' . $page . '</a>&nbsp;';
            } else {
                $fenye .= '<a href="' . $url . '?page=' . $i . '">' . $i . '</a>&nbsp;';
            }
        }
        if ($page < $allpage) {
            $next = $page + 1;
            $fenye .= '<a href="' . $url . '?page=' . $next . '">下一页</a>&nbsp;<a href="' . $url . '?page=' . $allpage . '">尾页</a>';
        }
        $fenye .= '<a class="disabled">' . $page . '/' . $allpage . '页</a>
				<form action="' . $url . '" method="get" class="page-order">
					到第
					<input type="text" value="' . $page . '" name="page">
					页
					<input class="sub" type="submit" value="确定">
				</form>
				</div>';
        return $fenye;
    }

    public function saveIntention(Request $request)
    {

        $param = $request->param();
        $rule = [
            'province'=>'require',
            'city'=>'require',
            'area'=>'require|number',
            'house_model'=>'require',
            'house_type'=>'require',
            'device_type'=>'require'
        ];
        $validate = $this->validate($param,$rule);
        if(true!==$validate){
            return json(['code'=>44004,'msg'=>$validate]);
        }
        if(!session('?uid')){
            return json(['code'=>40004,'msg'=>"请登录后重试！"]);
        }
        $param['uid'] = session('uid');
        $re = model('Intention')->allowField(true)->save($param);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'提交成功，客服会与您联系！']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

}

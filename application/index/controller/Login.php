<?php
/**
 * Login.php
 * desc:
 * created on  2019/6/18 2:13 PM
 * Created by caogu
 */

namespace app\index\controller;

use EasyWeChat\Factory;
use think\Controller;
use think\Request;

class Login extends Controller
{
    public function index()
    {
        if(session('uid')){
            $this->redirect('index/index');
        }
        $appid = 'wx550c010aad2baa39';
        $this->assign('appid',$appid);
        $redirect_uri = \think\facade\Request::domain().'/index/Login/login';
        $this->assign('redirect_uri',urlencode($redirect_uri));
        return $this->fetch();
    }

    public function login(Request $request)
    {
        $code = $request->param('code');
        if(!$code){
            $this->error('登录失败,请重试!');
        }
        $appid = 'wx550c010aad2baa39';
        $secret = 'a52d199433e300429d64ad051e949b9d';
        $code = $request->param('code');
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';
        $result = http_request($url);
        $result = json_decode($result,true);
        if(!$result || !isset($result['unionid'])){
            $this->error('登录失败,请重试!');
        }
        $info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$result['access_token'].'&openid='.$result['openid'];
        $userInfo = http_request($info_url);
        $userInfo = json_decode($userInfo,true);
        if(!$userInfo || isset($userInfo['errcode'])){
            $this->error('获取用户信息失败！');
        }
        $ishas = model('Member')->where(['union_id'=>$userInfo['unionid']])->find();
        $member = model('Member');
        if($ishas){
            $data = [
                'nickname'=>$userInfo['nickname'],
                'avatar'=>$userInfo['headimgurl'],
                'pc_openid'=>$userInfo['openid'],
            ];
            $re = $member->where(['id'=>$ishas['id']])->update($data);
            $uid = $ishas['id'];
        }else{
            $data = [
                'pc_openid'=>$userInfo['openid'],
                'union_id'=>$userInfo['unionid'],
                'nickname'=>$userInfo['nickname'],
                'realname'=>$userInfo['nickname'],
                'avatar'=>$userInfo['headimgurl'],
            ];
            $re = $member->save($data);
            $uid = $member->id;
        }
        if(false !== $re){
            session('uid',$uid);
            $this->redirect('index/index');
        }else{
            $this->error('用户数据更新失败');
        }
    }


    public function logout()
    {
        session(null);
        $this->success('退出成功',url('Index/index'));
    }
}
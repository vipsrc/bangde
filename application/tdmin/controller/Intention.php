<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/6/26
 * Time: 15:47
 */
namespace app\tdmin\controller;

use think\Request;

class Intention extends Common
{
    public function index(Request $request)
    {
        $param = $request->param();
        if($request->isAjax()){
            $map = [];
            $list = model('Intention')->with('user,province,city,device')->where($map)->page($param['page'],$param['limit'])->select();
            $count = model('Intention')->where($map)->count();
            return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
        }else{
            return view();
        }
    }
}
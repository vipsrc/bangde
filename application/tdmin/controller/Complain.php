<?php
namespace app\tdmin\controller;

use think\Request;

class Complain extends Common
{
	/**
	 * [index 投诉列表]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [type]     [description]
	 */
	public function index()
	{
		return view();
	}

    /**
     * [getComplain 投诉数据]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [type]     [description]
     */
    public function getComplain(Request $request)
    {
        $param = $request->param();
        $list  = Model('Complain')->page($param['page'], $param['limit'])->select()->each(function ($item, $key) {
            $item['nickname'] = db('member')->where(['id' => $item['uid']])->value('nickname');
            return $item;
        });
        $count = Model('Complain')->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [delComplain 删除投诉记录]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [type]     [description]
     */
    public function delComplain()
    {
        $param = input();
        $re    = db('Complain')->where(['id' => $param['id']])->delete();
        if (false != $re) {
            return json(['code' => 0, 'msg' => '请求成功', 'desc' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败', 'desc' => '删除失败']);
        }
    }

	/**
	 * [checkComplain 查看投诉建议]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [view]     [description]
	 */
	public function checkComplain()
	{
		$param = input();
		$info = model('Complain')->with('member')->where(['id'=>$param['id']])->find()->toArray();
		$this->assign('info',$info);
		return view();
	}
}
?>
<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/4/2
 * Time: 17:06
 */
namespace app\tdmin\controller;

use think\Db;
use think\Request;
use app\Common\model\RepairOrder as Order;

class RepairOrder extends Common
{
    public function index(Request $request)
    {
        if($request->isAjax()){
            $param = $request->param();
            $map = [];
            if (isset($param['order_no'])) {
                $map[] = ['order_no', 'like', '%' . $param['order_no'] . '%'];
            }
            if (isset($param['status']) and $param['status']!='') {
                $map[] = ['status', 'eq', $param['status']];
            }

            if (input('?date') and input('date')) {
                $map[] = ['create_time', 'between time', explode('#', input('date'))];
            }
            $order = model('RepairOrder');
            $list = $order->where($map)->page($param['page'],$param['limit'])->select()->toArray();
            foreach($list as $k=>$v){
                $list[$k]['repair_type_text'] = Db::name('RepairType')->where(['id'=>$v['repair_type']])->value('name');
            }
            $count = $order->where($map)->count();
            return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
        }else{
            return view();
        }
    }

    public function info()
    {
        $param = input();
        $map = [];
        $map[] = ['id', 'eq', $param['oid']];
        $info = model('RepairOrder')->with('user,builder')->where($map)->find()->toArray();
        $info['imgs'] && $info['imgs'] = explode(',',$info['imgs']);
        $info['repair_type_text'] = Db::name('RepairType')->where(['id'=>$info['repair_type']])->value('name');
        $this->assign('order', $info);
        return view();
    }


    public function changeOrderStatus()
    {
        $param = input();
        $Order = Model('RepairOrder');
        if (input('?id')) {
            $order = $Order->where(array('id' => $param['id']))->find();
            if (!$order) {
                return json(['code' => 44001, 'msg' => '参数错误']);
            }
            if ($param['status'] > 1 and empty($param['amount'])) {
                return json(['code' => 44001, 'msg' => '请填写订单金额']);
            }

            if ($Order->isUpdate(true)->allowField(true)->save($param)) {
//                if ($param['status'] == 3) {
//                    $user = model('AuthUser')->where(['id' => $order['uid']])->find();
//                    if ($user['pid'] and $order['give_commision'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['pid']])->setInc('commision', $order['give_commision']);
//                        Db::name('CommissionLog')->insert(['uid' => $user['pid'], 'type' => 1, 'amount' => $order['give_commision'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_commision'] . '元', 'create_time' => time()]);
//                    }
//                    if ($order['give_integral'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['id']])->setInc('integration', $order['give_integral']);
//                        Db::name('IntegrationLog')->insert(['uid' => $order['uid'], 'type' => 1, 'amount' => $order['give_integral'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_integral'] . '积分', 'create_time' => time()]);
//                    }
//                    $ordergodos = db('OrderGoods')->where(['order_no' => $order['order_no']])->select();
//                    foreach ($ordergodos as $k => $v) {
//                        Db::name('Goods')->where(['id' => $v['gid']])->setInc('sales', $v['num']);
//                    }
//                }
                return json(['code' => 0, 'msg' => '处理成功']);
            } else {
                return json(['code' => 40001, 'msg' => '处理失败']);
            }
        }
    }

    public function builderlist(Request $request)
    {
        if($request->isAjax()){
            $list = model('Member')->where([['role','in',[2,3]],['frozen','eq','0']])->page(input('page'),input('limit'))->select();
            $count = model('Member')->where([['role','in',[2,3]],['frozen','eq','0']])->count();
            return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
        }else{
            return view();
        }
    }

    public function appoint(Request $request)
    {
        $param = $request->param();
        $order = model('RepairOrder')->where(['id'=>$param['oid']])->find();
        if(!$order){
            return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if(!empty($order['builder'])){
            return json(['code'=>44004,'msg'=>'订单已分配施工方']);
        }
        $re = model('RepairOrder')->where(['id'=>$order['id']])->update(['builder'=>$param['uid'],'status'=>2]);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'指派成功']);
        }else{
            return json(['code'=>44004,'msg'=>'系统错误']);
        }
    }
}
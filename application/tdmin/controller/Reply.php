<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 22/3/19
 * Time: 上午10:47
 */

namespace app\tdmin\controller;


use think\Db;

class Reply extends Common
{
    public function index(){

        return view();
    }

    /**
     * 评论列表
     */
    public function getReplyList()
    {
        $param = input();
        $map = [];
        //$list = model('Reply')->where($map)->page($param['page'],$param['limit'])->select();
        $list =
            Db::table('td_forum_reply')->alias('r')
            ->join('td_auth_user u','r.uid = u.id')->field('r.*,u.nickname')
            ->join('td_forum f','r.forum_id = f.id')->field('f.title')
            ->where($map)->page($param['page'],$param['limit'])->order('r.create_time desc')->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time'] = Date('Y-m-d H:i:s',$v['create_time']);
        }
        $count = model('Reply')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
    }

    /**
     * 查看详情
     */
    public function showReply($id){
        $info = \app\common\model\Reply::where('id',$id)->find();
        return view('',compact('info'));
    }

    /**
     * 删除帖子
     */

    public function delReply(){
        $param = input();
        $re = model('Reply')->where(['id'=>$param['id']])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>44003,'msg'=>'删除失败']);
        }
    }
}
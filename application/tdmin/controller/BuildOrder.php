<?php
namespace app\tdmin\controller;

use think\Db;
use think\Request;
use app\common\controller\Common as Tool;

class BuildOrder extends Common
{
    public function index()
    {
        return view();
    }

    /**
     * [orderList 订单列表]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function orderList()
    {
        $param = input();
        $map = [];
        if (isset($param['uid'])) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        if (isset($param['order_no'])) {
            $map[] = ['order_no', 'like', '%' . $param['order_no'] . '%'];
        }
        if (isset($param['status']) and $param['status']!='') {
            $map[] = ['status', 'eq', $param['status']];
        }
        if (input('?date') and input('date')) {
            $map[] = ['create_time', 'between time', explode('#', input('date'))];
        }
        $map[] = ['status', 'neq', '-1'];
        $list = model('BuildOrder')->with('publisherUser')->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->toArray();

        $count = model('BuildOrder')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $list, 'count' => $count]);
    }

    public function info()
    {
        $param = input();
        $map = [];
        $map[] = ['id', 'eq', $param['oid']];
        $info = model('BuildOrder')->with('publisher_user,user,builder')->where($map)->find()->toArray();
        $info['imgs'] = explode(',',$info['imgs']);

        $this->assign('order', $info);
        return view();
    }

    /**
     * [getOrderInfo 订单详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getOrderInfo()
    {
        $param = input();
        $map = [];
        $map[] = ['id', 'eq', $param['id']];
        $info = model('Order')->with('OrderGoods,OrderGoods.Goods')->where($map)->find()->toArray();
        if ($info['status'] == '0') {
            $info['pay_limit'] = strtotime($info['create_time']) + (config('site.pay_limit') * 3600);
        }
        if ($info['status'] == '2') {
            $info['recieve_limit'] = $info['pay_time'] + (config('site.recieve_limit') * 86400);
        }
        foreach ($info['order_goods'] as $k => $v) {
            $info['order_goods'][$k]['goods']['img'] = config('site.site_url') . $v['goods']['img'];
            $comment = Db::name('comment')->where(['order_no' => $info['order_no'], 'gid' => $v['gid']])->find();
            if ($comment) {
                $info['ordergodos'][$k]['comment'] = $comment;
            }
        }
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $info]);
    }

    /**
     * [changeOrderStatus 修改订单状态]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function changeOrderStatus()
    {
        $param = input();
        $Order = Model('BuildOrder');
        if (input('?id')) {
            $order = $Order->where(array('id' => $param['id']))->find();
            if (!$order) {
                return json(['code' => 44001, 'msg' => '参数错误']);
            }
            if ($param['status'] > 1 and empty($param['amount'])) {
                return json(['code' => 44001, 'msg' => '请填写订单金额']);
            }

            if ($Order->isUpdate(true)->allowField(true)->save($param)) {
//                if ($param['status'] == 3) {
//                    $user = model('AuthUser')->where(['id' => $order['uid']])->find();
//                    if ($user['pid'] and $order['give_commision'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['pid']])->setInc('commision', $order['give_commision']);
//                        Db::name('CommissionLog')->insert(['uid' => $user['pid'], 'type' => 1, 'amount' => $order['give_commision'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_commision'] . '元', 'create_time' => time()]);
//                    }
//                    if ($order['give_integral'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['id']])->setInc('integration', $order['give_integral']);
//                        Db::name('IntegrationLog')->insert(['uid' => $order['uid'], 'type' => 1, 'amount' => $order['give_integral'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_integral'] . '积分', 'create_time' => time()]);
//                    }
//                    $ordergodos = db('OrderGoods')->where(['order_no' => $order['order_no']])->select();
//                    foreach ($ordergodos as $k => $v) {
//                        Db::name('Goods')->where(['id' => $v['gid']])->setInc('sales', $v['num']);
//                    }
//                }
                return json(['code' => 0, 'msg' => '处理成功']);
            } else {
                return json(['code' => 40001, 'msg' => '处理失败']);
            }
        }
    }

    /**
     * [delOrder 删除订单]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function delOrder()
    {
        $param = input();
        $re = db('BuildOrder')->where(['id' => $param['id']])->setField('is_delete', 1);
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    public function builderlist(Request $request)
    {
        if($request->isAjax()){
            $list = model('Member')->where([['role','in',[2,3]],['frozen','eq','0']])->page(input('page'),input('limit'))->select();
            $count = model('Member')->where([['role','in',[2,3]],['frozen','eq','0']])->count();
            return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
        }else{
            return view();
        }
    }

    public function appoint(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['id'=>$param['oid']])->find();
        if(!$order){
           return json(['code'=>44004,'msg'=>'参数错误']);
        }
        if(!empty($order['builder'])){
            return json(['code'=>44004,'msg'=>'订单已分配施工方']);
        }
        $builder = model('Member')->where(['id'=>$param['uid']])->find();
        $re = model('BuildOrder')->where(['id'=>$order['id']])->update(['builder'=>$param['uid'],'status'=>2]);
        if(false !== $re){
            Tool::sendMessage($order['uid'],'eVKu2qNIsaNk6VnTYW1ewsixJrqhroiLu4bg0zwiObI',['first'=>'施工方已接单，请及时预约勘察','keyword1'=>'安装订单','keyword2'=>$order['order_no'],'keyword3'=>'已接单','keyword4'=>date('Y-m-d H:i'),'keyword5'=>$builder['realname'],'remark'=>'']);
            Tool::sendMessage($param['uid'],'eKohTaoji3qGEXTk1G73Ewt8NPQZAfAvsp5SEecW_J4',['first'=>'平台已分配订单，请及时预约勘察','keyword1'=>$order['order_no'],'keyword2'=>$order['create_time'],'remark'=>'如有疑问，请联系客服']);
            return json(['code'=>0,'msg'=>'指派成功']);
        }else{
            return json(['code'=>44004,'msg'=>'系统错误']);
        }
    }

    public function progress(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['id'=>$param['oid']])->find();
        if(!$order){
            return json(['code'=>40001,'msg'=>'未找到订单']);
        }
        $data = [];
        $list = model('Progress')->where(['oid'=>$order['id']])->select()->toArray();
        foreach($list as $k=>$v){
            $v['img'] = $v['img'] ? explode(',',$v['img']) : [];
            $v['video'] =  $v['video'] ? explode(',',$v['video']) : [];
            if(isset($data[$v['status']])){
                $data[$v['status']]['content'][] = $v;
            }else{
                $data[$v['status']]['title'] = config('base.progress_status')[$v['status']];
                $data[$v['status']]['content'][] = $v;
            }
        }

        $this->assign('data',$data);
        return view();
    }

    public function setProgressShow(Request $request)
    {
        $param = $request->param();
        $order = model('BuildOrder')->where(['id'=>$param['id']])->find();
        if(!$order){
           return json(['code'=>44004,'msg'=>'参数错误']);
        }
        $re1 = Db::name('BuildOrder')->where([['progress_show','neq','0']])->setField('progress_show',0);
        $re2 = Db::name('BuildOrder')->where(['id'=>$param['id']])->setField('progress_show',$param['progress_show']);
        if(false !== $re1 and false !== $re2){
            return json(['code'=>0,'msg'=>'设置成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }
}

<?php
/**
 * @project : tdbase
 * @auther  : 秋水
 * @date    : 2018/9/21
 * @desc    : 优惠券控制器
 */
namespace app\tdmin\controller;

use think\Db;

class Coupon extends Common
{

    /**
     * 优惠券列表
     * @author 雨夜
     * @DateTime 2019年3月27日16:21:55
     */
    public function couponList()
    {
        $page = input('param.page', 1);
        $limit = input('param.limit', 10);
        $model = model('Coupon');
        $list = $model->order('create_time desc')->page($page, $limit)->select();
        $count = $model->count();
        $this->assign('page', $page);
        $this->assign('limit', $limit);
        $this->assign('count', $count);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 添加优惠券
     * @author 雨夜
     * @DateTime 2019年3月27日16:22:08
     * @Update 雨夜 2018年10月19日14:30:34
     */
    public function addCoupon()
    {
        if (request()->isPost()) {
            $param = input('post.');
            $rule = array(
                'name' => 'require',
                'start_time' => 'require|date',
                'end_time' => 'require|date',
                'use_time' => 'require|number'
            );
            $validate = $this->validate($param, $rule);
            true !== $validate && $this->error($validate);
            $param['over'] = isset($param['over']) ? '1' : '0';
            $r = model('Coupon')->save($param);
            if ($r) {
                return $this->success('添加成功', url('tdmin/coupon/couponList'));
            } else {
                return $this->error('添加失败');
            }
        } else {
            return $this->fetch();
        }
    }

    /**
     * 修改优惠券
     * @author 秋水
     * @DateTime 2018-09-21T15:50:45+0800
     * @Update 雨夜 2018年10月19日14:30:46
     */
    public function editCoupon()
    {
        if (request()->isPost()) {
            $param = input('post.');
            $rule = array(
                'name' => 'require',
                'start_time' => 'require|date',
                'end_time' => 'require|date',
                'use_time' => 'require|number'
            );
            $validate = $this->validate($param, $rule);
            true !== $validate && $this->error($validate);
            $param['over'] = isset($param['over']) ? '1' : '0';
            $r = model('Coupon')->update($param);
            if ($r) {
                return $this->success('修改成功', url('tdmin/coupon/couponList'));
            } else {
                return $this->error('修改失败');
            }
        } else {
            $id = input('param.id', 0);
            $data = model('Coupon')->where([['id', 'eq', $id]])->find();
            $this->assign('data', $data);
            return $this->fetch();
        }
    }


    public function couponLog()
    {
        model('UserCoupon')->where([['end_time', 'lt', time()]])->setField('status', '-1');
        if (request()->isAjax()) {
            $param = input();
            $map = [];
            if(isset($param['nickname']) and $param['nickname']!==''){
                $uids = db('Member')->where([['nickname|telephone','like','%'.$param['nickname'].'%']])->column('id');
                $map[] = ['member_id','in',$uids];
            }
            if(isset($param['status']) and $param['status']!==''){
                $map[] = ['status','eq',$param['status']];
            }
            $list = model('UserCoupon')->where($map)->page($param['page'], $param['limit'])->order('coupon_value desc,status desc')->select()->each(function ($item, $key) {
//                $item['cate_text'] = $item['cate'] == '0' ? '全部' : model('Cate')->where(['id' => $item['cate']])->value('name');
                $item['end_time'] = date('Y-m-d H:i:s', $item['end_time']);
                $item['member_name'] = Db::name('Member')->where(['id' => $item['member_id']])->value('nickname');

                return $item;
            });
            $count = model('UserCoupon')->where($map)->count();
            return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
        } else {
            return view();
        }
    }

    public function importMemberCoupon()
    {
        $file = request()->file('file');
        if (true !== $this->validate(['file' => $file], ['file' => 'require|file'])) {
            return json(['code' => 500, 'msg' => '请选择文件']);
        } else {
            $info = $file->validate(['size' => 1024 * 5000])->move('uploads/data');
            if ($info) {
                // 成功上传后 获取上传信息
                $savename = $info->getSaveName();
            } else {
                // 上传失败获取错误信息
                $error = $file->getError();
                return json(['code' => 500, 'msg' => $error]);
            }
        }
        $objPHPExcel = new \PHPExcel();
        $inputFileName = 'uploads/data/' . $savename;
        if (!file_exists($inputFileName)) {
            return json(['code' => 500, 'msg' => '未找到文件']);
        }
        $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $Member = model('Member');
        $UserCoupon = model('UserCoupon');
        // 获取一行的数据
        $i = 0;
        for ($row = 3; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            if (!empty($rowData[0][0]) and !empty($rowData[0][1]) and !empty($rowData[0][2])) {
                $price = $Price->where(['id' => $rowData[0][0]])->find();
                if (!$price) {
                    continue;
                }
                $pricedaydata = $Priceday::where(['date' => strtotime($rowData[0][1]), 'priceid' => $rowData[0][0]])->find();
                if ($pricedaydata) {
                    $re = $Priceday->save(['date' => strtotime($rowData[0][1]), 'price' => $rowData[0][2], 'time' => time()], ['id' => $pricedaydata->id]);
                } else {
                    $re = $Priceday->save(['priceid' => $rowData[0][0], 'date' => strtotime($rowData[0][1]), 'price' => $rowData[0][2], 'time' => time()]);
                }
                if (false !== $re) {
                    $i++;
                }
            }
        }
        return json(['code' => 0, 'msg' => '成功导入' . $i . '条数据，共' . ($highestRow - 2) . '条']);
    }
}
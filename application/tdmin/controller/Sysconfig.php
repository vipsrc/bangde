<?php
namespace app\tdmin\controller;

use think\Facade\Config;

class Sysconfig extends Common
{

	/**
	 * [index 站点配置]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [type]     [description]
	 */
	public function index(){
        $setfile='../config/site.php';
        if(!file_exists($setfile)){
        	fopen($setfile,"a");
		}
        $site = require $setfile;
        $this->assign('site',$site);
        return $this->fetch();
    }

    /**
     * [saveConfig 站点配置]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [view]     [description]
     */
    public function saveConfig()
    {
        $post = input();
        // if(isset($post['third_login'])){
        //     $post['third_login'] = 1;
        // }else{
        //     $post['third_login'] = 0;
        // }
        $setfile='../config/site.php';
        $set=var_export($post, TRUE);
        $settingstr="<?php \n return ";
        $settingstr.=$set;
        $settingstr.=";\n?>\n";
        if(file_put_contents($setfile,$settingstr)>0){
            return json(['code'=>0,'msg'=>'请求成功','desc'=>'保存成功']);
        }else{
            return json(['code'=>44003,'msg'=>'修改失败','desc'=>'保存失败']);
        }
    }

    public function backsql()
    {
        return view();
    }

    public function editsql()
    {

        //获取操作内容：（备份/下载/还原/删除）数据库
        $type = input("type",'list');
        //获取需要操作的数据库名字
        $name = input('name');
        $backup = new \org\Baksql(Config::get("database."));
        switch ($type) {
            //备份
            case "backup":
                $info = $backup->backup();
                return json(['code'=>0,'msg'=>$info]);
                break;
            //下载
            case "dowonload":
                $info = $backup->downloadFile($name);
                return json(['code'=>0,'msg'=>$info]);
                break;
            //还原
            case "restore":
                $info = $backup->restore($name);
                return json(['code'=>0,'msg'=>$info]);
                break;
            //删除
            case "del":
                $info = $backup->delfilename($name);
                return json(['code'=>0,'msg'=>$info]);
                break;
            //如果没有操作，则查询已备份的所有数据库信息
            default:
                $list = array_reverse($backup->get_filelist());
                return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
        }

    }
}

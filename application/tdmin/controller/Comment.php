<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/8/16
 * Time: 15:01
 */
namespace app\tdmin\controller;

use think\Request;

class Comment extends Common
{
    public function index()
    {
        return view();
    }

    public function getOrderComment(Request $request)
    {
        $param = $request->param();
        $map = [];
        $list = model('Comment')->with('user,goods')->where($map)->page($param['page'],$param['limit'])->order('crate_time desc')->select();
        foreach($list as $k=>$v){
            $list[$k]['goods_name'] = $v['goods']['name'];
        }
        $count = model('Comment')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
    }

    public function editComment(Request $request)
    {
        $param = $request->param();
        $re = model('Comment')->isUpdate(true)->allowField(true)->save($param);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'编辑成功']);
        }else{
            return json(['code'=>40001,'msg'=>"编辑失败"]);
        }
    }

    public function bcomment()
    {
        return view();
    }

    public function getBorderComment(Request $request)
    {
        $param = $request->param();
        $map = [];
        $map[] = ['type','eq',0];
        $list = model('BuildComment')->with('user,buildUser')->where($map)->page($param['page'],$param['limit'])->order('create_time desc')->select();

        $count = model('BuildComment')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
    }


    public function editBcomment(Request $request)
    {
        $param = $request->param();
        $re = model('BuildComment')->isUpdate(true)->allowField(true)->save($param);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'编辑成功']);
        }else{
            return json(['code'=>40001,'msg'=>"编辑失败"]);
        }
    }

    public function rcomment()
    {
        return view();
    }

    public function getRorderComment(Request $request)
    {
        $param = $request->param();
        $map = [];
        $map[] = ['type','eq',1];
        $list = model('BuildComment')->with('user,buildUser')->where($map)->page($param['page'],$param['limit'])->order('craete_time desc')->select();
        $count = model('BuildComment')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list,'count'=>$count]);
    }

    public function editRcomment(Request $request)
    {
        $param = $request->param();
        $re = model('BuildComment')->isUpdate(true)->allowField(true)->save($param);
        if(false !== $re){
            return json(['code'=>0,'msg'=>'编辑成功']);
        }else{
            return json(['code'=>40001,'msg'=>"编辑失败"]);
        }
    }
}
<?php
namespace app\tdmin\controller;

use think\Db;
use think\Request;

class Order extends Common
{
    public function index()
    {
        return view();
    }

    /**
     * [orderList 订单列表]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function orderList()
    {
        $param = input();
        $map = [];
        if (isset($param['uid']) and $param['uid']) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        if (input('?user') and input('user')) {
            $map[] = ['name|tel', 'like', '%'.input('user').'%'];
        }
        if (isset($param['order_no']) and $param['order_no']!=='') {
            $map[] = ['order_no', 'like', '%' . $param['order_no'] . '%'];
        }
        if (isset($param['status']) and $param['status']!=='') {

            $map[] = ['status', 'eq', $param['status']];
        }
        if (isset($param['pay_state']) and $param['pay_state']!=='') {
            $map[] = ['pay_state', 'eq', $param['pay_state']];
        }

        if (input('?date') and input('date')) {
            $map[] = ['create_time', 'between time', explode('#', input('date'))];
        }
        $map[] = ['is_delete', 'eq', '0'];

        $list = model('Order')->with(['OrderGoods', 'OrderGoods.Goods'])->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->toArray();
        foreach ($list as $k => $v) {
            foreach ($v['order_goods'] as $kk => $vv) {
                $list[$k]['order_goods'][$kk]['goods']['img'] = config('site.site_url') . $vv['goods']['img'];
            }
        }
        $count = model('Order')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $list, 'count' => $count]);
    }

    public function info()
    {
        $param = input();
        $map = [];
        $map[] = ['id', 'eq', $param['oid']];
        $info = model('Order')->with('OrderGoods,OrderGoods.Goods')->where($map)->find()->toArray();
        $this->assign('order', $info);
        return view();
    }

    /**
     * [getOrderInfo 订单详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getOrderInfo()
    {
        $param = input();
        $map = [];
        $map[] = ['id', 'eq', $param['id']];
        $info = model('Order')->with('OrderGoods,OrderGoods.Goods')->where($map)->find()->toArray();
        if ($info['status'] == '0') {
            $info['pay_limit'] = strtotime($info['create_time']) + (config('site.pay_limit') * 3600);
        }
        if ($info['status'] == '2') {
            $info['recieve_limit'] = $info['pay_time'] + (config('site.recieve_limit') * 86400);
        }
        foreach ($info['order_goods'] as $k => $v) {
            $info['order_goods'][$k]['goods']['img'] = config('site.site_url') . $v['goods']['img'];
            $comment = Db::name('comment')->where(['order_no' => $info['order_no'], 'gid' => $v['gid']])->find();
            if ($comment) {
                $info['ordergodos'][$k]['comment'] = $comment;
            }
        }
        return json(['code' => 0, 'msg' => '请求成功', 'desc' => '', 'data' => $info]);
    }

    /**
     * [changeOrderStatus 修改订单状态]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function changeOrderStatus()
    {
        $param = input();
        $Order = Model('Order');
        if (input('?id')) {
            $order = $Order->where(array('id' => $param['id']))->find();
            if (!$order) {
                return json(['code' => 44001, 'msg' => '参数错误']);
            }
            if ($param['status'] == 2 and empty($param['delivery_num'])) {
                return json(['code' => 44001, 'msg' => '缺少快递号']);
            }
            if ($param['status'] == '0') {
                $param['pay_state'] = 0;
            }
            if ($param['status'] >= '1' and $order['status']=='0') {
                $param['pay_state'] = 1;
                $param['pay_method'] = 3;
                $param['pay_name'] = '后台支付';
            }
            if ($Order->isUpdate(true)->allowField(true)->save($param)) {
//                if ($param['status'] == 3) {
//                    $user = model('Member')->where(['id' => $order['uid']])->find();
//                    if ($user['pid'] and $order['give_commision'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['pid']])->setInc('commision', $order['give_commision']);
//                        Db::name('CommissionLog')->insert(['uid' => $user['pid'], 'type' => 1, 'amount' => $order['give_commision'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_commision'] . '元', 'create_time' => time()]);
//                    }
//                    if ($order['give_integral'] > 0) {
//                        Db::name('AuthUser')->where(['id' => $user['id']])->setInc('integration', $order['give_integral']);
//                        Db::name('IntegrationLog')->insert(['uid' => $order['uid'], 'type' => 1, 'amount' => $order['give_integral'], 'remark' => '订单' . $order['order_no'] . '返利' . $order['give_integral'] . '积分', 'create_time' => time()]);
//                    }
//                    $ordergodos = db('OrderGoods')->where(['order_no' => $order['order_no']])->select();
//                    foreach ($ordergodos as $k => $v) {
//                        Db::name('Goods')->where(['id' => $v['gid']])->setInc('sales', $v['num']);
//                    }
//                }
                return json(['code' => 0, 'msg' => '处理成功']);
            } else {
                return json(['code' => 40001, 'msg' => '处理失败']);
            }
        }
    }

    public function changePrice(Request $request)
    {
        $param = $request->param();
        $order = model('Order')->where(array('id' => $param['id']))->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        $re = model('Order')->isUpdate(true)->allowField(true)->save($param);
        if (false !== $re) {
            return json(['code'=>0,'msg'=>'修改成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function delivery(Request $request)
    {
        $param = $request->param();
        $order = model('Order')->where(array('id' => $param['id']))->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        if(empty($param['delivery_num'])){
            return json(['code'=>44004,'msg'=>'请填写单号！']);
        }
        $param['status'] = 2;
        $re = model('Order')->isUpdate(true)->allowField(true)->save($param);
        if (false !== $re) {
            return json(['code'=>0,'msg'=>'发货成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function refund(Request $request)
    {
        $param = $request->param();
        $order = model('Order')->where(array('id' => $param['id']))->find();
        if (!$order) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }

        $re = model('Order')->isUpdate(true)->allowField(true)->save($param);
        if (false !== $re) {
            return json(['code'=>0,'msg'=>'处理成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [delOrder 删除订单]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function delOrder()
    {
        $param = input();
        $re = db('Order')->where(['id' => $param['id']])->setField('is_delete', 1);
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    public function progress(Request $request)
    {
        $param = $request->param();
        if($request->isAjax()){

        }else{
            return view();
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: yong
 * Date: 20/3/19
 * Time: 下午3:50
 */

namespace app\tdmin\controller;


use think\Db;

class Forum extends Common
{
    public function index(){

        return view();
    }

    /**
     * 帖子列表
     */
    public function getForumList()
    {
        $param = input();
        $map = [];

        $list = model('forum')->field('f.*,m.nickname')->alias('f')->join('member m','f.uid=m.id')->where($map)->page($param['page'],$param['limit'])->order('f.create_time desc')->select();
        foreach($list as $k=>$v){
            $list[$k]['cate_text'] = Db::name('ForumCate')->where(['id'=>$v['cid']])->value('name');
        }
        $count = model('Forum')->where($map)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
    }

    public function addForum()
    {
        return view();
    }


    /**
     * 渲染编辑页面
     */
    public function editForum()
    {
        $param = input();

        $info = model('Forum')->get($param['id']);
        $info['imgs_arr'] = explode(',',$info['imgs']);

        if(!$info){
            $this->error('参数错误');
        }
        $this->assign('info',$info);
        $this->assign('imgs_arr',$info['imgs_arr']);
        return view();
    }
    /**
     * 帖子编辑保存
     */
    public function saveForum()
    {
        $param = input();

        $rule = [
            'title'=>'require',
            'content|内容'=>'require',
            'uid'=>'require|float',

        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
        if(isset($param['id'])){
            $re = model('Forum')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('Forum')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>44005,'msg'=>'保存失败']);
        }
    }

    /**
     * 帖子删除
     */
    public function delForum()
    {
        $param = input();
        $re = model('Forum')->where(['id'=>$param['id']])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>44003,'msg'=>'删除失败']);
        }
    }




}

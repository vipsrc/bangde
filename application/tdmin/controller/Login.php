<?php
/**
 * Created by PhpStorm.
 * User: 江远
 * Date: 2018/11/30
 * Time: 18:46
 */
namespace app\tdmin\controller;

use think\Controller;
use think\Db;
use think\captcha\Captcha;

class Login extends Controller
{
    public function initialize(){

        
    }

    public function index(){
        if (session('uid')) {
            $this->redirect('tdmin/index/index');
        }
        return $this->fetch();
    }

    /****
     * 用户注销,退出登陆
     */
    public function logout(){
        session(null);
        $this->redirect('login/index');
    }

    /***
     * 登录功能
     */
    public function dologin()
    {
        $data   = input('post.');
        $return = model('AuthUser')->login($data, 'open');
        return json($return);
    }

    /***
     * 生成验证码
     */
    public function verify()
    {
        ob_clean();
        $config = [
            // 验证码字体大小
            'fontSize' => 25,
            // 验证码位数
            'length'   => 4,
            // 关闭验证码杂点
            'useNoise' => false,
            'bg'       => [255, 255, 255],
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }
}


<?php
namespace app\tdmin\controller;

use think\Db;

class Capital extends Common
{
	public function integration()
	{
		return view();
	}

    /**
     * [getIntegration 获取积分记录]
     * @Author   雨夜
     * @DateTime 2018-12-19
     * @return   [type]     [description]
     */
    public function getIntegration()
    {
        $param = input();
        $map   = [];
        if (isset($param['uid'])) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        $list = model('IntegrationLog')->with('user')->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select()->each(function ($item, $key) {
            $item['nickname'] = $item['user']['nickname'];
            return $item;
        });
        $count = model('IntegrationLog')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

	public function commission()
	{
		return view();
	}

    /**
     * [getCommission 获取佣金记录]
     * @Author   雨夜
     * @DateTime 2018-12-19
     * @return   [type]     [description]
     */
    public function getCommission()
    {
        $param = input();
        $map   = [];
        if (isset($param['uid'])) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        $list  = model('CommissionLog')->with('user')->where($map)->page($param['page'], $param['limit'])->order('create_time desc')->select();
        $count = model('CommissionLog')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

	public function withdraw()
	{
		return view();
	}

    /**
     * [getWithdraw 提现记录]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function getWithdraw()
    {
        $param = input();
        $map   = [];
        if (isset($param['uid'])) {
            $map[] = ['uid', 'eq', $param['uid']];
        }
        $list  = model('Withdraw')->with('user')->where($map)->page($param['page'], $param['limit'])->order('status asc , create_time desc')->select()->toArray();
        foreach($list as $k=>$v){
            $way = Db::name('WithdrawWay')->where(['uid'=>$v['uid']])->find();
            if($v['way']==1){
                $list[$k]['way_text'] = '支付宝：'.$way['ali_name'].';账号：'.$way['ali_account'];
            }else{
                $list[$k]['way_text'] = '银行卡：'.$way['credit_card'].';用户名：'.$way['name'];
            }
        }
        $count = model('Withdraw')->where($map)->count();
        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
    }

    /**
     * [checkWithdraw 审核提现]
     * @Author   雨夜
     * @DateTime 2018-12-20
     * @return   [type]     [description]
     */
    public function checkWithdraw()
    {
        $param    = input();
        $withdraw = model('Withdraw')->get($param['id']);
        if (!$withdraw) {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        if ($withdraw['status'] !== 0) {
            return json(['code' => 44001, 'msg' => '申请状态有误']);
        }
        Db::startTrans();
        $re1 = Db::name('Withdraw')->where(['id' => $withdraw['id']])->setField('status',$param['type']);
        if ($param['type'] == '2') {
            $re2 = Db::name('Member')->where(['id' => $withdraw['uid']])->setInc('commision', $withdraw['amount']);
            $re3 = Db::name('CommissionLog')->insert(['uid' => $withdraw['uid'],'amount'=>$withdraw['amount'] , 'type' => 3, 'remark' => '提现失败返还' . $withdraw['amount'] . '元', 'create_time' => time()]);
        }else{
            $re2= true;
            $re3 = true;
        }
        if(false!==$re1 and false!==$re2 and false!==$re3){
            Db::commit();
            return json(['code'=>0,'msg'=>'审核成功']);
        }else{
            Db::rollback();
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /**
     * [getWithDrawWay 提现方式]
     * @Author   雨夜
     * @DateTime 2019-01-10
     * @return   [type]     [description]
     */
    public function getWithDrawWay()
    {
        $param = input();
        $info = model('WithdrawWay')->where(['uid'=>$this->uid])->find();
        if($info && $info['wx_qrcode']){
            $info['wx_qrcode'] = config('site.site_url').$info['wx_qrcode'];
        }
        if($info){
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }else{
            return json(['code'=>0,'msg'=>'请求成功','data'=>[]]);
        }
    }
}
?>
<?php
/**
 * 秋水
 * 2018/11/28
 * 默认执行控制类
 */
namespace app\tdmin\controller;
use think\Db;
class Index extends Common
{
    public function initialize(){
        
        parent::initialize();
    }

	/**
	 * 首页渲染
	 * @author 秋水
	 * @DateTime 2018-11-29T14:40:43+0800
	 */
    public function index()
    {
    	return $this->fetch();
    }

    /**
     * 首页控制台
     * @author 秋水
     * @DateTime 2018-11-29T14:41:02+0800
     */
    public function controlBoard()
    {
        $count = [];
        $uids = db('auth_group_access')->where(['group_id'=>2])->column('uid');
        $map[] = ['id','in',$uids];
        $count['all_user'] = Db::name('AuthUser')->where($map)->count();
        $count['all_order'] = Db::name('Order')->where(['pay_state'=>1])->count();
        $count['all_pay'] = Db::name('Order')->where(['pay_state'=>1])->sum('pay_cash');
        $this->assign('count',$count);
        $version = Db::query('SELECT VERSION() AS ver');
        $config  = [
            'url'             => $_SERVER['HTTP_HOST'],
            'document_root'   => $_SERVER['DOCUMENT_ROOT'],
            'server_os'       => PHP_OS,
            'server_port'     => $_SERVER['SERVER_PORT'],
            'server_ip'       => $_SERVER['SERVER_ADDR'],
            'server_soft'     => $_SERVER['SERVER_SOFTWARE'],
            'php_version'     => PHP_VERSION,
            'mysql_version'   => $version[0]['ver'],
            'max_upload_size' => ini_get('upload_max_filesize')
        ];
        $this->assign('config', $config);
    	return $this->fetch();
    }

    public function delCache()
    {
        $re = deldir();
        if($re){
            $this->success('清理成功',url('tdmin/Index/controlBoard'));
        }else{
            $this->error('清除失败');
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2018/12/5
 * Time: 14:56
 */
namespace app\tdmin\controller;

use think\Request;
use think\Db;

class Goods extends Common
{
    /**
     * [categoryList 分类列表]
     * @Author   雨夜
     * @DateTime 2018-12-12
     * @return   [view]     [页面渲染]
     */
    public function categoryList(Request $request)
    {
        if($request->isAjax()){
            $where           = [];
            $categoryList    = db('Cate')->where($where)->order('sort desc')->select();
            $tree            = list_to_tree($categoryList, 'id', 'parentid');
            $arr             = tree_to_list($tree, '0', 'id', 'parentid');
            $result['data']  = $arr;
            $result['count'] = count($categoryList);
            $result['code']  = 0;
            $result['msg']   = 'success';
            return json($result);
        }
        $parent = Db::name('Cate')->order('sort desc')->select();
        $parent = cate_merge($parent,null,0,'id','parentid');
        $this->assign('parent',$parent);
        return view();
    }

    /***
     * 获取添加分类的菜单列表
     * @author 江远
     */
    public function selectCategory()
    {
        //获取菜单列表
        $categoryList = \Db::name('Cate')->where(['parentid' => 0])->order('sort desc')->field('id,name as title')->select();
        foreach($categoryList as $k=>$v){
            $categoryList[$k]['child'] = model('Cate')->where(['parentid'=>$v['id']])->order('sort desc')->field('id, name as title')->select()->each(function($item,$key){
                $item['title'] = '&nbsp;&nbsp;&nbsp;&nbsp;'.$item['title'];
                return $item;
            })->toArray();
        }
        $data         = [
            'id'    => 0,
            'title' => '顶级菜单',
        ];
        array_unshift($categoryList, $data);
        $result['data'] = $categoryList;
        $result         = array_merge($this->api_code[0], $result);
        return json($result);
    }

    /**
     * [addCategory 添加分类]
     * @Author   雨夜
     * @DateTime 2018-12-11
     */
    public function addCategory()
    {
        $pid        = input('parentid', 0);
        $cat_name   = input('name', '#');
        $sort       = input('sort', 0);
        $insertData = [
            'parentid' => $pid,
            'name'     => $cat_name,
            'sort'     => $sort,
        ];
        $r = \Db::name('Cate')->insert($insertData);
        if ($r) {
            $this->api_code[0]['msg'] = '添加分类成功';
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '添加分类失败';
            return json($this->api_code[40001]);
        }
    }

    /**
     * 编辑分类api
     * @author 雨夜
     * @date 2018年12月7日09:19:07
     * @return \think\response\Json
     */
    public function editCategory()
    {
        $pid        = input('parentid', 0);
        $cat_name   = input('name', '#');
        $sort       = input('sort', 0);
        $insertData = [
            'parentid' => $pid,
            'name'     => $cat_name,
            'sort'     => $sort,
            'icon'     => input('icon')
        ];
        $r = \Db::name('Cate')->where(['id' => input('id')])->update($insertData);
        if ($r) {
            $this->api_code[0]['msg'] = '编辑分类成功';
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['desc'] = '编辑分类失败';
            return json($this->api_code[40001]);
        }
    }

    /**
     * 删除分类
     * @author 雨夜
     * @date 2018年12月7日09:18:01
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function delCategory()
    {
        $hasgoods = db('Goods')->where(['cid' => input('id')])->find();
        if ($hasgoods) {
            return json(['code' => 44004, 'msg' => '请先删除分类下商品']);
        }
        $re = db('Cate')->where(['id' => input('id')])->delete();
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    /**
     * [goodsType 商品类型]
     * @Author   雨夜
     * @DateTime 2018-12-12
     * @return   [type]     [页面渲染]
     */
    public function goodsType(Request $request)
    {

        if($request->isAjax()){
            $map   = [];
            $list  = model('GoodsType')->where($map)->page(input('page'), input('limit'))->select()->toArray();
            $count = count($list);
            return json(['code' => 0, 'count' => $count, 'data' => $list]);
        }
        return view();
    }

    /**
     * 添加类型
     * @author 雨夜
     * @date 2018年12月7日09:17:04
     * @return \think\response\Json
     */
    public function addType()
    {
        $rule = [
            'name|名称'      => 'require',
            'sort|排序'      => 'require',
            'enabled|是否启用' => 'require|in:0,1',
        ];
        $validate = $this->validate(input(), $rule);
        if (true !== $validate) {
            return json(['code' => 44002, 'msg' => $validate]);
        }
        $re = model('GoodsType')->isUpdate(false)->allowField(true)->save(input());
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '新增成功']);
        } else {
            return json(['code' => 44005, 'msg' => '新增失败']);
        }
    }

    /**
     * 编辑类型
     * @author 雨夜
     * @date 2018年12月7日09:16:41
     * @return \think\response\Json
     */
    public function editType()
    {
        $rule = [
            'name|名称'      => 'require',
            'sort|排序'      => 'require',
            'enabled|是否启用' => 'require|in:0,1',
        ];
        $validate = $this->validate(input(), $rule);
        if (true !== $validate) {
            return json(['code' => 44002, 'msg' => $validate]);
        }
        $re = model('GoodsType')->isUpdate(true)->allowField(true)->save(input());
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '编辑成功']);
        } else {
            return json(['code' => 44005, 'msg' => '编辑失败']);
        }
    }

    /**删除类型
     * @author 雨夜
     * @date 2018年12月7日09:16:11
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delType()
    {
        $hasgoods = db('Goods')->where(['goods_type' => input('id')])->find();
        if ($hasgoods) {
            return json(['code' => 44004, 'msg' => '请先删除该类型商品']);
        }
        $re = model('GoodsType')->where(['id' => input('id')])->delete();
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    /**
     * [attribution 属性列表]
     * @Author   雨夜
     * @DateTime 2018-12-11
     * @return   [view]
     */
    public function attribution()
    {
        if (!input('param.tid')) {
            $this->error('参数错误');
        }
        return view();
    }

    /**
     * 属性数据集
     * @author 雨夜
     * @date 2018年12月7日09:15:08
     * @return \think\response\Json
     */
    public function attribute()
    {
        $map = [];
        if (input('?tid')) {
            $map[] = ['type_id', 'eq', input('tid')];
        } else {
            return json(['code' => 44001, 'msg' => '参数错误']);
        }
        $list  = model('Attribute')->where($map)->page(input('page'), input('limit'))->select()->toArray();
        $count = count($list);
        return json(['code' => 0, 'count' => $count, 'data' => $list]);
    }

    /**
     * 添加属性
     * @author 雨夜
     * @date 2018年12月7日09:13:42
     * @return \think\response\Json
     */
    public function addAttribute()
    {
        $rule = [
            'attr_name|名称'         => 'require',
            'sort|排序'              => 'require',
            'attr_input_type|录入方式' => 'require|in:0,1',
            'attr_show_type|属性作用'  => 'require|in:0,1',
            'type_id|类型'           => 'require',
        ];
        $validate = $this->validate(input(), $rule);
        if (true !== $validate) {
            return json(['code' => 44002, 'msg' => $validate]);
        }
        $re = model('Attribute')->isUpdate(false)->allowField(true)->save(input());
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '新增成功']);
        } else {
            return json(['code' => 44005, 'msg' => '新增失败']);
        }
    }

    /**
     * 编辑属性
     * @author 雨夜
     * @date 2018年12月7日09:12:22
     * @return \think\response\Json
     */
    public function editAttribute()
    {
        $rule = [
            'attr_name|名称'         => 'require',
            'sort|排序'              => 'require',
            'attr_input_type|录入方式' => 'require|in:0,1',
            'attr_show_type|属性作用'  => 'require|in:0,1',
            'attr_id|属性ID'         => 'require',
        ];
        $validate = $this->validate(input(), $rule);
        if (true !== $validate) {
            return json(['code' => 44002, 'msg' => $validate]);
        }
        $data = [
            'attr_name'       => input('attr_name'),
            'attr_input_type' => input('attr_input_type'),
            'attr_values'     => input('attr_values'),
            'sort'            => input('sort'),
            'attr_show_type'  => input('attr_show_type'),
        ];
        $re = model('Attribute')->where(['attr_id' => input('attr_id')])->update($data);
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '编辑成功']);
        } else {
            return json(['code' => 44005, 'msg' => '编辑失败']);
        }
    }

    /**
     * 删除属性
     * @author 雨夜
     * @date 2018年12月7日09:11:41
     * @return \think\response\Json
     * @throws \Exception
     */
    public function delAttribute()
    {
        $re = model('Attribute')->where(['attr_id' => input('id')])->delete();
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    /**
     * [goodsList 商品列表]
     * @Author   雨夜
     * @DateTime 2018-12-11
     * @return   [view]
     */
    public function goodsList(Request $request)
    {
        if($request->isAjax()){
            $param = input();
            $map   = [];
            $map[] = ['is_del', 'eq', '0'];
            if(isset($param['type']) and $param['type'] = 'recom'){
                $map[] = ['is_recom','eq',1];
            }
            if(isset($param['type']) and $param['type'] = 'cart_recom'){
                $map[] = ['cart_recom','eq',1];
            }
            if(isset($param['cate']) and $param['cate']){
                $map[] = ['cid','eq',$param['cate']];
            }
            if(isset($param['keywords']) and $param['keywords']){
                $map[] = ['name','like','%'.$param['keywords'].'%'];
            }
            $list  = model('Goods')->where($map)->page(input('page'), input('limit'))->order('sort desc')->select()->toArray();
            $count = model('Goods')->where($map)->count();
            return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
        }
        return view();
    }

    /**
     * [addGoods 新增商品页面]
     * @Author   雨夜
     * @DateTime 2018-12-11
     */
    public function addGoods(Request $request)
    {
        if($request->isAjax()){
            $param = input();

            $rule  = [
                'name|名称'     => 'require',
                'img|图片'      => 'require',
                'tags|标签'     => 'require',
                'price|价格'    => 'require|float',
                'cid|分类'      => 'require|integer',
                'sales|销量'    => 'require|integer',
                'sort|排序'     => 'require|integer',
                'show_user|面向用户'     =>'require'
            ];
            $validate = $this->validate($param, $rule);
            if (true !== $validate) {
                return json(['code' => 44001, 'msg' => $validate]);
            }
            if(isset($param['param_name']) and isset($param['param_value'])){
                $parameter = [];
                foreach($param['param_name'] as $k=>$v){
                    if(empty($v) || empty($param['param_value'][$k])){
                        continue;
                    }
                    $parameter[$k]['name'] = $v;
                    $parameter[$k]['value'] = $param['param_value'][$k];
                }
                $param['parameter'] = json_encode($parameter);
            }

            $param['status'] = input('?status') ? 1 : 0;
            $param['is_recom'] = input('?is_recom') ? 1 :0;
            $param['show_user'] = implode(',',$param['show_user']);
            $param['is_promote'] = input('?is_promote') ? 1 : 0;
            if (input('?is_promote')) {
                if(empty($param['promote_price']) || empty($param['start_time']) || empty($param['end_time'])){
                    return json(['code' => 44004, 'msg' => '秒杀参数不完整']);
                }
                $param['start_time'] = strtotime($param['start_time']);
                $param['end_time'] = strtotime($param['end_time']);
                if($param['start_time']>=$param['end_time']){
                    return json(['code' => 44004, 'msg' => '开始时间不能大于结束时间']);
                }
            }else{
                $param['start_time'] = 0;
                $param['end_time'] = 0;
            }
            Db::startTrans();
            $Goods      = model('Goods');
            $Goods_attr = model('Goods_attr');
            $re         = $Goods->allowField(true)->save($param);
            if (!$re) {
                Db::rollback();
                return json(['code' => 44005, 'msg' => '新增失败，请重试！']);
            }
            $goods_id = $Goods->id;
            /* 处理属性 引用ecshop的属性操作方法*/
            if ((isset($_POST['attr_id_list']) && isset($_POST['attr_value_list'])) || (empty($_POST['attr_id_list']) && empty($_POST['attr_value_list']))) {

                // 取得原有的属性值
                $goods_attr_list = array();
                $attr_list       = $Goods_attr->where(array('goods_id' => $goods_id))->select();

                foreach ($attr_list as $key => $row) {
                    $goods_attr_list[$row['attr_id']][$row['attr_value']] = array('sign' => 'delete', 'goods_attr_id' => $row['goods_attr_id']);
                }
                // 循环现有的，根据原有的做相应处理
                if (isset($_POST['attr_id_list'])) {
                    foreach ($_POST['attr_id_list'] as $key => $attr_id) {
                        $attr_value                                     = $_POST['attr_value_list'][$key];
                        isset($_POST['attr_price_list']) && $attr_price = $_POST['attr_price_list'][$key];
                        // $attr_num   = $_POST['attr_num_list'][$key];
                        if (!empty($attr_value)) {
                            if (isset($goods_attr_list[$attr_id][$attr_value])) {
                                // 如果原来有，标记为更新
                                $goods_attr_list[$attr_id][$attr_value]['sign']                             = 'update';
                                isset($attr_price) && $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                                // $goods_attr_list[$attr_id][$attr_value]['attr_num']   = $attr_num;
                            } else {
                                // 如果原来没有，标记为新增
                                $goods_attr_list[$attr_id][$attr_value]['sign']                             = 'insert';
                                isset($attr_price) && $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                                // $goods_attr_list[$attr_id][$attr_value]['attr_num']   = $attr_num;
                            }
                        }
                    }
                }
                $re2 = true;
                /* 插入、更新、删除数据 */
                foreach ($goods_attr_list as $attr_id => $attr_value_list) {
                    foreach ($attr_value_list as $attr_value => $info) {
                        $date = array();
                        if ($info['sign'] == 'insert') {
                            $date['goods_id']                                 = $goods_id;
                            $date['attr_id']                                  = $attr_id;
                            $date['attr_value']                               = $attr_value;
                            isset($info['attr_price']) && $date['attr_price'] = $info['attr_price'];
                            // $date['attr_num']   = $info['attr_num'];
                            $re2 = $Goods_attr->insert($date);
                        } elseif ($info['sign'] == 'update') {
                            $date['attr_value']                               = $attr_value;
                            isset($info['attr_price']) && $date['attr_price'] = $info['attr_price'];
                            // $date['attr_num']   = $info['attr_num'];
                            $re2 = $Goods_attr->where(array('goods_attr_id' => $info['goods_attr_id']))->update($date);
                        } else {
                            $re2 = $Goods_attr->where(array('goods_attr_id' => $info['goods_attr_id']))->delete();
                        }
                        if (!$re2) {
                            Db::rollback();
                            return json(['code' => 44005, 'msg' => '新增失败，请重试！']);
                        }
                    }
                }

            }else{

                $re2 = true;
            }
            /*属性处理结束*/
            if (false !== $re and false !== $re2) {
                Db::commit();
                return json(['code' => 0, 'msg' => '发布成功']);
            } else {
                Db::rollback();
                return json(['code' => 44005, 'msg' => '新增失败，请重试！']);
            }
        }
        $cate = db('cate')->order('sort desc')->select();
        $cate = cate_merge($cate, null, '0', 'id', 'parentid');
        $this->assign('cate', $cate);
        $goods_type = db('GoodsType')->where(['enabled' => 1])->order('sort desc')->select();
        $this->assign('goods_type', $goods_type);
        return view();
    }

    /**
     * [editGoods 编辑商品页面]
     * @Author   雨夜
     * @DateTime 2018-12-11
     * @return   [view]
     */
    public function editGoods(Request $request)
    {
        if($request->isAjax()){
            $param = input();
            $rule  = [
                'name|名称'     => 'require',
                'img|图片'      => 'require',
                'tags|标签'     => 'require',
//                'summary|简介'  => 'require',
                // 'o_price|市场价' => 'require|float',
                'price|价格'    => 'require|float',
                'cid|分类'      => 'require|integer',
                'sales|销量'    => 'require|integer',
                'sort|排序'     => 'require|integer',
                'id|商品ID'     => 'require',
                'show_user'     =>'require'
            ];
            $validate = $this->validate($param, $rule);
            if (true !== $validate) {
                return json(['code' => 44001, 'msg' => $validate]);
            }
            if(isset($param['param_name']) and isset($param['param_value'])){
                $parameter = [];
                foreach($param['param_name'] as $k=>$v){
                    if(empty($v) || empty($param['param_value'][$k])){
                        continue;
                    }
                    $parameter[$k]['name'] = $v;
                    $parameter[$k]['value'] = $param['param_value'][$k];
                }
                $param['parameter'] = json_encode($parameter);
            }
            $param['status'] = input('?status') ? 1 : 0;
            $param['is_recom'] = input('?is_recom') ? 1 :0;
            $param['show_user'] = implode(',',$param['show_user']);
            $param['is_promote'] = input('?is_promote') ? 1 : 0;
            if (input('?is_promote')) {
                $param['is_promote'] = 1;
                if(empty($param['promote_price']) || empty($param['start_time']) || empty($param['end_time'])){
                    return json(['code' => 44004, 'msg' => '秒杀参数不完整']);
                }
                $param['start_time'] = strtotime($param['start_time']);
                $param['end_time'] = strtotime($param['end_time']);
                if($param['start_time']>=$param['end_time']){
                    return json(['code' => 44004, 'msg' => '开始时间不能大于结束时间']);
                }
            }else{
                $param['start_time'] = 0;
                $param['end_time'] = 0;
            }
            $re = model('Goods')->isUpdate(true)->allowField(true)->save($param);
            if (!$re) {
                Db::rollback();
                return json(['code' => 44005, 'msg' => '新增失败，请重试！']);
            }
            $Goods_attr = model('Goods_attr');
            $goods_id   = $param['id'];
            /* 处理属性 引用ecshop的属性操作方法*/
            if ((isset($_POST['attr_id_list']) && isset($_POST['attr_value_list'])) || (empty($_POST['attr_id_list']) && empty($_POST['attr_value_list']))) {
                // 取得原有的属性值
                $goods_attr_list = array();
                $attr_list       = $Goods_attr->where(array('goods_id' => $goods_id))->select();

                foreach ($attr_list as $key => $row) {
                    $goods_attr_list[$row['attr_id']][$row['attr_value']] = array('sign' => 'delete', 'goods_attr_id' => $row['goods_attr_id']);
                }
                // 循环现有的，根据原有的做相应处理
                if (isset($_POST['attr_id_list'])) {
                    foreach ($_POST['attr_id_list'] as $key => $attr_id) {
                        $attr_value                                     = $_POST['attr_value_list'][$key];
                        isset($_POST['attr_price_list']) && $attr_price = $_POST['attr_price_list'][$key] ?? '0';
                        // $attr_num   = $_POST['attr_num_list'][$key];
                        if (!empty($attr_value)) {
                            if (isset($goods_attr_list[$attr_id][$attr_value])) {
                                // 如果原来有，标记为更新
                                $goods_attr_list[$attr_id][$attr_value]['sign']                             = 'update';
                                isset($attr_price) && $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                                // $goods_attr_list[$attr_id][$attr_value]['attr_num']   = $attr_num;
                            } else {
                                // 如果原来没有，标记为新增
                                $goods_attr_list[$attr_id][$attr_value]['sign']                             = 'insert';
                                isset($attr_price) && $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                                // $goods_attr_list[$attr_id][$attr_value]['attr_num']   = $attr_num;
                            }
                        }
                    }
                }
                /* 插入、更新、删除数据 */
                foreach ($goods_attr_list as $attr_id => $attr_value_list) {
                    foreach ($attr_value_list as $attr_value => $info) {
                        $date = array();
                        if ($info['sign'] == 'insert') {
                            $date['goods_id']                                 = $goods_id;
                            $date['attr_id']                                  = $attr_id;
                            $date['attr_value']                               = $attr_value;
                            isset($info['attr_price']) && $date['attr_price'] = $info['attr_price'];
                            // $date['attr_num']   = $info['attr_num'];
                            $re2 = $Goods_attr->insert($date);
                        } elseif ($info['sign'] == 'update') {
                            $date['attr_value']                               = $attr_value;
                            isset($info['attr_price']) && $date['attr_price'] = $info['attr_price'];
                            // $date['attr_num']   = $info['attr_num'];
                            $re2 = $Goods_attr->where(array('goods_attr_id' => $info['goods_attr_id']))->update($date);
                        } else {
                            $re2 = $Goods_attr->where(array('goods_attr_id' => $info['goods_attr_id']))->delete();
                        }
                    }
                }
                if (false === $re2) {
                    Db::rollback();
                    return json(['code' => 44005, 'msg' => '编辑失败，请重试！']);
                }
            } else {
                $re2 = true;
            }
            if (false !== $re and false !== $re2) {
                Db::commit();
                return json(['code' => 0, 'msg' => '发布成功']);
            } else {
                Db::rollback();
                return json(['code' => 44005, 'msg' => '编辑失败，请重试！']);
            }
        }
        $info = model('Goods')->where(['id' => input('gid')])->find();

        if (!$info) {
            $this->error('参数错误');
        }
        $info['parameter'] && $info['parameter'] = json_decode($info['parameter'],true);
        $info['show_user'] = explode(',',$info['show_user']);

        $this->assign('info', $info);
        $cate = db('cate')->order('sort desc')->select();
        $cate = cate_merge($cate, null, '0', 'id', 'parentid');
        $this->assign('cate', $cate);
        $goods_type = db('GoodsType')->where(['enabled' => 1])->order('sort desc')->select();
        $this->assign('goods_type', $goods_type);

        return view();
    }

    /**
     * [getAttrList 获取属性列表，构造html]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getAttrList()
    {
        $param      = input();
        $goods_id   = isset($param['goods_id']) ? intval($param['goods_id']) : 0;
        $goods_type = isset($param['goods_type_id']) ? intval($param['goods_type_id']) : 0;
        $content    = $this->build_attr_html($goods_type, $goods_id);
        return json($content);

    }

    /**
     * [delGoods 删除商品]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function delGoods()
    {
        $re = db('Goods')->where(['id' => input('id')])->setField('is_del', 1);
        if (false !== $re) {
            return json(['code' => 0, 'msg' => '删除成功']);
        } else {
            return json(['code' => 44004, 'msg' => '删除失败']);
        }
    }

    /**
     * [goodsGallery 商品相册]
     * @Author   雨夜
     * @DateTime 2018-12-11
     * @return   [view]
     */
    public function goodsGallery()
    {
        $goods = db('Goods')->where(['id' => input('id')])->find();
        if (!$goods) {
            $this->error('参数错误');
        }
        $this->assign('goods',$goods);
        return view();
    }

    /**
     * [galleryJson 相册数据集]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function galleryJson()
    {
        $post = input();
        $map  = [];
        if (input('?keywords')) {
            $map[] = ['info', 'like', '%' . input('keywords') . '%'];
        }
        $map[] = ['gid', 'eq', $post['gid']];
        $list  = model('GoodsGallery')->where($map)->page($post['page'], $post['limit'])->order('sort desc')->select();
        foreach ($list as $k => $v) {
            $list[$k]['img_url'] = config('site.site_url') . $v['img_url'];
        }
        if (!$list->isEmpty()) {
            $result['data']  = $list;
            $result['count'] = model('GoodsGallery')->where($map)->count();
            $result['code']  = 0;
            return json($result);
        } else {
            return json(['code' => 0, 'msg' => '暂无数据', 'count' => 0, 'data' => []]);
        }
    }

    public function addGallery(Request $request)
    {
        if($request->isAjax()){
            $post = input();
            $imgs = explode(',', $post['img_url']);
            foreach ($imgs as $k => $v) {
                $data[$k]['img_url'] = $v;
                $data[$k]['info']    = $post['info'];
                $data[$k]['gid']     = $post['gid'];
                $data[$k]['sort']    = $post['sort'];
            }
            $re = model('GoodsGallery')->allowField(true)->saveAll($data);
            if ($re) {
                return json(['code' => 0, 'msg' => '保存成功']);
            } else {
                return json(['code' => 44005, 'msg' => '失败，请重试']);
            }
        }
        $goods = db('Goods')->where(['id' => input('gid')])->find();
        if (!$goods) {
            $this->error('参数错误');
        }
        $this->assign('goods',$goods);
        return view();
    }

    public function editGallery(Request $request)
    {
        if($request->isAjax){
            $post = input();
            $re   = model('GoodsGallery')->isUpdate(true)->allowField(true)->save($post);
            if ($re) {
                return json(['code' => 0, 'msg' => '保存成功']);
            } else {
                return json(['code' => 500, 'msg' => '失败，请重试']);
            }
        }
        $gallery = db('GoodsGallery')->where(['id'=>input('id')])->find();
        if(!$gallery){
            $this->error('参数错误');
        }
        $this->assign('info',$gallery);
        return view();
    }

    /**
     * [getGalleryInfo 相册详情]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function getGalleryInfo()
    {
        //详细
        $info        = db('GoodsGallery')->where('id', input('id'))->find();
        $info['img'] = config('app.qiniu.domain_url') . $info['photo'];
        return json(['code' => 0, 'msg' => 'success', 'data' => $info]);
    }

    /**
     * [updateGalleryUnit description]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function updateGalleryUnit()
    {
        $post = input();
        $data = [
            'id'                => $post['id'],
            $post['valueTitle'] => $post['value'],
        ];
        $re = model('gallery')->isUpdate(true)->allowField(true)->save($data);

        if ($re) {
            return json(['code' => 200, 'msg' => '保存成功']);
        } else {
            return json(['code' => 500, 'msg' => '失败，请重试']);
        }
    }


    /**
     * [delGallery 删除相册]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function delGallery()
    {
        $re = db('GoodsGallery')->where('id', input('id'))->delete();
        if ($re) {
            return json(['code' => 200, 'msg' => '删除成功']);
        } else {
            return json(['code' => 500, 'msg' => '删除失败']);
        }
    }

    /**
     * [delAllGallery 批量删除相册]
     * @Author   雨夜
     * @DateTime 2018-12-13
     * @return   [type]     [description]
     */
    public function delAllGallery()
    {
        $re = db('GoodsGallery')->where('id', 'in', input('ids'))->delete();
        if ($re) {
            return json(['code' => 200, 'msg' => '删除成功']);
        } else {
            return json(['code' => 500, 'msg' => '删除失败']);
        }
    }

    public function video(Request $request)
    {
        $param = $request->param();
        if($request->isAjax()){
            $list = model('GoodsVideo')->where(['gid'=>$param['gid']])->page($param['page'],$param['limit'])->order('create_time desc')->select();
            $count = model('GoodsVideo')->where(['gid'=>$param['gid']])->count();
            return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
        }else{
            return view();
        }
    }

    public function addVideo()
    {
        return view();
    }
    public function editVideo()
    {
        $info = db('GoodsVideo')->find(input('id'));
        $this->assign('info',$info);
        return view();
    }
    public function saveVideo()
    {
        $param = input();
        $rule = [
            'title'=>'require',
            'video_url'=>'require'
        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
        $param['video'] = config('site.qiniu_domain').$param['video_url'];
//        $videoInfo = http_request(config('site.qiniu_domain').$param['video_url'].'?avinfo');
//        $param['duration'] = round((json_decode($videoInfo,true)['format']['duration'])/60);
        if(isset($param['id'])){
            $re = model('GoodsVideo')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('GoodsVideo')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function delVideo(Request $request)
    {
        $re = Db::name('GoodsVideo')->where(['id'=>$request->param('vid')])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    /*========================================私有类==========================================*/

    /**
     * [build_attr_html 创建属性html]
     * @Author   雨夜
     * @DateTime 2018-12-11
     * @param goods_type_id    [int]
     * @param goods_id    [int]
     * @return   [string]
     */
    private function build_attr_html($goods_type_id, $goods_id)
    {
        $goods_type = db('GoodsType')->where(['id' => $goods_type_id])->find();
        if (!$goods_type) {
            return ['code' => 44001, 'msg' => '参数错误，请重试！'];
        }

        // 查询属性值及商品的属性值
        // $sql = "SELECT a.attr_id, a.attr_name, a.attr_input_type, a.attr_values,a.attr_index,a.attr_type,a.attr_show_type, v.attr_value, v.attr_price, v.attr_num FROM 'td_attribute' AS a LEFT JOIN  'td_goods_attr'  AS v ON v.attr_id = a.attr_id AND v.goods_id = '".$goods_id."' WHERE a.type_id = " . intval($goods_type_id) ." OR a.type_id = 0  ORDER BY a.sort, a.attr_id, v.attr_price, v.goods_attr_id";

        // $attrlist = Db::query($sql);
        $attrlist = Db::view('Attribute', 'attr_id,attr_name,attr_input_type,attr_values,attr_show_type')
            ->view('GoodsAttr', 'attr_value,attr_price', 'GoodsAttr.attr_id=Attribute.attr_id and GoodsAttr.goods_id=' . $goods_id, 'LEFT')
            ->where(['type_id' => $goods_type_id])->select();
        // $attrlist = db('attribute')->where(['type_id' => $goods_type['id']])->select();
        if (!$attrlist) {
            return ['code' => 44001, 'msg' => '暂无属性可选'];
        }
        $html = '';
        $spec = 0;
        foreach ($attrlist as $k => $v) {
            $html .= '<div class="layui-form-item">';
            $html .= '<label class="layui-form-label">' . $v['attr_name'] . '</label>';
            $html .= '<input type="hidden" name="attr_id_list[]" value="' . $v['attr_id'] . '">';
            if ($v['attr_input_type'] == 1) {
                $attr_value_arr = explode(',', $v['attr_values']);

                $html .= '<div class="layui-input-inline" >';
                $html .= '<select name = "attr_value_list[]" >';
                foreach ($attr_value_arr as $kk => $vv) {
                    if ($v['attr_value'] != $vv) {
                        $html .= '<option value="' . $vv . '">' . $vv . '</option>';
                    } else {
                        $html .= '<option value="' . $vv . '"  selected>' . $vv . '</option>';
                    }
                }
                $html .= '</select>';
                $html .= '</div>';
            } else {

                $html .= '<div class="layui-input-inline" >';
                $html .= '<input type="text" name="attr_value_list[]" lay-verify="required" autocomplete="off" placeholder="请输入属性值" class="layui-input" value="' . htmlspecialchars($v['attr_value']) . '">';
                $html .= '</div>';
            }
            if ($v['attr_show_type'] == 1) {
                $html .= '<div class="layui-input-inline" >';
                $html .= '<input type="number" name="attr_price_list[]" lay-verify="required" autocomplete="off" placeholder="请输入属性价格" class="layui-input" step="0.01" value="' . htmlspecialchars($v['attr_price']) . '">';
                $html .= '</div>';
            }
            $html .= '<div class="layui-input-inline" >';
            if ($spec != $v['attr_id']) {
                $html .= '<button type="button" class="layui-btn layui-btn-primary addSpec" >+</button>';
            } else {
                $html .= '<button type="button" class="layui-btn layui-btn-primary removeSpec" >-</button>';
            }
            $html .= '</div>';
            $html .= '</div>';
            $spec = $v['attr_id'];
        }
        return ['code' => 0, 'msg' => '请求成功', 'data' => $html];
    }

    public function comment(){

        return view();
    }
}

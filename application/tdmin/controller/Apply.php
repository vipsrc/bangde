<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/22
 * Time: 14:52
 */
namespace app\tdmin\controller;

use think\Request;
use think\Db;

class Apply extends Common
{
    /**
     * 商家认证申请
     * @author 雨夜
     * @date 2019年3月22日15:06:16
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function shopindex(Request $request)
    {
        if($request->isAjax()){
            $map = [];
            $apply = model('Apply');
            $map[] = ['type','eq',1];
            $list = $apply->with('member')->where($map)->page($request->page,$request->limit)->order('create_time desc')->select();
            $count = $apply->where($map)->count();
            return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
        }
        return view();
    }

    /**
     * 施工单位认证申请
     * @author 雨夜
     * @date 2019年3月22日15:07:46
     * @param Request $request
     * @return \think\response\Json|\think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function builderindex(Request $request)
    {
        if($request->isAjax()){
            $map = [];
            $map[] = ['type','neq','1'];
            $apply = model('Apply');
            $list = $apply->with('member')->where($map)->page($request->page,$request->limit)->order('create_time desc')->select()->toArray();
            $count = $apply->where($map)->count();
            return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
        }
        return view();
    }

    /**
     * [checkApply] 认证审核
     * @author 雨夜
     * @date 2019/6/10
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function checkApply(Request $request)
    {
        $info = model('Apply')->where(['id'=>$request->param('id')])->find();
        if(!$info){
            return json(['code'=>0,'msg'=>'参数错误']);
        }
        $re = model('Apply')->isUpdate(true)->allowField(true)->save($request->param());
        if(false !== $re){
            if($request->param('status')==1){
                Db::name('Member')->where(['id'=>$info['uid']])->update(['role'=>$info['type'],'mobile'=>$info['mobile']]);
            }
            return json(['code'=>0,'msg'=>'审核成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function info()
    {
        $info = model('Apply')->with(['province','city','county','reprovince','recity','recounty','member'])->where(['id'=>input('id')])->find();

        if(!$info){
            $this->error('参数错误');
        }
        if(!empty($info['certificate'])){
            $info['certificate'] = explode(',',$info['certificate']);
        }
        $this->assign('info',$info);
        return view();
    }
}
<?php
namespace app\tdmin\controller;

use think\Db;
use think\Request;

class Video extends Common
{
	/**
	 * [videoCate 视频分类]
	 * @Author   雨夜
	 * @DateTime 2018-12-18
	 * @return   [type]     [description]
	 */
	public function videoCate(Request $request)
	{
        if($request->isAjax()){
            $param = input();
            $list = model('VideoCate')->page($param['page'],$param['limit'])->order('sort desc')->select();
            $count = model('Video')->count();
            return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
        }
		return view();
	}

    /**
     * [addCate 保存分类]
     * @Author   雨夜
     * @DateTime 2018-12-18
     */
    public function addCate()
    {
        $param = input();
        $rule = [
            'name'=>'require',
            'sort'=>'require|number'
        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
        if(isset($param['id'])){
            $re = model('VideoCate')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('VideoCate')->allowField(true)->save($param);
        }
        if(false!==$re ){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }


    /**
     * [delCate 删除分类]
     * @Author   雨夜
     * @DateTime 2018-12-18
     * @return   [type]     [description]
     */
    public function delCate()
    {
        $param = input();
        $re = db('VideoCate')->where(['id'=>$param['id']])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>44004,'msg'=>'删除失败']);
        }
    }

	/**
	 * [videoList 视频列表]
	 * @Author   雨夜
	 * @DateTime 2018-12-18
	 * @return   [type]     [description]
	 */
	public function index()
	{
		return view();
	}

    /**
     * [getVideoList 视频列表数据]
     * @Author   雨夜
     * @DateTime 2018-12-18
     * @return   [type]     [description]
     */
    public function getVideoList()
    {
        $param = input();
        $map = [];
        if(isset($param['cate']) and $param['cate']){
            $map[] = ['cate','eq',$param['cate']];
        }
        if(isset($param['keywords']) and $param['keywords']){
            $map[] = ['title|summary','like','%'.$param['keywords'].'%'];
        }
        $list = model('Video')->where($map)->page($param['page'],$param['limit'])->order('sort desc')->select()->each(function($item,$key){
            $item['video_url'] = config('site.qiniu_domain').$item['video_url'];
            return $item;
        });
        $count = model('Video')->count();
        return json(['code'=>0,'msg'=>'请求成功','data'=>$list]);
    }

	/**
	 * [addVideo 添加视频]
	 * @Author   雨夜
	 * @DateTime 2018-12-20
	 */
	public function addVideo()
	{
		$cate = db('VideoCate')->select();
		$this->assign('cate',$cate);
		return view();
	}

    /**
     * [saveVideo 保存视频]
     * @Author   雨夜
     * @DateTime 2018-12-21
     * @return   [type]     [description]
     */
    public function saveVideo()
    {
        $param = input();
        $rule = [
            'title'=>'require',
            'summary'=>'require',
            'video_url'=>'require',
            'cate'=>'require',
            'sort'=>'require',
        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44001,'msg'=>$validate]);
        }
//        $videoInfo = http_request(config('site.qiniu_domain').$param['video_url'].'?avinfo');
//        $param['duration'] = round((json_decode($videoInfo,true)['format']['duration'])/60);
        if(isset($param['id'])){
            $re = model('Video')->isUpdate(true)->allowField(true)->save($param);
        }else{
            $re = model('Video')->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            return json(['code'=>0,'msg'=>'保存成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

	/**
	 * [editVideo 编辑视频]
	 * @Author   雨夜
	 * @DateTime 2018-12-20
	 * @return   [type]     [description]
	 */
	public function editVideo()
	{
		$cate = db('VideoCate')->select();
		$this->assign('cate',$cate);
		$info = db('Video')->find(input('id'));
		$this->assign('info',$info);
		return view();
	}

    /**
     * [delVideo 删除视频]
     * @Author   雨夜
     * @DateTime 2018-12-19
     * @return   [type]     [description]
     */
    public function delVideo()
    {
        $param = input();
        $re = db('Video')->where(['id'=>$param['id']])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>40001,'msg'=>'删除失败']);
        }
    }

    /**
     * @author 雨夜
     * @date 2019年3月19日10:29:16
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getVideoInfo()
    {
        $param = input();
        $map[] = ['id','eq',$param['id']];
        $info = model('video')->where($map)->find();
        $info['video_url'] = config('site.qiniu_domain').$info['video_url'];
        if($info){
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }

    public function slide(Request $request)
    {
        if($request->isAjax()){
            $post = input();
            $list = model('slide')->page($post['page'], $post['limit'])->order('sort desc')->select()->toArray();
            foreach($list as $k=>$v){
                $list[$k]['img_url'] = config('site.site_url').$v['img'];
            }
            if ($list) {
                $result['data'] = $list;
                $result['count'] = db('slide')->count();
                $result['code'] = 0;
                return json($result);
            } else {
                return json(['code' => 205, 'msg' => '暂无数据']);
            }
        }
        return view();
    }

    public function addSlide()
    {
        if (request()->isAjax()) {
            $post = input();
            if(empty($post['img'])){
                return json(['code'=>500,'msg'=>'请上传轮播图片']);
            }
            $res = model('slide')->isUpdate(false)->allowField(true)->save($post);
            if (false !== $res) {
                return json(['code' => 200, 'msg' => '添加成功']);
            } else {
                return json(['code' => 500, 'msg' => '添加失败']);
            }
        }
        return view();
    }

    public function editSlide()
    {
        if (request()->isAjax()) {
            $post = input();
            if(empty($post['img'])){
                return json(['code'=>500,'msg'=>'请上传轮播图片']);
            }
            $res = model('slide')->isUpdate(true)->allowField(true)->save($post);
            if (false !== $res) {
                return json(['code' => 200, 'msg' => '编辑成功']);
            } else {
                return json(['code' => 500, 'msg' => '编辑失败']);
            }
        }
        $info = model('slide')->where('id', input('id'))->find();
        !$info && $this->error('参数错误');
        $this->assign('info', $info);
        return view();
    }

    public function delSlide()
    {
        $info = db('slide')->where('id', input('id'))->find();
        !$info && $this->error('参数错误');
        $res = db('slide')->where('id', input('id'))->delete();
        if ($res) {
            return json(['code' => '200', 'msg' => '删除成功']);
        } else {
            return json(['code' => '500', 'msg' => '删除失败']);
        }
    }
}
?>
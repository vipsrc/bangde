<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/18
 * Time: 14:12
 */
namespace app\tdmin\controller;

use think\Db;
use think\Request;
use app\common\model\VipLevel;
use think\facade\Cache;

class Member extends Common
{
    /**
     * [index 会员列表]
     * @Author   雨夜
     * @DateTime 2019-3-18 14:13:27
     * @return   [view]     [视图渲染]
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * [getUserList 获取会员列表]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function getUserList()
    {
        $map = [];
        $userList = model('Member')->where($map)->page(input('page'), input('limit'))->order('create_time desc')->select()->toArray();
        $result['data']  = $userList;
        $result['count'] = model('Member')->count();
        $result          = array_merge($this->api_code[0], $result);

        return json($result);
    }

    /***
     * 根据ID获取用户的信息
     * @author 江远
     */
    public function getUserInfoById()
    {
        $uid = input('uid');
        if (empty($uid)) {
            $this->api_code[40001]['msg'] = '参数不正确！';
            return json($this->api_code[40001]);
        }
        $where[]        = array('id', 'eq', $uid);
        $info           = model('Member')->where($where)->find();
        if ($info) {
            $this->api_code[0]['msg']  = '获取用户信息成功！';
            $this->api_code[0]['data'] = $info;
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '获取用户信息失败！';
            return json($this->api_code[40001]);
        }
    }

    /**
     * 会员等级
     * @author 雨夜
     * @date 2019年3月20日16:06:24
     * @return \think\response\View
     */
    public function viplevel(Request $request)
    {
        if($request->isAjax()){
            $map = [];
            $list = model('VipLevel')->where($map)->page($request->param('page'),$request->param('limit'))->order('')->select();
            $count = model('VipLevel')->where($map)->count();
            return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
        }
        return view();
    }

    /**
     * 保存会员等级
     * @author 雨夜
     * @date 2019年3月21日17:28:57
     * @param Request $request
     * @return \think\response\Json
     */
    public function saveLevel(Request $request)
    {
        $param = $request->param();
        $rule = [
            'name|名称'=>'require',
            'role|角色'=>'require|number',
            'discount|折扣'=>'require|between:0,100',
            'min_consume|最低消费'=>'require',
            'max_consume|最高消费'=>'require|gt:min_consume',
        ];
        $validate = $this->validate($param,$rule);
        if(true !== $validate){
            return json(['code'=>44004,'msg'=>$validate]);
        }
        $VipLevel = model('VipLevel');

        if($request->param('id')){
            $ishas = $VipLevel->where([['role','eq',$param['role']],['max_consume','>=',$param['min_consume']],['min_consume','<=',$param['min_consume']],['id','neq',$request->param('id')]])->find();
            if($ishas){
                return json(['code'=>44001,'msg'=>'消费区间有重叠，请重试！']);
            }
            $re = $VipLevel->isUpdate(true)->allowField(true)->save($param);
        }else{
            $ishas = $VipLevel->where([['role','eq',$param['role']],['max_consume','>=',$param['min_consume']],['min_consume','<=',$param['min_consume']]])->find();
            if($ishas){
                return json(['code'=>44001,'msg'=>'消费区间有重叠，请重试！']);
            }
            $re = $VipLevel->isUpdate(false)->allowField(true)->save($param);
        }
        if(false !== $re){
            $list = $VipLevel->where(['role'=>$param['role']])->select()->toArray();
            Cache::set('level_'.$param['role'],$list,0);
            return json(['code'=>0,'msg'=>'保存成功！']);
        }else{
            return json(['code'=>4001,'msg'=>'系统错误！']);
        }
    }

    public function delLevel(Request $request,$id)
    {
        $viplevel = new VipLevel;
        $re = $viplevel->where('id',$id)->delete();
        if(false != $re){
            return json(['code'=>0,'msg'=>'删除成功']);
        }else{
            return json(['code'=>40001,'msg'=>'系统错误']);
        }
    }
}
<?php
/**
 * 江远
 * 2018/11/28
 * 用户管理控制类
 */
namespace app\tdmin\controller;

use think\Db;
use think\Request;

class Authuser extends Common
{
    /**
     * 用户列表
     * @author 雨夜
     * @DateTime 2019-3-18 13:54:51
     */
    public function index(Request $request)
    {
        if($request->isAjax()){
            $uids = db('auth_group_access')->where([['group_id','neq',2]])->column('uid');
            $map[] = ['id','in',$uids];
            $userList = model('AuthUser')->where($map)->page(input('page'), input('limit'))->order('create_time desc')->select();
            foreach ($userList as $k => $v) {
                $userList[$k]['lastlogintime'] = $v['lastlogintime'] ? date('Y-m-d H:i', $v['lastlogintime']) : '';
            }
            $result['data']  = $userList;
            $result['count'] = model('AuthUser')->count();
            $result          = array_merge($this->api_code[0], $result);
            return json($result);
        }
        $role = db('AuthGroup')->where(['status'=>1])->select();
        $this->assign('roles',$role);
        return $this->fetch();
    }

    /***
     * 添加用户
     */
    public function addAuthUser()
    {
        if($this->request->isAjax()){
            $rule = [
                'nickname|昵称'   => 'require',
                'realname|真实姓名' => 'require',
                'account|账号'    => 'require|alphaNum',
                'mobile|手机号'    => 'require|mobile',
                'frozen|状态'     => 'require|in:0,1',
                'avatar|头像'     => 'require',
                'group_id|角色'   => 'require|array',
                'password'      => 'require',
            ];
            $post     = input();
            $validate = $this->validate($post, $rule);
            if (true !== $validate) {
                return json(['code' => 44001, 'msg' => $validate]);
            }
            $ishas = Db::name('AuthUser')->where([['account','=',input('account')]])->find();
            if($ishas){
                return josn(['code'=>44004,'msg'=>'账号已存在']);
            }
            $post['password'] = md5(md5($post['password']));
            $User             = model('AuthUser');
            $r                = $User->isUpdate(false)->allowField(true)->save($post);
            if (false !== $r) {
                $uid = $User->id;
                foreach ($post['group_id'] as $v) {
                    db('auth_group_access')->insert(['uid' => $uid, 'group_id' => $v]);
                }
                $this->api_code[0]['msg'] = '添加用户成功';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['msg'] = '添加用户失败';
                return json($this->api_code[40001]);
            }
        }
        return $this->fetch();
    }

    /***
     * 编辑用户
     * @author 雨夜
     */
    public function editAuthUser()
    {
        if($this->request->isAjax()){
            $rule = [
                'nickname|昵称'   => 'require',
                'realname|真实姓名' => 'require',
                'account|账号'=>'require|alphaNum',
                'mobile|手机号'    => 'require|mobile',
                'frozen|状态'     => 'require|in:0,1',
                'avatar|头像'     => 'require',
                'group_id|角色'   => 'require|array',
                'id|用户ID'       => 'require|number',
            ];
            $post     = input();
            // $validate = $this->validate($post, $rule);
            // if (true !== $validate) {
            //     return json(['code' => 44001, 'msg' => $validate]);
            // }
            $ishas = Db::name('AuthUser')->where([['account','=',input('account')],['id','neq',$post['id']]])->find();
            if($ishas){
                return josn(['code'=>44004,'msg'=>'账号已存在']);
            }
            if (isset($post['password'])) {
                $post['password'] = md5(md5($post['password']));
            } else {
                unset($post['password']);
            }
            $User = model('AuthUser');
            $r    = $User->isUpdate(true)->allowField(true)->save($post);
            if (false !== $r) {
                if(isset($post['group_id'])){
                    db('auth_group_access')->where(['uid' => $post['id']])->delete();
                    foreach ($post['group_id'] as $v) {
                        db('auth_group_access')->insert(['uid' => $post['id'], 'group_id' => $v]);
                    }
                }
                $this->api_code[0]['msg'] = '编辑用户成功！';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['msg'] = '编辑用户失败！';
                return json($this->api_code[40001]);
            }
        }else{
            $uid  = input('uid');
            $info = model('AuthUser')->field('id,nickname,realname,usertype,mobile,frozen,avatar,level')->where('uid', 'eq', $uid)->find();
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

    /***
     * 删除用户
     * @author 江远
     */
    public function delAuthUser()
    {
        $uid     = input('uid');
        $where[] = array('uid', 'eq', $uid);
        $r       = Db::name('AuthUser')->where($where)->delete();
        if ($r) {
            $this->api_code[0]['msg'] = '删除用户成功！';
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '删除用户失败！';
            return json($this->api_code[40001]);
        }
    }

    /***
     * 根据ID获取用户的信息
     * @author 雨夜
     */
    public function getUserInfoById()
    {
        $uid = input('uid');
        if (empty($uid)) {
            $this->api_code[40001]['msg'] = '参数不正确！';
            return json($this->api_code[40001]);
        }
        $where[]        = array('id', 'eq', $uid);
        $info           = model('AuthUser')->where($where)->find();
        $info['groups'] = db('auth_group_access')->where(['uid' => $uid])->column('group_id');

        if ($info) {
            $this->api_code[0]['msg']  = '获取用户信息成功！';
            $this->api_code[0]['data'] = $info;
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '获取用户信息失败！';
            return json($this->api_code[40001]);
        }
    }
}

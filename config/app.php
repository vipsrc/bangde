<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用名称
    'app_name'               => '邦德仕家',
    // 应用地址
    'app_host'               => '',
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => false,
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'Asia/Shanghai',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空模块名
    'empty_module'           => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法前缀
    'use_action_prefix'      => false,
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // HTTPS代理标识
    'https_agent_name'       => '',
    // IP代理获取标识
    'http_agent_ip'          => 'X-REAL-IP',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由延迟解析
    'url_lazy_route'         => false,
    // 是否强制使用路由
    'url_route_must'         => false,
    // 合并路由规则
    'route_rule_merge'       => false,
    // 路由是否完全匹配
    'route_complete_match'   => false,
    // 使用注解路由
    'route_annotation'       => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],
    // 是否开启路由缓存
    'route_check_cache'      => false,
    // 路由缓存的Key自定义设置（闭包），默认为当前URL和请求类型的md5
    'route_check_cache_key'  => '',
    // 路由缓存类型及参数
    'route_cache_option'     => [],

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => Env::get('think_path') . 'tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl'    => Env::get('think_path') . 'tpl/dispatch_jump.tpl',

    // 异常页面的模板文件
    'exception_tmpl'         => Env::get('think_path') . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    // auth配置 20181128 秋水
    'auth'                   => [
        'auth_on'           => 1, // 权限开关
        'auth_type'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'auth_group'        => 'auth_group', // 用户组数据不带前缀表名
        'auth_group_access' => 'auth_group_access', // 用户-用户组关系不带前缀表
        'auth_rule'         => 'auth_rule', // 权限规则不带前缀表
        'auth_user'         => 'member', // 用户信息不带前缀表
        'super_admin'       => 1,       //超级管理id
    ],

    // 接口返回码配置
    'api_code'               => [
        0     => ['code' => 0, 'msg' => '请求成功', 'desc' => ''],
        40001 => ['code' => 40001, 'msg' => '访问失败，请重新进入页面   ', 'desc' => 'token不能为空'],
        40002 => ['code' => 40002, 'msg' => '用户名不存在，请先注册', 'desc' => '没有传token或者id'],
        40003 => ['code' => 40003, 'msg' => '密码不正确，请重新填写', 'desc' => '密码不对'],
        40004 => ['code' => 40004, 'msg' => '登录过期，请重新登录 ', 'desc' => 'token过期'],
        40005 => ['code' => 40005, 'msg' => '此用户违反规定已进入黑名单，请联系客服', 'desc' => '用户因为xx原因被拉黑'],
        40006 => ['code' => 40006, 'msg' => '验证码不正确，请重新获取   ', 'desc' => '验证码与手机号不匹配'],
        41001 => ['code' => 41001, 'msg' => '余额不足，请先充值  ', 'desc' => '余额不足'],
        42001 => ['code' => 42001, 'msg' => '积分不足，请前往赚取积分   ', 'desc' => '积分不足'],
        43001 => ['code' => 43001, 'msg' => '充值失败，请检查后重新充值  ', 'desc' => ''],
        44001 => ['code' => 44001, 'msg' => '参数不正确', 'desc' => 'xx参数未填写/不正确'],
        // 44002 => ['code'=>44002, 'msg'=>'信息填写不正确，请检查后重新填写   ', 'desc'=>'xx参数格式不对'],
        44003 => ['code' => 44003, 'msg' => '修改失败，请检查后重新提交  ', 'desc' => '此订单在某些条件下不能被修改'],
        44004 => ['code' => 44004, 'msg' => '删除失败，请重新删除 ', 'desc' => '此订单在某些条件下不能被删除'],
        44005 => ['code' => 44005, 'msg' => '创建失败，请检查后重新提交  ', 'desc' => '此订单在某些条件下不能被创建'],
        50001 => ['code' => 50001, 'msg' => '服务器连接不通顺，请反馈客服 ', 'desc' => '服务器崩了'],
    ],

];
